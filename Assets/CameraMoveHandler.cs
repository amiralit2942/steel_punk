﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class CameraMoveHandler : MonoBehaviour , IBeginDragHandler, IDragHandler, IEndDragHandler,IPointerDownHandler
{
    [SerializeField] private float minX;
    [SerializeField] private float maxX;
    [SerializeField] private Transform cameraTransform;
    
    private Vector3 pInitial;
    private Vector3 camPosInitial;

    private Vector3 lastPos;
    private bool _isDragging = false;

    private float speed = 0;
    private float initialSpeed = 0;
    private float t = 0;

    private void Start()
    {
        cameraTransform.position = new Vector3(-10,0,0);
    }

    private void Update()
    {
        if (!_isDragging)
        {
            t += Time.deltaTime;
            var newX = cameraTransform.position.x + speed;
            if (newX < maxX && newX > minX)
            {
                cameraTransform.Translate(speed, 0, 0);
            }
            speed = Mathf.Lerp(initialSpeed, 0, t);
        }

        if (cameraTransform.position.x > maxX)
        {
            cameraTransform.position = new Vector3(maxX,cameraTransform.position.y,0);
        }
        else if (cameraTransform.position.x < minX)
        {
            cameraTransform.position = new Vector3(minX,cameraTransform.position.y,0);
        }
        
    }

    public void OnBeginDrag(PointerEventData eventData)
    {
        pInitial = eventData.pointerCurrentRaycast.worldPosition;
        camPosInitial = cameraTransform.position;
        _isDragging = true;
        speed = 0;
        lastPos = pInitial;
    }

    public void OnDrag(PointerEventData eventData)
    {
        var currentPos = eventData.pointerCurrentRaycast.worldPosition;
        var moveX = currentPos.x - pInitial.x;
        var newX = cameraTransform.position.x - moveX;
        if (newX < maxX && newX > minX)
        {
            cameraTransform.Translate(-moveX, 0, 0);
        }

        speed = Mathf.Lerp(currentPos.x - lastPos.x, speed, 0.7f);
        lastPos = currentPos;
    }

    public void OnEndDrag(PointerEventData eventData)
    {
        _isDragging = false;
        speed = -speed;
        initialSpeed = speed;
        t = 0;
    }

    public void OnPointerDown(PointerEventData eventData)
    {
        speed = 0;
    }
}
