﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameCanvasFixer : MonoBehaviour
{
    [SerializeField] private RectTransform uiCanvasRt;
    [SerializeField] private RectTransform rectTransform;
    private void Start()
    {
        rectTransform.sizeDelta = uiCanvasRt.sizeDelta;
        rectTransform.localScale = uiCanvasRt.localScale;
    }
}
