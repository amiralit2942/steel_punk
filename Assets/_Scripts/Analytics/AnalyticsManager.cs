using System;
using System.Diagnostics;
using Firebase.Analytics;
using GameAnalyticsSDK;
using GameAnalyticsSDK.Events;
using ir.metrix.unity;
using UnityEngine;
using Util;

namespace _Scripts.Analytics
{
    public class AnalyticsManager : MonoBehaviourSingleton<AnalyticsManager>
    {
        private void Awake()
        {
            DontDestroyOnLoad(this);
        }

        private void Start()
        {
            Initialize();
        }

        //Currency : irr , amount : 30000 (3 hezar toman), itemType:Gem , itemID: , cartType : EndOfLevel
        //
        
        private void Initialize()
        {
            GameAnalytics.Initialize();

            
            // MetrixConfig metrixConfig = new MetrixConfig("aabsjoaihetpvom");
            // metrixConfig.SetFirebaseId("1:135193740:android:a9018c5b66421ba89cc7a9", "joorandegan",
            //     "AIzaSyCfzSrCNWm0coV7v3QYF4uh318pzqI6r8o");
            // metrixConfig.SetStore("Bazaar");
            // metrixConfig.SetDefaultTrackerToken("");
            // Metrix.OnCreate(metrixConfig);
            

#if !UNITY_EDITOR
            Firebase.FirebaseApp.CheckAndFixDependenciesAsync().ContinueWith(task => {
                var dependencyStatus = task.Result;
                if (dependencyStatus == Firebase.DependencyStatus.Available)
                {
                    // Create and hold a reference to your FirebaseApp,
                    // where app is a Firebase.FirebaseApp property of your application class.
                    //   app = Firebase.FirebaseApp.DefaultInstance;
                    Firebase.FirebaseApp app = Firebase.FirebaseApp.DefaultInstance;
                    // Set a flag here to indicate whether Firebase is ready to use by your app.
                    UnityEngine.Debug.Log("Create and hold a reference to your FirebaseApp");

                }
                else
                {
                    UnityEngine.Debug.LogError($"Could not resolve all Firebase dependencies: {dependencyStatus}");
                    // Firebase Unity SDK is not safe to use here.
                }
            });
            Firebase.Messaging.FirebaseMessaging.TokenReceived += OnTokenReceived;
            Firebase.Messaging.FirebaseMessaging.MessageReceived += OnMessageReceived;
#endif
        }

        private void OnTokenReceived(object sender, Firebase.Messaging.TokenReceivedEventArgs token)
        {
            UnityEngine.Debug.Log("Received Registration Token: " + token.Token);
        }

        private void OnMessageReceived(object sender, Firebase.Messaging.MessageReceivedEventArgs e)
        {
            UnityEngine.Debug.Log("Received a new message from: " + e.Message.From);
        }
        
        public void SetPurchase(string itemType, string sku, string itemName, int price,
            string cartType, string currency = "IRR")
        {
            FirebaseAnalytics.LogEvent(FirebaseAnalytics.EventPurchase,
                new Firebase.Analytics.Parameter(Firebase.Analytics.FirebaseAnalytics.ParameterPrice, price),
                new Firebase.Analytics.Parameter(Firebase.Analytics.FirebaseAnalytics.ParameterItemName, itemName),
                new Firebase.Analytics.Parameter(Firebase.Analytics.FirebaseAnalytics.ParameterContentType, itemType),
                new Firebase.Analytics.Parameter(Firebase.Analytics.FirebaseAnalytics.ParameterCurrency, currency),
                new Firebase.Analytics.Parameter(Firebase.Analytics.FirebaseAnalytics.ParameterItemId, sku));
            GameAnalytics.NewBusinessEvent(currency, price, itemType, itemName, cartType);
            Metrix.NewRevenue("rsciv", price * 10, 0, sku);
        }
        
        public void SinkFlow(string currency, int amount, string itemType, string itemName, string from)
        {
            FirebaseAnalytics.LogEvent("Flow",
                new Firebase.Analytics.Parameter("Sink", amount),
                new Firebase.Analytics.Parameter("From", from),
                new Firebase.Analytics.Parameter(Firebase.Analytics.FirebaseAnalytics.ParameterVirtualCurrencyName,
                    currency),
                new Firebase.Analytics.Parameter(Firebase.Analytics.FirebaseAnalytics.ParameterItemName, itemName),
                new Firebase.Analytics.Parameter(Firebase.Analytics.FirebaseAnalytics.ParameterContentType, itemType));
            GameAnalytics.NewResourceEvent(GAResourceFlowType.Sink, currency, amount, itemType, itemName);
        }
        
        public void SourceFlow(string currency, int amount, string itemType, string itemName, string from)
        {
            FirebaseAnalytics.LogEvent("Flow",
                new Firebase.Analytics.Parameter("Source", amount),
                new Firebase.Analytics.Parameter("From", from),
                new Firebase.Analytics.Parameter(Firebase.Analytics.FirebaseAnalytics.ParameterVirtualCurrencyName,
                    currency),
                new Firebase.Analytics.Parameter(Firebase.Analytics.FirebaseAnalytics.ParameterItemName, itemName),
                new Firebase.Analytics.Parameter(Firebase.Analytics.FirebaseAnalytics.ParameterContentType, itemType));
            GameAnalytics.NewResourceEvent(GAResourceFlowType.Source, currency, amount, itemType, itemName);
        }

        public void OnButtonClick(string buttonName)
        {
            GameAnalytics.NewDesignEvent($"Button Click:{buttonName}");
            //FirebaseAnalytics.LogEvent("BottonClick",);
        }
        
        public void ResourceEvent(GAResourceFlowType flowType, string currency,float amount)
        {
            GameAnalytics.NewDesignEvent("Panel:ShowPanel:panelname");
        }
    }
}