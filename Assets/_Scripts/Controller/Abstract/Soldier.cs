﻿using System.ComponentModel;
using System.Net.Mime;
using _Scripts.Model;
using UnityEngine;
using UnityEngine.UI;

namespace _Scripts.Controller.Abstract
{
    public abstract class Soldier : MonoBehaviour
    {
        [SerializeField] public CardsInfo soldierInfo;
        protected string Name;
        protected Soldier.Soldiers SoldierType;
        //protected Side SoldierSide;
        protected int Health;
        //protected int Damage;
        //protected float SpawnCoolDown;
        //protected float AttackCoolDown;
        //[SerializeField]protected float ViewRange;
        protected float Speed;
        protected Rigidbody2D Rigidbody2D;
        protected bool CanMove;
        //protected int RequiredMana;
        //protected bool IsSpecial;
       // protected int Usage;
        protected int soldierLevel;
        protected bool CanAttack;
        protected Animator Anim;
        protected int MovingSide;
        public enum Side
        {
            Player1 = 1,
            Player2 = -1
        }

        public enum Soldiers
        {
            Base,
            Sopo,
            Tank,
            Cop,
            Bomber,
            Sniper,
            Ninja,
            Wizard
        }

        // protected abstract void See();
        //
        // protected abstract void Move();
        //
        // protected abstract void Attack();
        //
        // protected abstract void AttackCoolDownTimer();
        //
        // public abstract void Death();
        //
        // public abstract int GetHealth();
        //
        // protected abstract void SetSoldierInfo();
        //
        // public abstract void SetHealth(int newHealth);
        // public abstract void SetSide(Side soldierSide);
        // public abstract void SetSoldierType(Soldiers soldierType);
    }
}
