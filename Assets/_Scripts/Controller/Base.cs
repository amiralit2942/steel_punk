﻿using System.Collections.Generic;
using _Scripts.Controller.Abstract;
using _Scripts.Manager;
using _Scripts.Model;
using _Scripts.Controller.Abstract;
using _Scripts.DataHolder;
using _Scripts.Interface;
using DG.Tweening;
using Manager;
using _Scripts.Model;
using _Scripts.Model.ScriptableObjectsScripts;
using UnityEngine;
using UnityEngine.UI;
using Util;

namespace _Scripts.Controller
{
    public class Base : MonoBehaviourSingleton<Base>,IHittable
    {
        // [SerializeField] private Text healthText;
        [SerializeField] private Button attackButton;
        [SerializeField] private Image coolDownImage;
        [SerializeField] private Animator gunAnimator;
        [SerializeField] private Transform particleSp;
        private List<int> comicParticlePerHit ;
        public CardsInfo _baseInfo;
        private  Bases _baseType;
        private Soldier.Side _baseSide;
        public Soldier.Side BaseSide => _baseSide;
        public BaseGunInfo BaseGunInfo => _baseGunInfo;
        
        
        private BaseGunInfo _baseGunInfo;
        private int _baseHealth;
        private int _gunDamage;
        private float _gunRange;
        private float _laserGunCoolDown;
        private float _timer;
        private bool _canAttack;
        private float _maxHealth;
        private float _r;
        private float _g;

        

        public enum Bases
        {
            Player,
            Enemy
        }

        private void Update()
        {
            GunCoolDown();
        }

        private void Start()
        {
            SetBaseInfo();
            SetSide();

            comicParticlePerHit = ParticleManager.Instance.ComicParticlePerHit;
            var baseLevel = SafeBox.Instance.userInfo.cards.Find(x => x.card_id == _baseInfo.ID).level;
            _r = 0;
            _g = 1;
            _baseHealth = _baseInfo.health[baseLevel - 1];
            _maxHealth = _baseInfo.health[baseLevel - 1];
            _gunDamage = _baseInfo.damage[baseLevel - 1];
            _gunRange = _baseInfo.view_range;
            _laserGunCoolDown = _baseInfo.attack_cool_down;
            _timer = _laserGunCoolDown;
            // healthText.text = _baseHealth.ToString();
            // healthText.color = new Color(_r,_g,0,1);
            coolDownImage.fillAmount = 1;
            _canAttack = false;
            if (_baseType == Bases.Player)
            {
                attackButton.onClick.AddListener(Attack);
            }
        }
        
        

        private RaycastHit2D[] See()
        {
            var raycastHits = new RaycastHit2D[0];
            if (_baseType == Bases.Player)
            {
                raycastHits = Physics2D.RaycastAll(transform.position, Vector2.right, _gunRange);
            }
            else if (_baseType == Bases.Enemy)
            {
                raycastHits = Physics2D.RaycastAll(transform.position, Vector2.left, _gunRange);
            }

            return raycastHits;
        }

        public void AttackButtonClick()
        {
            gunAnimator.Play("BaseGun");
            print("Attack Button");
        }

        public void Attack()
        {
            print("Attack Called");
            if (_canAttack)
            {
                var raycastHits = See();
                _timer = _laserGunCoolDown;
                foreach (var obstacle in raycastHits)
                {
                    _canAttack = false;
                    if (obstacle.transform.TryGetComponent(out IHittable hit) && hit.side != _baseSide)
                    {
                        hit.OnHit(_gunDamage,Soldier.Soldiers.Base);
                    }

                    /* Soldier.Side soldierSide;
                     var soldierC = obstacle.collider.gameObject.GetComponent<PlayerSoldier>();
                     if (soldierC != null)
                     {
                         soldierSide = soldierC.GetSoldierSide();
                         if (soldierSide != _baseSide)//obstacle.collider.gameObject.CompareTag("Enemy"))
                         {
                             var damagedEnemySoldier = obstacle.collider.GetComponent<Soldier>();
                             if (damagedEnemySoldier != null)
                             {
                                 var damagedEnemySoldierHealth = damagedEnemySoldier.GetHealth();
                                 damagedEnemySoldierHealth -= _gunDamage;
                                 if (damagedEnemySoldierHealth <= 0)
                                 {
                                     damagedEnemySoldier.Death();
                                 }
     
                                 damagedEnemySoldier.SetHealth(damagedEnemySoldierHealth);
                             }
                         }    
                     }*/
                    else
                    {
                        continue;
                    }

                }
            }
        }

        public void Death()
        {
            Destroy(gameObject);
            Dead = true;
            if (_baseType == Bases.Player)
            {
                GameManager.Instance.GameOver(false);    
            }
            else
            {
                GameManager.Instance.GameOver(true);
            }
        }

        private void SetSide()
        {
            if (transform.position.x <= GameManager.Instance.playerSpawnPoint.transform.position.x)
            {
                _baseType = Bases.Player;
                _baseSide = Soldier.Side.Player1;
            }
            else if (transform.position.x >= GameManager.Instance.enemySpawnPoint.transform.position.y)
            {
                _baseType = Bases.Enemy;
                _baseSide = Soldier.Side.Player2;
            }
        }

        private void SetBaseInfo()
        {
            _baseInfo = SafeBox.Instance.cardsInfo
                .Find(x => x.soldier_type == Soldier.Soldiers.Base);
            Dead = false;
            //var userBaseGun = SafeBox.Instance.userInfo.selectedGun;
            // _baseGunInfo = SafeBox.Instance.baseGunsInfo
            //     .Find(x => x.gun_id == userBaseGun.id);
            //gunImage.sprite = PrefabHolder.Instance.GetBaseGunSprite(_baseGunInfo.base_gun_type);
        }

        public void SetHealth(int newHealth)
        {
            _baseHealth = newHealth;
            //healthText.text = _baseHealth.ToString();
        }

        public int GetHealth()
        {
            return _baseHealth;
        }

        public Soldier.Side side => _baseSide;
        public bool Dead { get; set; }

        public void OnHit(int damage, Soldier.Soldiers attackingSoldier)
        {
            GetRandomComicParticle();
            Instantiate(ParticleManager.Instance.GetSoldierParticle(attackingSoldier, ParticleType.Hit), particleSp);
            if (GetHealth() > damage)
            {
                SetHealth(GetHealth() - damage);
            }
            else
            {
                Death();
            }

            //var damagePerColor = damage * 510 / _maxHealth;
            var damagePerColor = damage * 2 / _maxHealth;
            if (_r + damagePerColor > 1)
            {
                var difference = damagePerColor - (1 - _r);
                
                if (_g - difference < 0)
                {
                    _g = 0;
                    _r = 1;
                    //healthText.DOColor(new Color(_r, _g, 0, 1), 0.2f);
                }
                else
                {
                    _g -= difference;
                    _r = 1;
                    //healthText.DOColor(new Color(_r, _g, 0, 1), 0.2f);
                }
            }
            else
            {
                _r += damagePerColor;
                //healthText.DOColor(new Color(_r, _g, 0, 1), .5f);
            }
        }

        private int _remainingHit;
        public void GetRandomComicParticle()
        {
            if (_remainingHit == 0)
            {
                Instantiate(ParticleManager.Instance.GetComicParticle(), particleSp);
                _remainingHit = comicParticlePerHit[Random.Range(0, comicParticlePerHit.Count)];
                return;
            }
            _remainingHit--;
        }

        public GameObject GetGameObject()
        {
            return gameObject;
        }

        private void GunCoolDown()
        {
            if (side == Soldier.Side.Player1)
            {
                if (_timer > 0)
                {
                    _timer -= Time.deltaTime;
                    coolDownImage.fillAmount = 1 - _timer / _laserGunCoolDown;
                    if (_timer <= 0)
                    {
                        _canAttack = true;
                        attackButton.interactable = true;
                        _timer = 0;
                    }
                    else
                    {
                        attackButton.interactable = false;
                        _canAttack = false;
                    }
                }
            }
        }

        public void Cheat()
        {
            print("Cheat");
            _timer = 1;
        }
    }
}