using UnityEngine;

namespace _Scripts.Controller
{
    public class BaseGun : MonoBehaviour
    {
        [SerializeField] private Base playerBase;

        public void Attack()
        {
            playerBase.Attack();
        }
    }
}
