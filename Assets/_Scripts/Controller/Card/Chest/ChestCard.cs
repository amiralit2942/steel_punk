﻿using System.Collections.Generic;
//using System.Runtime.Remoting;
using _Scripts.Controller.Abstract;
using _Scripts.DataHolder;
using _Scripts.Manager;
using _Scripts.Manager.Currencies;
using _Scripts.Model;
using _Scripts.UI.NoticePopUp;
using _Scripts.Util;
using TMPro;
using UnityEngine;

namespace _Scripts.Controller.Card.Chest
{
    public class ChestCard : MonoBehaviour
    {
        //[SerializeField] private GameObject chestTimerPanel;
        [SerializeField] public ChestTimer chestTimer;
        //[SerializeField] private Animator animator;
        
        private OpenChestStruct _contents;
        private int _openedItems = 0;
        public UserChest _userChest;
        public UserChest UserChest => _userChest;
        private float _remainingTime;
        private ChestContentCard _tempChestCard;
        
        
        public void Open()
        {
            print("Tried TO Open Chest");
            _userChest.chest_state = ChestState.ChestEmpty;
            ChestManager.Instance.ChestUnlocked();
            ServerManager.Instance.OpenChest(_userChest.ID, (response) =>
            {
                _contents = response;
                ChestOpeningPanel.Instance.ActivePanel(_userChest.chest_type,_contents);
                ChestManager.Instance.ChestOpened(_userChest.chest_id);
                print("Contents index 0: " + _contents);
                print("blah blah blah");
                Destroy(gameObject,1);
            }, (response) =>
            {
                    print("Opening Chest Failed :" + response);
            });
        }

        public void ChestReady()
        {
            _userChest.chest_state = ChestState.ChestOpened;
            ChestManager.Instance.ChestUnlocked();
        }

        public void UnlockChest()
        {
            if (ChestManager.Instance.CheckCanUnlockChest())
            {
                ServerManager.Instance.UnlockChestPost(_userChest.ID,
                    (response) =>
                    {
                        SafeBox.Instance.userInfo.unlocking_user_chest_id = _userChest.ID;
                        chestTimer.SetTimer(response.remaining_time);
                    }, (statusCode) =>
                    {
                        //TODO: Another Chest Is Already Unlocking
                        print("Unlock Chest: " + statusCode);
                    });
            }
            else
            {
                NoticePopUpPanel.Instance.UnlockChestWithGem(Strings.OpenChestWithGem
                     ,ChestManager.Instance.OpenChestWithGemPrice(),ChestManager.Instance.OpenChestWithGem);
            }
        }
        
        public void OnChestClick()
        {
            switch (_userChest.chest_state)
            {
                case ChestState.ChestEmpty:
                    Destroy(gameObject);
                    break;
                case ChestState.ChestOpened:
                    Open();
                    //SmashBox();
                    break;
                case ChestState.ChestUnlocking:
                    
                    //TODO Show Open With Gem Panel
                    print("Unlock With Gem");
                    break;
                case ChestState.ChestLocked:
                    if (ChestManager.Instance.CheckCanUnlockChest())
                    {
                        UnlockChest();
                        print("check");
                    }
                    else
                    {
                        UnlockChest();
                        print("RIDI");
                        //TODO Show Open With Gem Panel
                    }
                    break;
            }
        }

        
        // public void OpenChest()
        // {
        //     if (_smashedTimes < smashesNeeded) 
        //     {
        //         
        //         //TODO Play Opening Animation
        //         //TODO On Complete:
        //         //SpawnContent(_contents[_openedItems]);
        //         SmashBox();
        //         _smashedTimes++;
        //         //_openedItems++;
        //         return;
        //     }
        //     
        //     if (_openedItems >= _contents.Count)
        //     {
        //         //TODO Show Close Btn
        //         _userChest.chest_state = ChestState.ChestEmpty;
        //         Destroy(gameObject);
        //         return;
        //     }
        //
        //     
        //     contentCounterPanel.SetActive(true);
        //     
        //     if(_openedItems > 0)
        //     {
        //         Destroy(_tempChestCard.gameObject);
        //         SpawnContent(_contents[_openedItems]);
        //         _openedItems++;
        //     }
        //     contentsCounter.text = (_contents.Count - _openedItems).ToString();
        // }
        
        // private void SpawnContent(UserChestPrizeStruct chestContent)
        // {
        //     var content = Instantiate(PrefabHolder.Instance.GetChestContentCard()
        //         , cardSpawnPoint);
        //     content.transform.localPosition = new Vector3(0,0,0);
        //     
        //     switch(chestContent.prizeType)
        //     {
        //         case PrizeType.CoinPrizeType:
        //             content.SetInfo(chestContent.prizeType,chestContent.prizeAmount);
        //             break;
        //         case PrizeType.GemPrizeType:
        //             content.SetInfo(chestContent.prizeType,chestContent.prizeAmount);
        //             break;
        //         case PrizeType.CardPrizeType:
        //             content.SetInfo(chestContent.prizeType,chestContent.prizeAmount,chestContent.cardId);
        //             break;
        //     }
        //     _tempChestCard = content;
        // }
        
        public void SetTimer()
        {
            ServerManager.Instance.GetChestRemainingTime((response) =>
            {
                _remainingTime = response.remaining_time;
                _userChest.chest_state = ChestState.ChestUnlocking;
                chestTimer.SetTimer(_remainingTime);
            }, (response) =>
            {
                print("Getting Chest Remaining Time Failed");
            });
        }

        
        
        
        // private void CollectPrizes()
        // {
        //     foreach (var content in _contents)
        //     {
        //         switch (content.prizeType)
        //         {
        //             case PrizeType.CoinPrizeType :
        //                 CoinManager.Instance.EarnCoin(content.prizeAmount,"Chest");
        //                 break;
        //             case PrizeType.GemPrizeType:
        //                 GemManager.Instance.EarnGem(content.prizeAmount,"Chest","Chest Gem Pack");
        //                 break;
        //             case PrizeType.CardPrizeType:
        //                 ShardManager.Instance.EarnShard(content.cardId,content.prizeAmount);
        //                 break;
        //         }
        //     }
        // }

        public void SetInfo(UserChest userChest)
        {
            _userChest = userChest;
            // ServerManager.Instance.GetChestRemainingTime((chestInfo) =>
            // {
            //     switch (chestInfo.chest_state)
            //     {
            //         case ChestState.ChestEmpty:
            //             Destroy(gameObject);
            //             break;
            //         case ChestState.ChestOpened:
            //             _userChest.chest_state = ChestState.ChestOpened;
            //             chestTimer.AlreadyReady();
            //             break;
            //         case ChestState.ChestUnlocking:
            //             chestTimer.SetTimer(chestInfo.remaining_time);
            //             break;
            //         default:
            //             print("Chest: "+_userChest.chest_id +" "+_userChest.chest_state);
            //             break;
            //     }
            // }, (response) =>
            // {
            //     print("Chest Inquiry: " + response);
            // });
            if (_userChest.chest_state == ChestState.ChestOpened)
            {
                chestTimer.AlreadyReady();
            }
        }

        public void StopAnimation()
        {
            //animator.speed = 0;
        }
        
    }
}