﻿using _Scripts.DataHolder;
using _Scripts.Model;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace _Scripts.Controller.Card.Chest
{
    public class ChestContentCard : MonoBehaviour
    { 
        [SerializeField] private Image _cardImage;
        [SerializeField] private TMP_Text _amountText;

        // public void SetInfo(Sprite cardSprite , int amount)
        // {
        //     _cardImage.sprite = cardSprite;
        //     _amountText.text = amount.ToString();
        // }

        public void SetInfo(PrizeType prizeType,int amount,uint cardID = 0)//UserChestPrizeStruct userChestPrizeStruct)
        {
            print("Prize Type: "+prizeType);
            switch (prizeType)
            {
                case PrizeType.CoinPrizeType :
                    _cardImage.sprite = PrefabHolder.Instance.GetChestContentCardSprite(prizeType);
                    break;
                case PrizeType.GemPrizeType:
                    _cardImage.sprite = PrefabHolder.Instance.GetChestContentCardSprite(prizeType);
                    break;
                case PrizeType.CardPrizeType :
                    _cardImage.sprite = PrefabHolder.Instance.GetChestContentCardSprite(prizeType,cardID);
                    break;
            }
            _amountText.text = "X " + amount;
        }

        // public void SetInfo(PrizeType prizeType,int amount,uint cardID = 0)
        // {
        //     switch (prizeType)
        //     {
        //         case PrizeType.CoinPrizeType :
        //             _cardImage.sprite = PrefabHolder.Instance.GetChestContentCardSprite(prizeType);
        //             break;
        //         case PrizeType.GemPrizeType:
        //             _cardImage.sprite = PrefabHolder.Instance.GetChestContentCardSprite(prizeType);
        //             break;
        //         case PrizeType.CardPrizeType :
        //             _cardImage.sprite = PrefabHolder.Instance.GetChestContentCardSprite(prizeType,cardID);
        //             break;
        //     }
        //     _amountText.text = amount.ToString();
        // }

    }
}
