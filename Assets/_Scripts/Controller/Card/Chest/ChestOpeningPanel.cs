﻿using System;
using System.Collections.Generic;
using _Scripts.Controller.Card.Deck;
using _Scripts.DataHolder;
using _Scripts.Manager;
using _Scripts.Manager.Currencies;
using _Scripts.Model;
using _Scripts.UI;
using _Scripts.Util;
using DG.Tweening;
using UnityEngine;
using Util;

namespace _Scripts.Controller.Card.Chest
{
    public class ChestOpeningPanel : MonoBehaviourSingleton<ChestOpeningPanel>
    {
        [SerializeField] private RectTransform openingChestPanel;
        [SerializeField] private float openingPanelDur;
        [SerializeField] private Transform chestSpawnPoint;
        
        [SerializeField] private RectTransform contentsSpawnPoint;
        [SerializeField] private CanvasGroup contentsCanvas;
        
        [SerializeField] private GameObject bronzeChest;
        [SerializeField] private GameObject goldenChest;
        [SerializeField] private GameObject whiteChest;
        private UnlockedChest _chest;
        private GameObject _chestPrefab;
        private List<GameObject> _prizeCards = new List<GameObject>();

        private void Start()
        {
            openingChestPanel.anchoredPosition = new Vector2(0,-1080);
            openingChestPanel.gameObject.SetActive(false);
        }

        public void ActivePanel(ChestType chestCard,OpenChestStruct prizes)
        {
            openingChestPanel.gameObject.SetActive(true);
            openingChestPanel.anchoredPosition = new Vector2(0,-1080);
            contentsSpawnPoint.anchoredPosition = new Vector2(0,-60);
            contentsCanvas.alpha = 0;
            SpawnPrizes(prizes);
            
            switch (chestCard)
            {
                case ChestType.CommonChest: 
                    _chestPrefab = Instantiate(bronzeChest, chestSpawnPoint);
                    break;
                case ChestType.RareChest:
                    _chestPrefab = Instantiate(goldenChest, chestSpawnPoint);
                    break;
                case ChestType.EpicChest:
                    _chestPrefab =Instantiate(whiteChest, chestSpawnPoint);
                    break;
            }
            
            openingChestPanel.DOAnchorPos(new Vector2(), openingPanelDur).SetDelay(1.5f).OnComplete(() =>
            {
                contentsSpawnPoint.DOAnchorPos(new Vector2(), 1f).OnUpdate(() =>
                {
                    contentsCanvas.DOFade(1, 1f);
                });
            });
            //Get Chest Prefab
            //Chest Scale : 85 
        }
        
        public void SpawnPrizes(OpenChestStruct prizes)
        {
            // foreach (var content in prizes)
            // {
            //     var contentCard = Instantiate(PrefabHolder.Instance.GetChestContentCard(), contentsSpawnPoint);
            //     contentCard.SetInfo(content);
            //     _prizeCards.Add(contentCard.gameObject);
            // }
            CollectPrizes(prizes.user);
            if (prizes.chestReward.coinAmount > 0)
            {
                var contentCard = Instantiate(PrefabHolder.Instance.GetChestContentCard()
                    , contentsSpawnPoint);
                contentCard.
                    SetInfo(PrizeType.CoinPrizeType,prizes.chestReward.coinAmount);
            }
            if (prizes.chestReward.gemAmount > 0)
            {
                var contentCard = Instantiate(PrefabHolder.Instance.GetChestContentCard()
                    , contentsSpawnPoint);
                contentCard.
                    SetInfo(PrizeType.GemPrizeType,prizes.chestReward.coinAmount);
            }
            if (prizes.chestReward.cardAmount > 0)
            {
                var contentCard = Instantiate(PrefabHolder.Instance.GetChestContentCard()
                    , contentsSpawnPoint);
                contentCard.
                    SetInfo(PrizeType.CardPrizeType,prizes.chestReward.coinAmount,prizes.chestReward.cardId);
                print("1 Card ID : " + prizes.chestReward.cardId);
                DeckPanel.Instance.NewCard(prizes.chestReward.cardId);
                
            }
            
        }

        public void ClosePanel()
        {
            openingChestPanel.DOAnchorPos(new Vector2(0, -1080), openingPanelDur).OnComplete(() =>
            {
                CurrencyUIManager.Instance.UpdateUI();
                Destroy(_chest.gameObject);
                foreach (var prizeCard in _prizeCards)
                {
                    Destroy(prizeCard);
                }
            });
        }

        public void CollectPrizes(UserInfo userInfo)
        {
            // foreach (var prize in prizes)
            // {
            //     switch (prize.prizeType)
            //     {
            //         case PrizeType.CoinPrizeType:
            //             CoinManager.Instance.EarnCoin(prize.prizeAmount,Strings.FromChestPanel,Strings.ItemTypeCoin);
            //             break;
            //         case PrizeType.GemPrizeType:
            //             GemManager.Instance.EarnGem(prize.prizeAmount,Strings.FromChestPanel,Strings.ItemTypeGem);
            //             break;
            //         case PrizeType.CardPrizeType:
            //             ShardManager.Instance.EarnShard(prize.cardId,prize.prizeAmount);
            //             break;
            //     }
            // }
            SafeBox.Instance.userInfo = userInfo;
            var t = 0;
            DOTween.To(() => t, (v) => t = v, 1, 3).OnComplete(() =>
            {
                CurrencyUIManager.Instance.UpdateUI();
            });
            
        }
        



    }
}