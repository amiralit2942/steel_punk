﻿using System.Collections.Generic;
using _Scripts.Model;
using UnityEngine;

namespace _Scripts.Controller.Card.Chest
{
    public class UnlockedChest : MonoBehaviour
    {
        [SerializeField] private Animation animation;
        [SerializeField] private List<AnimationClip> animations;
        [SerializeField] private ChestType chestType;
        private List<UserChestPrizeStruct> _userChestPrizes = new List<UserChestPrizeStruct>();
        
        
        public void SetInfo(List<UserChestPrizeStruct> prizes)
        {
            _userChestPrizes = prizes;
        }

        private int _clickCounter = 0;
        
        public void OnClick()
        {
            if (_clickCounter + 1 < animations.Count)
            {
                animation.clip = animations[_clickCounter];
                animation.Play();
                _clickCounter++;
            }
            else if(_clickCounter + 1 == animations.Count)
            {
                //Spawn Prizes
            }
            else if(_clickCounter + 1 > animations.Count)
            {
                //Close Panel
            }
        }
    }
}