﻿using System;
using System.Collections.Generic;
using System.Linq;
//using System.Diagnostics.Eventing.Reader;
using _Scripts.Controller.Spells;
using _Scripts.DataHolder;
using _Scripts.Manager;
using _Scripts.Manager.Currencies;
using _Scripts.Model;
using _Scripts.UI.Equips;
using _Scripts.UI.NoticePopUp;
using _Scripts.Util;
using Newtonsoft.Json;
using UnityEngine;
using UnityEngine.UI;
using Util;
using Random = UnityEngine.Random;

namespace _Scripts.Controller.Card.Deck
{
    // NEWWWWWW
    public class DeckPanel : MonoBehaviourSingleton<DeckPanel>
    {
        [SerializeField] private GameObject ownedSoldiersPanel;
        [SerializeField] private GameObject ownedSpellsPanel;
        [SerializeField] private List<SoldierDeckSlot> soldiersDeckSlots;
        [SerializeField] private List<SpellDeckSlot> spellsDeckSlots;
        public List<SoldierDeckCard> _soldierDeckCards;
        private List<SpellDeckCard> _spellDeckCards;
        [SerializeField] private Button addBtn;
        [SerializeField] private Button removeBtn;

        private UserCard _selectedSoldier;
        private UserSpell _selectedSpell;
        enum PanelState
        {
            SoldiersPanel,
            SpellPanel
        }

        private void Awake()
        {
            _soldierDeckCards = new List<SoldierDeckCard>();
            _spellDeckCards = new  List<SpellDeckCard>();
        }

        private void Start()
        {
            SpawnCards();
        }

        public void SpawnCards()
        {
            //Spawning All The Cards We Got
            foreach (var cards in SafeBox.Instance.userInfo.cards)
            {
                if (cards.card_id != 1)
                {
                    var deckCard = Instantiate(PrefabHolder.Instance.GetSoldierDeckCard(), ownedSoldiersPanel.transform);
                    deckCard.SetInfo(cards);   
                    _soldierDeckCards.Add(deckCard);
                }
            }
            // Moving Cards That They Are In The Deck Already
            if (SafeBox.Instance.userInfo.deck_cards != null)
            {
                for (int i = 0; i < SafeBox.Instance.userInfo.deck_cards.Count; i++)
                {
                    var userCard = SafeBox.Instance.userInfo.deck_cards[i];
                    if (userCard.card_id != 1)
                    {
                        //Creat A Card In Deck Panel
                        FindEmptySoldierSlot().SetInfo(userCard);
                        //Destroy The Card In Owned Panel
                        var deckCard = _soldierDeckCards.Find(x =>
                            x._userCard.card_id == userCard.card_id);
                        Destroy(deckCard.gameObject);
                        _soldierDeckCards?.Remove(deckCard);
                    }
                }
            }

            //Then We Do The Same Thing With Spells
            if (SafeBox.Instance.userInfo.spells != null)
            {
                foreach (var spell in SafeBox.Instance.userInfo.spells)
                {
                    var deckCard = Instantiate
                        (PrefabHolder.Instance.GetSpellDeckCard(), ownedSpellsPanel.transform);
                    deckCard.SetInfo(spell);
                    _spellDeckCards.Add(deckCard);
                }    
            }

            if (SafeBox.Instance.userInfo.deck_spells?.Count > 0)
            {
                for (int i = 0; i < SafeBox.Instance.userInfo.deck_spells.Count; i++)
                {
                    var userCard = SafeBox.Instance.userInfo.deck_spells[i];
                
                    FindEmptySpellSlot().SetInfo(userCard);
                    var deckCard=_spellDeckCards.Find(x => 
                        x.UserSpell.spellId == userCard.spellId);
                    if (!deckCard.HasMoreThanOne())
                    {
                        _spellDeckCards.Remove(deckCard);
                        Destroy(deckCard.gameObject);   
                    }
                    else
                    {
                        deckCard.DecreaseSpell();
                    }
                }   
            }

        }

        public void ReloadCards()
        {
            DestroyCards();
            SpawnCards();
        }

        public void DestroyCards()
        {
            if (_spellDeckCards.Count > 0)
            {
                foreach (var spellDeckCard in _spellDeckCards)
                {
                    Destroy(spellDeckCard.gameObject);
                }
            }

            if (_soldierDeckCards.Count > 0)
            {
                foreach (var soldierDeckCard in _soldierDeckCards)
                {
                    Destroy(soldierDeckCard.gameObject);
                }
            }
            
            foreach (var spellsDeckSlot in spellsDeckSlots)
            {
                spellsDeckSlot.Clear();  
            }

            foreach (var soldierDeckSlot in soldiersDeckSlots)
            {
                soldierDeckSlot.DisableUi();
            }
        }

        public void AddToDeckBtn()
        {
            if (_selectedSoldier != null)
            {
                AddToDeck(_selectedSoldier);
                DeckInfoPanel.Instance.DeActivePanel();
            }
            else if(_selectedSpell != null)
            {
                AddToDeck(_selectedSpell);
                DeckInfoPanel.Instance.DeActivePanel();
            }
        }
        
        public void AddToDeck(UserCard userCard)
        {
            //We Add The Selected Soldier Then Send The New Deck To The Server
            if (!SafeBox.Instance.userInfo.deck_cards.Contains(userCard))
            {
                var cardCreated = false;
                var soldiersSlutCount = soldiersDeckSlots.Count;
                SafeBox.Instance.userInfo.deck_cards.Add(userCard);
                //TODO: Post To Server
                ServerManager.Instance.PostDeck(SafeBox.Instance.userInfo.deck_cards, 
                    (response) =>
                    {
                        var slot = FindEmptySoldierSlot();
                        slot.SetInfo(userCard);
                        var deckCard =_soldierDeckCards
                            .Find(x => x.UserCard == userCard);
                        Destroy(deckCard.gameObject);
                        cardCreated = true;
                        print("Deck Changed");
                    }, (response) =>
                    {
                        NoticePopUpPanel.Instance.NewNotice().WithContent(Strings.ChangingDeckFailed).WithRejectButton();
                        SafeBox.Instance.userInfo.deck_cards.Remove(userCard);
                        print("Deck post failed : " + response);
                    });
            }
        }
        
        public void AddToDeck(UserSpell userSpell)
        {
            print(JsonConvert.SerializeObject(SafeBox.Instance.userInfo.deck_spells));
            if (SafeBox.Instance.userInfo.deck_spells == null)
            {
                SafeBox.Instance.userInfo.deck_spells = new List<UserSpell>();
            }
            //Exactly like add to solider deck method
            if (SafeBox.Instance.userInfo.deck_spells.Any(t=>  t.spellId == userSpell.spellId) == false)
            {
                var cardCreated = false;
                var spellsSlotCount= spellsDeckSlots.Count;
                SafeBox.Instance.userInfo.deck_spells.Add(userSpell);
                
                //TODO Change post method
                ServerManager.Instance.PostSpellDeck(SafeBox.Instance.userInfo.deck_spells, 
                    () =>
                    {
                        var slot = FindEmptySpellSlot();
                        slot.SetInfo(userSpell);
                        var card = _spellDeckCards
                            .Find(x => x.UserSpell == userSpell);
                        if(!card.HasMoreThanOne())
                        {
                            _spellDeckCards.Remove(card);
                            Destroy(card.gameObject);
                        }
                        else
                        {
                            card.DecreaseSpell();
                        }
                        cardCreated = true;
                        print("Deck Changed");
                    }, (response) =>
                    {
                        NoticePopUpPanel.Instance.NewNotice().WithContent(Strings.ChangingDeckFailed).WithRejectButton();
                        SafeBox.Instance.userInfo.deck_spells.Remove(userSpell);
                        print("Deck post failed : " + response);
                    });
            }
            else
            {
                NoticePopUpPanel.Instance.NewNotice().WithContent(Strings.SpellAlreadyInDeck).WithRejectButton();
            }
        }
        
        public void RemoveFromDeckBtn()
        {
            if (_selectedSoldier != null)
            {
                RemoveFromDeck(_selectedSoldier);
                DeckInfoPanel.Instance.DeActivePanel();
            }
            else if(_selectedSpell != null)
            {
                RemoveFromDeck(_selectedSpell);
                DeckInfoPanel.Instance.DeActivePanel();
            }
        }

        public void RemoveFromDeck(UserCard userCard)
        {
            // if (SafeBox.Instance.userInfo.deck_cards.Count == 1)
            // {
            //     PopUpManager.Instance.EmptyDeck();
            //     return;
            // }
            //We Remove The Selected Soldier Then Send The New Deck To The Server
            SafeBox.Instance.userInfo.deck_cards?.Remove(userCard);
            ServerManager.Instance.PostDeck(SafeBox.Instance.userInfo.deck_cards, (response) =>
            {
                var deckCard = Instantiate(PrefabHolder.Instance.GetSoldierDeckCard()
                    , ownedSoldiersPanel.transform);
                deckCard.SetInfo(userCard);
                //SafeBox.Instance.userInfo = response;
                _soldierDeckCards?.Add(deckCard);
                soldiersDeckSlots?.Find(x => x._userCard == userCard).RemoveFromDeck();
            }, (response) =>
            {
                NoticePopUpPanel.Instance.NewNotice().WithContent(Strings.ChangingDeckFailed).WithRejectButton();
                SafeBox.Instance.userInfo.deck_cards.Add(userCard);
                print("Deck Post Failed : " + response);
            });
        }
        
        public void RemoveFromDeck(UserSpell userSpell)
        {
            //Exactly like remove Soldier Method 
            SafeBox.Instance.userInfo.deck_spells?.Remove(userSpell);
            ServerManager.Instance.PostSpellDeck(SafeBox.Instance.userInfo.deck_spells, () =>
            {
                var spellDeckCard = _spellDeckCards?.Find(x => x.UserSpell.spellId == userSpell.spellId);
                if (spellDeckCard == null)
                {
                    var card = Instantiate(PrefabHolder.Instance.GetSpellDeckCard()
                        , ownedSpellsPanel.transform);
                    card.SetInfo(userSpell);
                    _spellDeckCards.Add(card);
                }
                else
                {
                    spellDeckCard.AddSpell();
                }
                print("Null ID: " + userSpell.spellId);
                spellsDeckSlots?.Find(x => x.UserSpell.spellId == userSpell.spellId).RemoveFromDeck();
                
            }, (response) =>
            {
                NoticePopUpPanel.Instance.NewNotice().WithContent(Strings.ChangingDeckFailed).WithRejectButton();
                SafeBox.Instance.userInfo.deck_spells.Add(userSpell);
            });
        }

        public void ActiveInfoPanel(UserCard soldier,Action upgrade)
        {
            DeckInfoPanel.Instance.ActivePanel(soldier,upgrade);
            _selectedSoldier = soldier;
            _selectedSpell = null;
            var soldierInDeck = SafeBox.Instance.userInfo.deck_cards?
                .Find(x => x.card_id == soldier.card_id);
            if (soldierInDeck != null)
            {
                removeBtn.gameObject.SetActive(true);
                addBtn.gameObject.SetActive(false);
            }
            else
            {
                removeBtn.gameObject.SetActive(false);
                addBtn.gameObject.SetActive(true);
            }
        }
        
        
        
        public void ActiveInfoPanel(UserSpell selectedSpell,bool isFromSlotCard)
        {
            DeckInfoPanel.Instance.ActivePanel(selectedSpell);
            _selectedSoldier = null;
            _selectedSpell = selectedSpell;
            var spell = SafeBox.Instance.userInfo.deck_spells?.Find(x => x.spellId == selectedSpell.spellId);
            if (isFromSlotCard)
            {
                removeBtn.gameObject.SetActive(true);
                addBtn.gameObject.SetActive(false);
            }
            else
            {
                removeBtn.gameObject.SetActive(false);
                addBtn.gameObject.SetActive(true);
            }
        }
        

        public void Upgrade()
        {
            var info = CardsManager.Instance.GETSoldierInfo(_selectedSoldier.card_id);
            if (CoinManager.Instance.GetCoin() >= info.coin_price[_selectedSoldier.level] &&
                ShardManager.Instance.CheckShard(_selectedSoldier.card_id,_selectedSoldier.shards))
            {
                ServerManager.Instance.ItemShopPurchase(_selectedSoldier.card_id, (userInfo) =>
                {
                    SafeBox.Instance.userInfo = userInfo;
                    // CoinManager.Instance.BuyItem(info.coin_price[_selectedSoldier.level],"Soldier Upgrade"
                    //     ,info.soldier_type.ToString(),"Equip Panel");
                    XpManager.Instance.EarnXp(info.xp_reward[_selectedSoldier.level]);
                    //ShardManager.Instance.UseShard(info.ID,info.required_shard[_selectedSoldier.level]);
                    //soldiersDeckSlots.Find(x => x.CardsInfo.ID == info.ID).SetShardSlider();
                }, (response) =>
                {
                    //Show Failed PopUp
                });
            }
        }

        private SoldierDeckSlot FindEmptySoldierSlot()
        {
            foreach (var slot in soldiersDeckSlots)
            {
                if (slot.isEmpty)
                {
                    return slot;
                }
            }
            var randomSlot = soldiersDeckSlots[Random.Range(0, soldiersDeckSlots.Count)];
            RemoveFromDeck(randomSlot._userCard);
            return randomSlot;
        }

        private SpellDeckSlot FindEmptySpellSlot()
        {
            foreach (var slot in spellsDeckSlots)
            {
                if (slot.isEmpty)
                {
                    return slot;
                }
            }

            var randomSlot = spellsDeckSlots[Random.Range(0, spellsDeckSlots.Count)];
            RemoveFromDeck(randomSlot.UserSpell);
            return randomSlot;
        }

        public void StartGameBtn()
        {
            if (SafeBox.Instance.userInfo.deck_cards.Count >= 3)
            {
                //Start The Game
                PanelManager.Instance.OnPlayClick();
            }
            else
            {
                //Test
                PanelManager.Instance.OnPlayClick();
                //Show PopUp
                //NoticePopUpPanel.Instance.NewNotice().WithContent(Strings.NeedMoreSoldiers).WithRejectButton();
            }
        }

        public void NewCard(uint newCardId)
        {
            var soldierDeckCard = _soldierDeckCards?.Find(x => x.UserCard.card_id == newCardId);
            if (soldierDeckCard != null)
            {
                soldierDeckCard.UpdateCard(newCardId);
                return;
            }
            
            var newUserCard = SafeBox.Instance.userInfo.cards.Find(x => x.card_id == newCardId);
            var deckCard = Instantiate(PrefabHolder.Instance.GetSoldierDeckCard(), ownedSoldiersPanel.transform);
            print("Card ID : " + newCardId);
            deckCard.SetInfo(newUserCard);
            deckCard.UpdateCard(newCardId);
            _soldierDeckCards?.Add(deckCard);
        }
        
        public void NewCard(UserSpell newSpell)
        {
            var spell = _spellDeckCards.Find(x => x.UserSpell.spellId == newSpell.spellId);
            if (spell)
            {
                print("SpellFounded");
                spell.AddSpell();
                return;
            }
            var deckCard = Instantiate(PrefabHolder.Instance.GetSpellDeckCard()
                , ownedSpellsPanel.transform);
            deckCard.SetInfo(newSpell);
            _spellDeckCards.Add(deckCard);
        }
        
    }
}