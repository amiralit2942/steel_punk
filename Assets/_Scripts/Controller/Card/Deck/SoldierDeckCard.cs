﻿using _Scripts.DataHolder;
using _Scripts.Manager;
using _Scripts.Manager.Currencies;
using _Scripts.Model;
using _Scripts.UI;
using _Scripts.UI.NoticePopUp;
using _Scripts.Util;
using UnityEngine;
using UnityEngine.UI;

namespace _Scripts.Controller.Card.Deck
{
    public class SoldierDeckCard : MonoBehaviour
    {
        // NEW !!!
        [SerializeField] private Image image;
        
        private CardsInfo _cardsInfo;
        public CardsInfo CardsInfo => _cardsInfo;
        public UserCard _userCard;
        public UserCard UserCard => _userCard;

        
        public void SetInfo(UserCard userCard)
        {
            image.gameObject.SetActive(true);
            _cardsInfo = CardsManager.Instance.GETSoldierInfo(userCard.card_id);
            _userCard = userCard;
            image.sprite = PrefabHolder.Instance.GetCardImage(_cardsInfo.soldier_type);
        }

        public void OnClick()
        {
            DeckPanel.Instance.ActiveInfoPanel(_userCard,Upgrade);
        }

        private void Upgrade()
        {
            var level = UserCard.level;
            var price = _cardsInfo.coin_price[level];
            var requiredShard = _cardsInfo.required_shard[level];
            if (UserCard.shards >= requiredShard && CoinManager.Instance.CheckCoin(price))
            {
                ServerManager.Instance.ItemShopPurchase(_userCard.card_id, (userInfo) =>
                {
                    SafeBox.Instance.userInfo = userInfo;
                    // CoinManager.Instance.BuyItem
                    //     (price,Strings.ItemTypeSoldier,_cardsInfo.soldier_type.ToString(),Strings.FromDeckPanel);
                    //ShardManager.Instance.UseShard(_userCard.card_id,requiredShard);
                    _userCard = SafeBox.Instance.userInfo.cards.Find(x => x.card_id == _userCard.card_id);
                }, (response) =>
                {
                    NoticePopUpPanel.Instance.NewNotice().WithContent(Strings.UpgradeFailed).WithRejectButton();
                    print("Soldier Upgrade Failed: " + response);
                });
            }
        }

        public void UpdateCard(uint id)
        {
            _userCard = SafeBox.Instance.userInfo.cards.Find(x => x.card_id == id);
        }
        

        #region DragAndDrop

        // private float _diffX;
        // private float _diffY;
        // public void OnBeginDrag(PointerEventData eventData)
        // {
        //     _diffX = eventData.pointerCurrentRaycast.worldPosition.x - transform.position.x;
        //     _diffY = eventData.pointerCurrentRaycast.worldPosition.y - transform.position.y;
        // }

        // public void OnEndDrag(PointerEventData eventData)
        // {
        //     if (transform.localPosition.y > 200)
        //     {
        //         transform.parent = EquipPanel.Instance.EquipedSoldiersPanel;
        //         
        //     }
        //     else
        //     {
        //         transform.parent = EquipPanel.Instance.OwnedSoldiersPanel;
        //     }
        //     transform.localPosition = new Vector3(0,0,0);
        // }
        //
        // public void OnDrag(PointerEventData eventData)
        // {
        //      var x = eventData.pointerCurrentRaycast.worldPosition.x - _diffX;
        //      var y = eventData.pointerCurrentRaycast.worldPosition.y - _diffY;
        //      transform.position = new Vector3(x,y,0);
        //     // transform.position.x = x;
        //     // transform.position = eventData.pointerCurrentRaycast.worldPosition;
        // }
        #endregion
        // public void OnEndDrag(PointerEventData eventData)
        // {
        //     if (_rectTransform.anchoredPosition == EquipPanel.Instance.EquipedSoldiersPanel.anchoredPosition)
        //     {
        //         EquipPanel.Instance.AddToDeck(transform);
        //         //Server Method
        //     }
        //     else
        //     {
        //         //Remove From Deck
        //         EquipPanel.Instance.RemoveFromDeck(transform);
        //     }
        // }
    }
}