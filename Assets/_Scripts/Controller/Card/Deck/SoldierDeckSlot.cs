﻿using System;
using _Scripts.Controller.Abstract;
using _Scripts.DataHolder;
using _Scripts.Manager;
using _Scripts.Manager.Currencies;
using _Scripts.Model;
using _Scripts.UI;
using _Scripts.UI.NoticePopUp;
using _Scripts.Util;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace _Scripts.Controller.Card.Deck
{
    
    public class SoldierDeckSlot : MonoBehaviour
    {
        /*[SerializeField] private Slider shardSlider;*/
        [SerializeField] private Image soldierImage;
        [SerializeField] private TMP_Text shardText;
        
        //[SerializeField] private Button upgradeButton;
        public UserCard _userCard;
        public UserCard UserCard => _userCard;
        private CardsInfo _cardsInfo;
        public CardsInfo CardsInfo => _cardsInfo;

        public bool isEmpty = true;

        private void Awake()
        {
            //upgradeButton.gameObject.SetActive(false);
            isEmpty = true;
        }

        public void SetInfo(UserCard userCard)
        {
            _userCard = userCard;
            _cardsInfo = CardsManager.Instance.GETSoldierInfo(userCard.card_id);
            soldierImage.sprite = PrefabHolder.Instance.GetCardImage
                (_cardsInfo.soldier_type);
            shardText.text = $"{_userCard.shards} / {_cardsInfo.required_shard[_userCard.level]}";
            isEmpty = false;
            EnableUi();

        }

        // public void SetShardSlider()
        // {
        //     shardSlider.gameObject.SetActive(true);
        //     shardSlider.value = _userCard.shards;
        //     shardSlider.maxValue = _cardsInfo.required_shard[_userCard.level];
        //     if (shardSlider.value == shardSlider.maxValue)
        //     {
        //       //  upgradeButton.gameObject.SetActive(true);
        //     }
        // }

        private void Upgrade()
        {
            var level = UserCard.level;
            var price = _cardsInfo.coin_price[level];
            var requiredShard = _cardsInfo.required_shard[level];
            if (UserCard.shards >= requiredShard && CoinManager.Instance.CheckCoin(price))
            {
                ServerManager.Instance.ItemShopPurchase(_userCard.card_id, (userInfo) =>
                {
                    SafeBox.Instance.userInfo = userInfo;
                    // CoinManager.Instance.BuyItem
                    //     (price,Strings.ItemTypeSoldier,_cardsInfo.soldier_type.ToString(),Strings.FromDeckPanel);
                    _userCard.level++;
                    //TODO: Get A New UseInfo From Server
                    _userCard.shards -= requiredShard;
                    //ShardManager.Instance.UseShard(_userCard.card_id,requiredShard);
                    CurrencyUIManager.Instance.UpdateUI();
                    OnClick();
                }, (response) =>
                {
                    NoticePopUpPanel.Instance.NewNotice().WithContent(Strings.UpgradeFailed).WithRejectButton();
                    print("Soldier Upgrade Failed: " + response);
                });
            }
        }
        
        public void RemoveFromDeck()
        {
            isEmpty = true;
            DisableUi();
        }

        public void DisableUi()
        {
            soldierImage.sprite = null;
            _userCard = null;
            soldierImage.gameObject.SetActive(false);
            //shardSlider.gameObject.SetActive(false);
            shardText.gameObject.SetActive(false);
        }

        public void EnableUi()
        {
            soldierImage.gameObject.SetActive(true);
            //shardSlider.gameObject.SetActive(true);
            shardText.gameObject.SetActive(true);
        }

        public void OnClick()
        {
            if (!isEmpty)
            {
                DeckPanel.Instance.ActiveInfoPanel(_userCard,Upgrade);   
            }
        }
    }
}
