﻿using System;
using _Scripts.DataHolder;
using _Scripts.Manager;
using _Scripts.Model;
using _Scripts.UI;
using UnityEngine;
using UnityEngine.UI;

namespace _Scripts.Controller.Card.Deck
{
    public class SpellDeckSlot : MonoBehaviour
    {
        [SerializeField] private Image spellImage;
        [SerializeField] private UserSpell _userSpell;
        public UserSpell UserSpell => _userSpell;
        public bool isEmpty = true;

        

        public void SetInfo(UserSpell userSpell)
        {
            _userSpell = userSpell;
            var spellType = SafeBox.Instance.spellsInfo
                .Find(x => x.id == _userSpell.spellId).spell_type;
            spellImage.gameObject.SetActive(true);
            spellImage.sprite = PrefabHolder.Instance?.GetSpellImageHolderAsset(spellType);
            isEmpty = false;
        }

        public void OnClick()
        {
            if (!isEmpty)
            {
                DeckPanel.Instance.ActiveInfoPanel(_userSpell,true);   
            }
        }
        
        public void RemoveFromDeck()
        {
            Clear();
        }

        public void Clear()
        {
            spellImage.sprite = null;
            spellImage.gameObject.SetActive(false);
            _userSpell = null;
            isEmpty = true;
        }
        
        
        
    }
}
