﻿using System;
using _Scripts.Manager;
using _Scripts.Manager.Currencies;
using _Scripts.Model;
using _Scripts.UI.NoticePopUp;
using _Scripts.Util;
using CafeBazaar.Billing;
using Manager;
using TMPro;
using UnityEngine;
using UnityEngine.UI;
using Purchase = CafeBazaar.Billing.Purchase;

namespace _Scripts.Controller.Card
{
    public class ProductCard : MonoBehaviour
    {
        [SerializeField] private TMP_Text amountText;
        [SerializeField] private TMP_Text priceText;
        [SerializeField] private TMP_Text nameText;
        [SerializeField] private ShopItems _contentInfo;
        public ShopItems ContentInfo => _contentInfo;

        
        public void SetInfo(ShopItems shopItems)
        {
            _contentInfo = shopItems;
            amountText.text = _contentInfo.reward_amount.ToString();
            ChangeName();
            Localization.OnChangeLanguage += ChangeName;
        }

        private void OnDisable()
        {
            Localization.OnChangeLanguage -= ChangeName;
        }

        public void ChangeName()
        {
            priceText.text = string.Format(Localization.Get("price"), _contentInfo.price.ToString());
        }
        

        public void Click()
        {
            NoticePopUpPanel.Instance.NewNotice().WithContent(Strings.BuyCurrency)
                .WithAcceptButton(Buy).WithRejectButton(); //.WithRejectButton("نه");
        }

        public void Buy()
        {
            if (_contentInfo.price_type == ShopItems.PricesType.PriceGem)
            {
                print("On Mouse Down Check");
                if (GemManager.Instance.CheckGem(_contentInfo.price))
                {
                    
                    print("Has The Price");
                    ServerManager.Instance.ShopPurchase(_contentInfo.sku,
                        (SystemInfo.deviceUniqueIdentifier + DateTime.Now).ToString(),"", () =>
                    {
                        GemManager.Instance.SpendGem(_contentInfo.price,"Store Gem Pack",_contentInfo.name,"Store");
                        CoinManager.Instance.EarnCoin(_contentInfo.reward_amount,"Store","Store Coin pack",_contentInfo.name);
                        print("Coin Earned");
                    }, (statusCode) =>
                    {
                        print(statusCode);
                        print("Doesnt Have The Price");
                    });
                    
                }
                else
                {
                    NoticePopUpPanel.Instance.NewNotice().WithContent(Strings.NotEnoughGem).WithRejectButton();
                }    
            }
            else if (_contentInfo.price_type == ShopItems.PricesType.PriceMoney)
            {
                print("Gem Button");
                BazaarBilling.Purchase(_contentInfo.sku, (purchaseResult) =>
                {
                    if (purchaseResult.Successful)
                    {
                        Purchase purchase = purchaseResult.Body;
                        
                        ServerManager.Instance.ShopPurchase(_contentInfo.sku,purchase.PurchaseToken,"", () =>
                        {
                            GemManager.Instance.EarnGem(_contentInfo.reward_amount,"Store","Store Gem Pack");
                            BazaarBilling.Consume(_contentInfo.sku , (consumeResult) =>
                            {
                            
                            });    
                        }, (response) =>
                        {
                            //Call PopUp
                            NoticePopUpPanel.Instance.NewNotice().WithContent(Strings.PurchaseFailed)
                                .WithRejectButton();
                        });
                        
                    }
                });
                
            }
            /*else if (shop_type == ShopItems.ShopType.PriceCoin)
            {
                print("Item Button");
                var gemPrice = CoinManager.Instance.ConvertCoinToGem(price);
                if (CoinManager.Instance.GetCoin() >= price)
                {
                   // PanelManager.Instance.AcceptBuyingItem(transform , price);
                    ServerManager.Instance.ItemShopPurchase(id , () =>
                    {
                        CoinManager.Instance.BuyItem(price);
                        PanelManager.Instance.SetCurrencies();
                        ItemStoreManager.Instance.CreatUpgradeCard(this);
                        
                        //Moving Purchased Card From Cards To Buy to Owned Cards 
                        var purchasedCard = SafeBox.Instance.cardsToBuy.Find(x => x.ID == id);
                        SafeBox.Instance.cardsToBuy.Remove(purchasedCard);
                        SafeBox.Instance.ownedCards.Add(purchasedCard);
                        Destroy(gameObject);
                        
                    } , (response) =>
                    {
                        PanelManager.Instance.SetItemShopPurchaseErrorPanel(response);
                    });
                    
                }
                else if (CoinManager.Instance.GetCoin() <= price && CoinManager.Instance.ConvertCoinToGem(price) >= GemManager.Instance.GetGem())
                {
                    PanelManager.Instance.NotEnoughCoin(gemPrice,true);
                }
            }*/
        }
    }
}
