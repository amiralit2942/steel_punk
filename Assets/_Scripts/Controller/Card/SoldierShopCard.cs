﻿using System;
using _Scripts.DataHolder;
using _Scripts.Manager;
using _Scripts.Manager.Currencies;
using _Scripts.Model;
using _Scripts.UI;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace _Scripts.Controller.Card
{
    public class SoldierShopCard : MonoBehaviour
    {
        [SerializeField] private Image image;
        //[SerializeField] private TMP_Text name;
        [SerializeField] private TMP_Text price;
        private UserCard _userCard;
        private CardsInfo _soldierInfo;


        public void OnClick()
        {
            PopUpManager.Instance.BuySoldier(this);
        }
        
        public void Buy()
        {
            print("Method Deleted");
            // if (CoinManager.Instance.CheckCoin(_soldierInfo.coin_price[0]))
            // {
            //     ServerManager.Instance.ItemShopPurchase(_soldierInfo.ID, () =>
            //     {
            //         CardsManager.Instance.EarnsSoldier(_userCard);
            //         //ShopPanel.Instance.GetSoldiers();
            //     }, (response) =>
            //     {
            //         print("Buying Item Failed: " + response);
            //     });
            // }
            // else
            // {
            //     PopUpManager.Instance.NeedMoreMoney();
            // }
        }
        
        public void SetInfo(CardsInfo soldierInfo)
        {
            _soldierInfo = soldierInfo;
            SafeBox.Instance.userInfo.cards
                .Find(x => x.card_id == _soldierInfo.ID);
            
            image.sprite = PrefabHolder.Instance.GetCardImage(_soldierInfo.soldier_type);
            //name.text = _soldierInfo.name;
            price.text = _soldierInfo.coin_price[0].ToString();
        }

        public void InfoPanel()
        {
            SoldierInfoPopUp.Instance.ActivePopUp(_soldierInfo.soldier_type);
        }
        
        
    }
        
}