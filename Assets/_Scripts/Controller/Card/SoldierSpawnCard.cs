﻿using _Scripts.Controller.Abstract;
using _Scripts.DataHolder;
using _Scripts.Manager;
using _Scripts.Manager.Currencies;
using _Scripts.Model;
using UnityEngine;
using UnityEngine.UI;

namespace _Scripts.Controller.Card
{
    public class SoldierSpawnCard : MonoBehaviour
    {
        
        //  [SerializeField] private Text cardNameText;
        // [SerializeField] private Text requiredManaText;
        [SerializeField] private Image coolDownSlider;
        [SerializeField] private Image manaSlider;
        [SerializeField] private Image cardImage;
        public UserCard userCard;
        public CardsInfo _cardInfo;
        private bool _isActive;
        private float _coolDownTimer;
        private bool _spawnCooledDown;
        private int _requiredMana;
        private int _level;
        private void Start()
        {
            _coolDownTimer = _cardInfo.spawn_cool_down;
            _requiredMana = _cardInfo.required_mana;
            // manaSlider.fillAmount.maxValue = _requiredMana;
            // coolDownSlider.maxValue = _cardInfo.spawn_cool_down;
            var image = PrefabHolder.Instance.GetCardImage(_cardInfo.soldier_type);
            cardImage.sprite = image;
            manaSlider.sprite = image;
            _level = SafeBox.Instance.userInfo.cards.Find(x => x.card_id == _cardInfo.ID).level;
            _isActive = true;
            _spawnCooledDown = false;
        }

        private void Update()
        {
            
            manaSlider.fillAmount=  ManaManager.Instance.GetMana() / _requiredMana;
            coolDownSlider.fillAmount = _coolDownTimer / _cardInfo.spawn_cool_down;
            //_mana = ManaManager.Instance.GetMana();
            
        }

        public void OnMouseDown()
        {
            //print("Spawned" + _cardInfo.soldier_type);
            if (_isActive)
            {
                var mana = ManaManager.Instance.GetMana();
                if (mana >= _cardInfo.required_mana && _coolDownTimer >= _cardInfo.spawn_cool_down)
                {
                    var usedMana = mana - _cardInfo.required_mana;
                    var spawnedSoldier = Instantiate(PrefabHolder.Instance.GetPlayerSoldier(_cardInfo.soldier_type),
                        GameManager.Instance.playerSpawnPoint);
                    spawnedSoldier.SetSide(Soldier.Side.Player1);
                    spawnedSoldier.SetSoldierInfo(_cardInfo);
                    GameManager.Instance.FieldSoldiers.Add(spawnedSoldier);
                    //spawnedSoldier.transform.localScale = new Vector3(.5f,.5f,.5f);
                    _coolDownTimer = 0f;
                    ManaManager.Instance.SetMana(usedMana);
                    GameManager.Instance.SoldierSpawned(spawnedSoldier,false);
                }
            }
                
            /*}
            else
            {
                var card = this.GetSoldierType();
                DeckManager.Instance.GetEquipedSoldiers().Add(card);
                DeckManager.Instance.GetOwnedSoldiers().Remove(card);
            }*/
        }

        private void SetCardInfo()
        {
            _cardInfo =
                SafeBox.Instance.cardsInfo.Find(soldier => soldier.ID == userCard.card_id);
        }

        public void SetIsActive(bool boolean)
        {
            _isActive = boolean;
        }

        public void SetUserCard(UserCard UserCard)
        {
            this.userCard = UserCard;
            SetCardInfo();
        }

        public int GetRequiredMana()
        {
            return _cardInfo.required_mana;
        }

        public CardsInfo GetSoldierInfo()
        {
            return _cardInfo;
        }

        public UserCard UserCard()
        {
            return userCard;
        }

        public bool GetSpawnCoolDown()
        {
            return _spawnCooledDown;
        }

        /*public void CheckSpecialCard()
        {
            if (_cardInfo.isSpecial)
            {
                if (_cardInfo.usage >= 1)
                {
                    _cardInfo.usage--;
                }
            }
        }*/

        public float CoolDown()
        {
            _coolDownTimer += 1;
            _coolDownTimer = Mathf.Clamp(_coolDownTimer, 0, _cardInfo.spawn_cool_down);
            if (_coolDownTimer >= _cardInfo.spawn_cool_down)
            {
                _coolDownTimer = _cardInfo.spawn_cool_down;
                _spawnCooledDown = true;
            }
            else
            {
                _spawnCooledDown = false;
            }
            return _coolDownTimer;
        }
    }
}