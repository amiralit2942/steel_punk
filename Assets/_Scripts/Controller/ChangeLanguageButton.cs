﻿using System;
using _Scripts.UI;
using _Scripts.Util;
using RTLTMPro;
using UnityEngine;
using UPersian.Components;

namespace _Scripts.Controller
{
    public class ChangeLanguageButton: MonoBehaviour
    {
        [SerializeField] private RTLTextMeshPro text;
        private string _currentLanguage;
        
        private void Start()
        {
            if (Localization.Language == LocalizationLanguage.Persian)
            {
                text.text = "فا";
            }
            else
            {
                text.text = "EN";
            }
        }

        public void OnClick()
        {
            if (Localization.Language == LocalizationLanguage.Persian)
            {
                Localization.ChangeLanguage(LocalizationLanguage.English);
                text.text = "EN";
            }
            else
            {
                Localization.ChangeLanguage(LocalizationLanguage.Persian);
                text.text = "فا";
            }
            //LoadingPanel.Instance.FakeLoading(0);
        }
    }
}