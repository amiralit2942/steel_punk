﻿using System.Threading.Tasks;
using _Scripts.Controller.Abstract;
using _Scripts.DataHolder;
using _Scripts.Manager;
using _Scripts.Model;
using _Scripts.Model.ScriptableObjects;
using _Scripts.Model.ScriptableObjectsScripts;
using _Scripts.Util;
using DG.Tweening;
using Manager;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

namespace _Scripts.Controller.Player
{
    
   // public class PlayerSoldier : Soldier,IHittable
   // {
  //       [SerializeField] private GameObject hitParticleSp;
  //       //[SerializeField] private Text healthText;
  //       //[SerializeField] private GameObject healthTextSp;
  //       private float radius = 5f;
  //       private int _direction;
  //       //private Animator _anim;
  //       private float _timer;
  //       private int _maxHealth;
  //       [SerializeField] private UnityEvent _onFreezeStart;
  //       [SerializeField] private UnityEvent _onFreezeEnd;
  //       
  //       protected virtual void Awake()
  //       {
  //           Anim = GetComponent<Animator>();
  //           Rigidbody2D = GetComponent<Rigidbody2D>();
  //           
  //           print("Moving Side" + MovingSide);
  //           
  //       }
  //
  //       protected virtual void Start()
  //       {
  //           //SetSoldierInfo();
  //           if (SoldierSide == Side.Player1)
  //           {
  //               _direction = 1;
  //               //Test
  //               MovingSide = 1;
  //           }
  //           else if (SoldierSide == Side.Player2)
  //           {
  //               _direction = -1;
  //               //Test
  //               MovingSide = -1;
  //           }
  //           CanMove = true;
  //           Anim.SetBool("IsMoving",CanMove);
  //           
  //       }
  //
  //       protected virtual void Update()
  //       {
  //           See();
  //       }
  //
  //       
  //
  //       protected virtual void See()
  //       {
  //           // CanMove = true;
  //           // _anim.SetBool("IsMoving",CanMove);
  //           //var boxCastHits = Physics2D.RaycastAll(transform.position, Vector2.right, ViewRange);
  //           var boxCastHits
  //               = Physics2D.OverlapBoxAll(transform.position, new Vector2(SoldierInfo.view_range, 10), 0f);
  //           /*if (boxCastHits.Length >= 2)
  //           {
  //               if (boxCastHits[1].collider.CompareTag("Enemy"))
  //               {
  //                   Rigidbody2D.velocity = Vector2.zero;
  //                   var enemyType = 's';
  //                   Attack(boxCastHits[1].collider.gameObject, enemyType);
  //                   CanMove = false;
  //               } else if (boxCastHits[1].collider.CompareTag("EnemyBase"))
  //               {
  //                   Rigidbody2D.velocity = Vector2.zero;
  //                   var enemyType = 'b';
  //                   Attack(boxCastHits[1].collider.gameObject, enemyType);
  //                   CanMove = false;
  //               }
  //               else
  //               {
  //                   Move();
  //               }
  //           }
  //           else
  //           {
  //               Move();
  //           }*/
  //           var attacking = false;
  //           foreach (var obstacle in boxCastHits)
  //           {
  //               if (obstacle.TryGetComponent(out IHittable hit)&&hit.side!=SoldierSide)
  //               {
  //                   AttackCoolDownTimer();
  //                   CanMove = false;
  //                   attacking = true;
  //                   Rigidbody2D.velocity = Vector2.zero;
  //                   Anim.SetBool("IsMoving",CanMove);
  //                   continue;
  //               }
  //               
  //               /*Side obsBaseSide;
  //               Side obsSoldierSide;
  //               var playerS = obstacle.GetComponent<PlayerSoldier>();
  //               if (playerS != null)
  //               { 
  //                   obsSoldierSide = playerS.SoldierSide;
  //                   if (obsSoldierSide != SoldierSide)//obstacle.gameObject.CompareTag("Enemy"))
  //                   {
  //                       
  //                       Rigidbody2D.velocity = Vector2.zero;
  //                       var enemyType = 's';
  //                       Attack(obstacle.gameObject, enemyType);
  //                       attacking = true;
  //                       CanMove = false;
  //                       _anim.SetBool("IsMoving",CanMove);
  //                       continue;
  //                   }
  //               }
  //               var baseC = obstacle.GetComponent<Base>();
  //               if (baseC != null)
  //               {
  //                  obsBaseSide  = baseC.BaseSide;
  //                  if (obsBaseSide != SoldierSide)//obstacle.gameObject.CompareTag("EnemyBase"))
  //                  {
  //                     
  //                      Rigidbody2D.velocity = Vector2.zero;
  //                      var enemyType = 'b';
  //                      Attack(obstacle.gameObject, enemyType);
  //                      attacking = true;
  //                      CanMove = false;
  //                      _anim.SetBool("IsMoving",CanMove);
  //                  }
  //               }*/
  //           }
  //
  //           if (!attacking)
  //           {
  //               CanMove = true;
  //               Move();
  //           }
  //       }
  //
  //       protected virtual void Move()
  //       {
  //           if (CanMove)
  //           {
  //               // _anim.Play("Move");
  //               if (SoldierSide == Side.Player1)
  //               {
  //                   Rigidbody2D.velocity = Vector2.right * Speed;    
  //               }
  //               else if (SoldierSide == Side.Player2)
  //               {
  //                   Rigidbody2D.velocity = Vector2.left * Speed;
  //               }
  //           }
  //           else
  //           {
  //               // _anim.Play("Idle");
  //               Rigidbody2D.velocity = Vector2.zero;    
  //           }
  //           Anim.SetBool("IsMoving",CanMove);
  //       }
  //
  //       protected virtual void Attack()
  //       {
  //           if (SoldierType == Soldiers.Bomber)
  //           {
  //               print("Bomber Attacking");
  //               var circleCastHit = Physics2D.OverlapCircleAll(transform.position, radius);
  //               foreach (var obstacle in circleCastHit)
  //               {
  //                   if (obstacle.TryGetComponent<IHittable>(out var hit) && hit.side != SoldierSide)
  //                   {
  //                       hit.OnHit(SoldierInfo.damage[SoldierLevel],SoldierInfo.soldier_type);
  //                   }
  //                   else
  //                   {
  //                       continue;
  //                   }
  //               }
  //               Death();
  //           }
  //           else
  //           {
  //               var boxCastHits = Physics2D.OverlapBoxAll(transform.position, new Vector2(SoldierInfo.view_range, 10), 0f);
  //               foreach (var obstacle in boxCastHits)
  //               {
  //                   if (obstacle.TryGetComponent(out IHittable hit) && hit.side != SoldierSide)
  //                   {
  //                       hit.OnHit(SoldierInfo.damage[SoldierLevel],SoldierInfo.soldier_type);
  //                       //Rigidbody2D.velocity = Vector2.zero;
  //                       // attacking = true;
  //                       _timer = SoldierInfo.attack_cool_down;
  //                       CanAttack = false;
  //                       Anim.SetBool("IsMoving",CanMove);
  //                       
  //                       break;
  //                   }    
  //               }
  //           }
  //
  //           
  //           // _anim.Play("Attack");
  //           /* _anim.SetTrigger("Attack");
  //            if (CanAttack)
  //            {
  //                if (enemyType == 's')
  //                {
  //                    var enemySoldier = attacked.GetComponent<Soldier>();
  //
  //
  //                    if (enemySoldier != null)
  //                    {
  //                        var enemySoldierHealth = enemySoldier.GetHealth();
  //                        enemySoldierHealth -= Damage;
  //                        if (enemySoldierHealth <= 0)
  //                        {
  //                            enemySoldier.Death();
  //                        }
  //
  //                        enemySoldier.SetHealth(enemySoldierHealth);
  //                        CanAttack = false;
  //                    }
  //                    // AttackCoolDownTimer();
  //
  //                }
  //                else if (enemyType == 'b')
  //                {
  //                    var enemyBase = attacked.GetComponent<Base>();
  //                    if (enemyBase != null)
  //                    {
  //                        var enemyBaseHealth = enemyBase.GetHealth();
  //                        enemyBaseHealth -= Damage;
  //                        if (enemyBaseHealth <= 0)
  //                        {
  //                            enemyBase.Death();
  //                        }
  //
  //                        enemyBase.SetHealth(enemyBaseHealth);
  //
  //                        AttackCoolDownTimer();
  //                    }
  //                }
  //            }*/
  //       }
  //
  //      
  //
  //       protected virtual void AttackEnded()
  //       {
  //           Anim.SetBool("Attacking",false);
  //       }
  //
  //       protected virtual void AttackCoolDownTimer()
  //       {
  //           if (_timer >= 0)
  //           {
  //               _timer -= Time.deltaTime;
  //               if (_timer <= 0)
  //               {
  //                   // CanAttack = true;
  //                   // Double Check
  //                   Anim.SetBool("Attacking",true);
  //                   // print("Animation Played");    
  //               }
  //           }
  //       }
  //
  //       public virtual void Death()
  //       {
  //           //Destroy(gameObject);                                                                                                                                                                                                                                                            
  //           Anim.SetBool("Died",true);
  //           Anim.SetBool("Moving",false);
  //           Anim.SetBool("Attacking",false);
  //           Dead = true;
  //           // if (SoldierType == Soldiers.Bomber)
  //           // {
  //           //     Instantiate(PrefabHolder.Instance.GetParticle(SoldierType,ParticleType.Death), transform.position , transform.rotation);    
  //           // }
  //           // Instantiate(PrefabHolder.Instance.GetParticle(Soldiers.Base,ParticleType.Death), transform.position , transform.rotation);
  //       }
  //
  //       protected virtual void CollectBody()
  //       {
  //           print("Void");
  //           //Anim.SetBool("Died",false);
  //           var t = 0f;
  //           DOTween.To(() => t, (v) => t = v, 0, 5).OnComplete(() =>
  //           {
  //               print("DoTween");
  //               var image = gameObject.GetComponent<CanvasGroup>();
  //               image.DOFade(0, 2).OnComplete(() =>
  //               {
  //                   print("Done");
  //                   Destroy(gameObject);
  //               });
  //           });
  //       }
  //
  //       public virtual int GetHealth()
  //       {
  //           return Health;
  //       }
  //
  //       
  //       public virtual void SetSoldierInfo(CardsInfo cardsInfo)
  //       {
  //           SoldierInfo = cardsInfo;
  //           //GameManager.Instance.soldierInfos.Find(soldier => soldier.soldier_type == SoldierType);
  //           Name = SoldierInfo.name;
  //           SoldierLevel = SafeBox.Instance.userInfo.cards.Find(x => x.card_id == SoldierInfo.ID).level;
  //           // Health = SoldierInfo.health[SoldierLevel - 1];
  //           // _maxHealth = SoldierInfo.health[SoldierLevel - 1];
  //           // Damage = SoldierInfo.damage[SoldierLevel - 1];
  //           // SpawnCoolDown = SoldierInfo.spawn_cool_down;
  //           // AttackCoolDown = SoldierInfo.attack_cool_down;
  //           // RequiredMana = SoldierInfo.required_mana;
  //           // ViewRange = SoldierInfo.view_range;
  //           // Speed = SoldierInfo.speed;
  //           _timer = SoldierInfo.attack_cool_down;
  //           Dead = false;
  //       }
  //
  //       public virtual void SetHealth(int newHealth)
  //       {
  //           // _anim.Play("Damaged");
  //           Health = newHealth;
  //           //healthText.text = Health.ToString();
  //       }
  //
  //       public virtual void SetSide(Side soldierSide)
  //       {
  //           SoldierSide = soldierSide;
  //       }
  //       
  //
  //       private void OnDrawGizmos()
  //       {
  //           Gizmos.DrawWireCube(transform.position,new Vector2(SoldierInfo.view_range, 10));
  //       }
  //
  //       public Side side => SoldierSide;
  //
  //       public bool Dead
  //       {
  //           get;
  //           set;
  //       }
  //
  //       public void OnHit(int damage,Soldiers attackingSoldier)
  //       {
  //           if (Health > damage)
  //           {
  //               SetHealth(Health - damage);
  //               GetHitParticle(attackingSoldier);
  //               //HealthTextAnim(damage);
  //           }
  //           else
  //           {
  //               Death();
  //              // GameManager.Instance.SoldierDied(this,false);
  //           }
  //           
  //       }
  //
  //       protected void GetHitParticle(Soldiers attackingSoldier)
  //       {
  //           Instantiate(PrefabHolder.Instance.GetParticle(attackingSoldier, ParticleType.Hit), hitParticleSp.transform);
  //       }
  //
  //       
  //
  //       public void Heal(int hp)
  //       {
  //           
  //           Health += hp;
  //       }
  //
  //       public void SpeedUp(float boostSpeed,float duration)
  //       {
  //           Speed += boostSpeed;
  //           Anim.speed += boostSpeed / Speed;
  //           var t = 0f;
  //           DOTween.To(() => t, (v) => t = v, 10, duration).OnComplete(() =>
  //           {
  //               Speed -= boostSpeed;
  //               Anim.speed = 1;
  //           });
  //           
  //       }
  //
  //       public void Freeze(float duration)
  //       {
  //           _onFreezeStart?.Invoke();
  //           var oldSpeed = Speed;
  //           //
  //           var animSpeed = 1f;
  //           //
  //           
  //           DOTween.To(() => animSpeed, v => animSpeed = v, 0, 1.5f).OnUpdate(() =>
  //           {
  //               Anim.speed = animSpeed;
  //               Speed = oldSpeed * animSpeed;
  //
  //           }).OnComplete(() =>
  //           {
  //               var t = 0f;
  //               DOTween.To(() => t, (v) => t = v, 1, duration).OnComplete(() =>
  //               {
  //                   _onFreezeEnd?.Invoke();
  //                   DOTween.To(() => animSpeed, v => animSpeed = v, 1, 1.5f).OnUpdate(() =>
  //                   {
  //                       Anim.speed = animSpeed;
  //                       Speed = oldSpeed * animSpeed;
  //                   }).OnComplete(() =>
  //                   {
  //                       Speed = oldSpeed;
  //                       Anim.speed = 1; 
  //                   });
  //               });
  //           });
  //
  //       }
  //
  //       public void ShieldSpell(float duration,int shieldAmount)
  //       {
  //           Health += shieldAmount;
  //           var t = 0;
  //           DOTween.To(() => t, (v) => t = v, 1, duration).OnComplete(() =>
  //           {
  //               Health -= shieldAmount;
  //           });
  //       }
  //       
  //
  //       /*private void HealthTextAnim(int damage)
  //       {
  //           healthText.gameObject.SetActive(true);
  //           AnimatedText.Instance.TextColorChanger(_maxHealth,damage,healthText,0,1,0,1);
  //           healthText.transform.DOLocalMoveY( 5, 2, true);
  //           healthText.DOFade(0, 2).OnComplete(() =>
  //           {
  //               healthText.gameObject.SetActive(false);
  //               healthText.transform.localPosition = new Vector3(0,0,0);
  //           });
  //       }*/
  //   }
}