using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace _Scripts.Controller
{
    public class ScrollViewWithBtn : MonoBehaviour
    {
        [SerializeField] private Transform contentMid;
        [SerializeField] private Transform content;
        [SerializeField] private Transform soldiersSection;
        [SerializeField] private Transform spellsSection;
        [SerializeField] private Button nextButton;
        [SerializeField] private Button previousButton;
        [SerializeField] private float movingDuration;

        private void Awake()
        {
            _isOnSoldierSection = true;
            previousButton.interactable = false;
            StartCoroutine(SnapRoutine(soldiersSection));
        }

        private IEnumerator SnapRoutine(Transform target)
        {
            var vDiff = new Vector3(contentMid.position.x - target.position.x, 0);
            var t = 0f;
            var initialPos = content.transform.position;
            var targetPos = initialPos + vDiff;
            while (t < movingDuration)
            {
                t += Time.deltaTime;
                t = Mathf.Clamp(t, 0, movingDuration);
                content.transform.position = Vector3.Lerp(initialPos, targetPos, (movingDuration / t));
                yield return null;
            }
        }


        private bool _isOnSoldierSection ;
        public void ScrollButton()
        {
            if (!_isOnSoldierSection)
            {
                // Go to soldier section
                StartCoroutine(SnapRoutine(soldiersSection));
                nextButton.interactable = true;
                previousButton.interactable = false;
                _isOnSoldierSection = true;
            }
            else
            {
                //Go to spell section
                StartCoroutine(SnapRoutine(spellsSection));
                nextButton.interactable = false;
                previousButton.interactable = true;
                _isOnSoldierSection = false;
            }
        }
    }
}