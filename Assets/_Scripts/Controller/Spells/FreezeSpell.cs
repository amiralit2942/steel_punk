﻿using _Scripts.Manager;
using UnityEngine;

namespace _Scripts.Controller.Spells
{
    public class FreezeSpell : Spell
    {
        public FreezeSpell(float duration, int level) : base(duration, level)
        {
            spellType = SpellType.FreezeSpell;
        }

        public override void ActiveSpell()
        {
            GameManager.Instance.FreezeSpell(_duration);
        }
    }
}
