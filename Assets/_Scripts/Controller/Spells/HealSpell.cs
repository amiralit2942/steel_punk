﻿using _Scripts.Manager;
using UnityEngine;

namespace _Scripts.Controller.Spells
{
    public class HealSpell : Spell
    {
        public int Hp { get; }
        public HealSpell(float duration, int level, int hp) : base(duration, level)
        {
            Hp = hp;
            base.spellType = SpellType.HealSpell;
        }

        public override void ActiveSpell()
        {
            GameManager.Instance.HealSpell(Hp);
        }
    }
}