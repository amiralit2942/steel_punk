﻿using _Scripts.Manager;
using UnityEngine;

namespace _Scripts.Controller.Spells
{
    public class SpeedSpell : Spell
    {
        public int BoostAmount { get; }
        
        public SpeedSpell(float duration, int level, int boostAmount) : base( duration, level)
        {
            base.spellType = SpellType.SpeedSpell;   
            BoostAmount = boostAmount;
        }

        public override void ActiveSpell()
        {
            GameManager.Instance.SpeedSpell(BoostAmount,_duration);
        }
    }
}