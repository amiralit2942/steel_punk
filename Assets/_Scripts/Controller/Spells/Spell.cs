﻿using UnityEngine;

namespace _Scripts.Controller.Spells
{
    public class Spell : MonoBehaviour
    {
        public float _duration { get; }
        private int _level;
        public int Level => _level;

        public SpellType spellType
        {
            get;
            protected set;
        }
        public enum SpellType
        {
            HealSpell = 0,
            SpeedSpell = 1,
            FreezeSpell = 2
        }
        public Spell(float duration, int level)
        {
            //this.spellType = spellType;
            _duration = duration;
            _level = level;
        }

        public virtual void ActiveSpell()
        {
            
        }
        





    }
}