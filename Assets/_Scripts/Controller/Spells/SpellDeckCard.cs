﻿using System;
using _Scripts.Controller.Card.Deck;
using _Scripts.DataHolder;
using _Scripts.Manager;
using _Scripts.Model;
using _Scripts.UI;
using _Scripts.UI.Equips;
using TMPro;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

namespace _Scripts.Controller.Spells
{
    public class SpellDeckCard : MonoBehaviour 
    {
        [SerializeField] private Image spellImage;
        [SerializeField] private TMP_Text spellAmountText;
        private SpellModel _spellInfo;
        public SpellModel SpellInfo => _spellInfo;
        [SerializeField] private UserSpell _userSpell;
        public UserSpell UserSpell => _userSpell;
        private int _spellAmount;


        public void OnClick()
        {
            DeckPanel.Instance.ActiveInfoPanel(_userSpell,false);
        }
        

        public void SetInfo(UserSpell userSpell)
        {
            _userSpell = userSpell;
            print("Given Spell: " + userSpell);
            _spellInfo = CardsManager.Instance.GETSpellInfo(userSpell);
            spellImage.sprite = PrefabHolder.Instance.
                GetSpellImageHolderAsset(_spellInfo.spell_type);
            spellImage.gameObject.SetActive(true);
            spellAmountText.text = userSpell.amount.ToString();
        }

        public void AddSpell()
        {
            spellAmountText.text = _userSpell.amount.ToString();
        }

        public void DecreaseSpell()
        {
            var amount = _userSpell.amount;
            spellAmountText.text = (amount - 1).ToString();
        }

        public bool HasMoreThanOne()
        {
            if (_userSpell.amount <= 1)
            {
                return false;
            }
            else
            {
                return true;
            }
        }
        
        
    }
}