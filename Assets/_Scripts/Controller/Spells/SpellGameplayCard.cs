using _Scripts.DataHolder;
using _Scripts.Manager;
using _Scripts.Model;
using UnityEngine;
using UnityEngine.UI;

namespace _Scripts.Controller.Spells
{
    public class SpellGameplayCard : MonoBehaviour
    {
        [SerializeField] private UserSpell _userSpell;
        [SerializeField] private SpellModel _spellInfo;
        [SerializeField] private Image _spellImage;

        public void SetInfo(UserSpell userSpell)
        {
            print("2 Spell ID: " + userSpell.spellId);
            _userSpell = userSpell;
            _spellInfo = SafeBox.Instance.spellsInfo?.Find(x => x.id == userSpell.spellId);
            print("Spell Info: " + _spellInfo.id);
            //_spellInfo = SpellsManager.Instance.GetSpellInfo(_userSpell.spellId);
            _spellImage.sprite = PrefabHolder.Instance.GetSpellImageHolderAsset(_spellInfo.spell_type);
        }

        public void OnClick()
        {
            switch (_spellInfo.spell_type)
            {
                case Spell.SpellType.FreezeSpell:
                    GameManager.Instance.FreezeSpell(_spellInfo.duration);
                    break;
                case Spell.SpellType.HealSpell:
                    GameManager.Instance.HealSpell(_spellInfo.power);
                    break;
                case Spell.SpellType.SpeedSpell:
                    GameManager.Instance.SpeedSpell(_spellInfo.power,_spellInfo.duration);
                    break;
            }
            GameManager.Instance.SpellUsed(_userSpell);
            Destroy(gameObject);
        }
    }
}
