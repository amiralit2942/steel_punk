﻿using System;
using _Scripts.DataHolder;
using _Scripts.Manager;
using _Scripts.Manager.Currencies;
using _Scripts.Model;
using _Scripts.UI.NoticePopUp;
using _Scripts.Util;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace _Scripts.Controller.Spells
{
    public class SpellShopCard : MonoBehaviour
    {
        [SerializeField] private Image image;
        [SerializeField] private TMP_Text price;
        [SerializeField] private TMP_Text name;
        [SerializeField] private SpellModel _spellModel;
        [SerializeField] private UserSpell _userSpell;

        public void SetInfo(SpellModel spellModel)
        {
            _spellModel = spellModel;
            image.sprite = PrefabHolder.Instance.
                GetSpellImageHolderAsset(_spellModel.spell_type);
            price.text = spellModel.price.ToString();
            ChangeName();
            Localization.OnChangeLanguage += ChangeName;
        }

        private void OnDisable()
        {
            Localization.OnChangeLanguage -= ChangeName;
        }

        public void OnClick()
        {
            //PopUpManager.Instance.BuySpell(this);
            NoticePopUpPanel.Instance.NewNotice().WithContent(Strings.BuySpellNotice)
                .WithAcceptButton(Buy).WithRejectButton();
        }

        public void Buy()
        {
            if ( CoinManager.Instance.GetCoin() >= _spellModel.price)
            {
                ServerManager.Instance.BuySpell(_spellModel.id, (userSpell) =>
                {
                    CoinManager.Instance.BuyItem(_spellModel.price,"Spell",
                        _spellModel.spell_type.ToString(),"Spell Shop");
                    SpellsManager.Instance.EarnSpell(userSpell);    
                }, (response) =>
                {
                    print(response);
                });
            }
            else
            {
                //PopUpManager.Instance.NeedMoreMoney();
                NoticePopUpPanel.Instance.NewNotice().WithContent(Strings.NotEnoughCoin).WithRejectButton();
            }
        }

        public void ChangeName()
        {
            name.text = Localization.Get(_spellModel.spell_type.ToString());
        }
    }
}