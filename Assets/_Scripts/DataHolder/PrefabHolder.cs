﻿using _Scripts.Controller.Card;
using _Scripts.Controller.Player;
using _Scripts.Model.ScriptableObjects;
using _Scripts.Controller.Abstract;
using _Scripts.Controller.Card.Chest;
using _Scripts.Controller.Card.Deck;
using _Scripts.Controller.Spells;
using _Scripts.Manager;
using _Scripts.Model;
using _Scripts.Model.ScriptableObjectsScripts;
using _Scripts.Soldiers;
using TMPro;
using UnityEngine;
using Util;

namespace _Scripts.DataHolder
{
    public class PrefabHolder : MonoBehaviourSingleton<PrefabHolder>
    {
        [SerializeField] private TMP_FontAsset persianFont;
        [SerializeField] private TMP_FontAsset englishFont;
        [SerializeField] private SoldierSpawnCard soldierSpawnCard;
        [SerializeField] private SoldierDeckCard soldierDeckCard;
        [SerializeField] private CardHolderAsset cardHolderAsset;
        [SerializeField] private ImageHolderAsset imageHolderAsset;
        [SerializeField] private ParticleHolderAsset particleHolderAsset;
        [SerializeField] private ChestContentCardAsset chestContentCardAsset;
        [SerializeField] private SoldierUpgradeCard itemShopCard;
        [SerializeField] private ProductCard gemStoreCard;
        [SerializeField] private ProductCard coinStoreCard;
        [SerializeField] private GameObject deathParticle;
        [SerializeField] private SoldierUpgradeCard soldierUpgradeCard;
        [SerializeField] private AudioSource menuMusic;
        [SerializeField] private ChestContentCard chestContentCard;
        //[SerializeField] private ChestCard chestCard;
        [SerializeField] private ChestsPrefabHolder chestsPrefabHolder;
        [SerializeField] private SpellDeckCard spellDeckCard;
        [SerializeField] private SpellImageHolderAsset spellImageHolderAsset;
        [SerializeField] private SpellShopCard spellShopCard;
        [SerializeField] private BaseGunSpriteHolder baseGunSpriteHolder;
        [SerializeField] private SoldierShopCard soldierShopCard;
        [SerializeField] private GameObject shopRow;
        [SerializeField] private SpellGameplayCard spellGameplayCard;

        public TMP_FontAsset PersianFont => persianFont;
        public TMP_FontAsset EnglishFont => englishFont;
        public NormalSoldier GetPlayerSoldier(Soldier.Soldiers type)
        {
            var selectedSoldier = 
                cardHolderAsset.cards.Find(soldier => soldier.soldierType == type);
            return selectedSoldier.prefab[0];
        }
        
        public SoldierSpawnCard GetCard()
        {
            return soldierSpawnCard;
        }

        public SoldierDeckCard GetSoldierDeckCard()
        {
            return soldierDeckCard;
        }

        public SoldierUpgradeCard GetItemShopCard()
        {
            return itemShopCard;
        }

        public ProductCard GetGemStoreCard()
        {
            return gemStoreCard;
        }

        public ProductCard GetCoinStoreCard()
        {
            return coinStoreCard;
        }
        
        public Sprite GetCardImage(Soldier.Soldiers soldierType)
        {
            var sprite = imageHolderAsset.images.Find(image => image.soldierType == soldierType).soldierImage;
            if (sprite != null)
            {
                return sprite;    
            }

            return null;
        }

        public GameObject GetDeathParticle()
        {
            return deathParticle;
        }

        public SoldierUpgradeCard GetUpgradeCart()
        {
            return soldierUpgradeCard;
        }

        public GameObject GetParticle(Soldier.Soldiers soldierType,ParticleType particleType)
        {
            var particle = particleHolderAsset.particleHolders.Find(
                x => x.soldierType == soldierType && x.particleType == particleType);
            
            return particle.particle;
        }

        public GameObject GetParticle(Soldier.Soldiers soldierType)
        {
            var particle = particleHolderAsset.particleHolders.Find(
                x => x.soldierType == soldierType);

            return particle.particle;
        }

        public ChestContentCard GetChestContentCard()
        {
            return chestContentCard;
        }

        public Sprite GetChestContentCardSprite(PrizeType prizeType, uint cardID = 0)
        {
            switch (prizeType)
            {
                case PrizeType.CoinPrizeType:
                    return chestContentCardAsset.CoinSprite;
                case PrizeType.GemPrizeType:
                    return chestContentCardAsset.GemSprite;
                case PrizeType.CardPrizeType:
                    var soldierType = CardsManager.Instance.GETSoldierInfo(cardID).soldier_type;
                    return chestContentCardAsset.UpgradeCardsAsset
                        .Find(x => x.SoldierType == soldierType).Sprite;
            }
            return null;
        }

        public ChestCard GetChestCard(ChestType chestType)
        {
            return chestsPrefabHolder.ChestPrefabs.Find(x => x.ChestType == chestType).ChestCard;
        }

        public SpellDeckCard GetSpellDeckCard()
        {
            return spellDeckCard;
        }

        public SpellGameplayCard GetSpellGamePlayCard()
        {
            return spellGameplayCard;
        }

        public Sprite GetSpellImageHolderAsset(Spell.SpellType spellType)
        {
            var spell = spellImageHolderAsset.spellImageHolders
                .Find(x => x.spellType == spellType);

            return spell.spellImage;
        }

        public SpellShopCard GetSpellShopCard()
        {
            return spellShopCard;
        }

        public Sprite GetBaseGunSprite(BaseGunType baseGunType)
        {
            return baseGunSpriteHolder.BaseGunSprites
                .Find(x => x.BaseGunType1 == baseGunType).GunSprite;
        }

        public SoldierShopCard GETSoldierShopCard()
        {
            return soldierShopCard;
        }

        public GameObject GetShopRow()
        {
            return shopRow;
        }
        
    }
}