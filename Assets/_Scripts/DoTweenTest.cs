﻿using System.Collections.Generic;
using DG.Tweening;
using Newtonsoft.Json;
using UnityEngine;

namespace _Scripts
{
    public class DoTweenTest : MonoBehaviour
    {
        public List<float> maxMana;
        // Start is called before the first frame update
        void Start()
        {
            maxMana = new List<float>()
            {
                1, 2, 3
            };

            print(JsonConvert.SerializeObject(maxMana));
        }

    }
}
