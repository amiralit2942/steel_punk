﻿using System;
using UnityEngine;
using UnityEngine.EventSystems;

namespace _Scripts.DragAndDrop
{
    public class ItemDragHandler : MonoBehaviour,IBeginDragHandler , IDragHandler 
    {
        [SerializeField] private CanvasGroup canvasGroup;
        [SerializeField] private RectTransform rectTransform;
        
        

        private float _diffX;
        private float _diffY;
        public void OnBeginDrag(PointerEventData eventData)
        {
            //canvasGroup.blocksRaycasts = false;
            rectTransform.position = eventData.pointerCurrentRaycast.worldPosition;
            // _diffX = eventData.pointerCurrentRaycast.worldPosition.x - transform.position.x;
            // _diffY = eventData.pointerCurrentRaycast.worldPosition.y - transform.position.y;
        }
        public void OnDrag(PointerEventData eventData)
        {
            rectTransform.anchoredPosition = eventData.pointerCurrentRaycast.worldPosition;
            
            // rectTransform.localPosition
            //     = eventData.pressPosition; //- new Vector3(_diffX,_diffY,0);
        }
    }
}
