﻿using UnityEngine;
using UnityEngine.EventSystems;

namespace _Scripts.DragAndDrop
{
    public class Slot : MonoBehaviour,IDropHandler 
    {
        public void OnDrop(PointerEventData eventData)
        {
            if (eventData.pointerDrag != null)
            {
                eventData.pointerDrag.GetComponent<RectTransform>().anchoredPosition
                    = GetComponent<RectTransform>().anchoredPosition;
            }
        }
    }
}
