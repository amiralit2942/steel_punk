﻿using System.Collections.Generic;
using _Scripts.DataHolder;
using _Scripts.Manager.Stores;
using _Scripts.Model;
using CafeBazaar.Billing;
using UnityEngine;
using UnityEngine.UI;
using Util;

namespace _Scripts.IAB
{
    public class Iab : MonoBehaviourSingleton<Iab>
    {
        private int _boughtGem;
        private string _id;
        private List<ShopItems> _gemStoreItems;
        private List<string> _skuList;
        [SerializeField] private Transform productPanel;
        [SerializeField] private Text priceText;
        [SerializeField] private Text amountText;
        // Start is called before the first frame update
        void Start()
        {
            _gemStoreItems = new List<ShopItems>();
            _gemStoreItems.AddRange(ShopsProductManager.Instance.GetGemStoreProducts());
            BazaarBilling.Init(_id, (result) =>
            {
                
            });

            foreach (var item in _gemStoreItems)
            {
                Instantiate(PrefabHolder.Instance.GetGemStoreCard(), productPanel);
            }

            foreach (var item in _gemStoreItems)
            {
                _skuList.Add(item.sku);
            }

            //BazaarBridge.Instance.SetBazaarSKUList(_skuList);
            /*{
                "pack_1",
                "Pack_2",
                "Pack_3",
                "Pack_4",
                "Pack_5",
                "Pack_6",
                "Pack_7",
                "Pack_8",
            });*/
           /* BazaarBridge.Instance.OnPurchseSucceed = (sku, token) =>
            {
                ServerManager.Instance.ShopPurchase(sku,token,"", () =>
                {
                    BazaarBridge.Instance.Consume();
                    
                },(statusCode)=>
                {
                    
                });
                
                switch (_id)
                {
                    case "Pack_1" :  _boughtGem = 30;
                        break;
                    case "Pack_2" : _boughtGem = 90;
                        break;
                    case "Pack_3" : _boughtGem = 180;
                        break;
                    case "Pack_4" : _boughtGem = 330;
                        break;    
                    case "Pack_5" : _boughtGem = 690;
                        break;
                    case "Pack_6" : _boughtGem = 1080;
                        break;
                    case "Pack_7" : _boughtGem = 1860;
                        break;
                    case "Pack_8" : _boughtGem = 3900;
                        break;
                }
                
                print("switch check");
                GemManager.Instance.EarnGem(_boughtGem);
                _boughtGem = 0;
            };

            BazaarBridge.Instance.OnPurchaseFailed = (result) =>
            {
                
            };*/
        }
        
        
        
        public void Pardakht(string id)
        {
            //Purchase(id);
            _id = id;

        }

        /*public void Purchase(string sku)
        {
            BazaarBridge.Instance.Purchase(sku);
        }*/

        
    }
}
