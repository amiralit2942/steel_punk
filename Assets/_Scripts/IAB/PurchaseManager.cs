﻿using _Scripts.Manager;
using _Scripts.Manager.Currencies;
using CafeBazaar.Billing;
using Manager;
using Util;

namespace IAB
{
    public class PurchaseManager : MonoBehaviourSingleton<PurchaseManager>
    {
        // Start is called before the first frame update
        void Start()
        {
            BazaarBilling.Init((response) =>
            {
            
            });
        }

        public void Purchase(string sku, int amount)
        {
            BazaarBilling.Purchase(sku, (response) =>
            {
                if (response.Successful)
                {
                    ServerManager.Instance.ShopPurchase(sku, response.Body.PurchaseToken, "", () =>
                    {
                        GemManager.Instance.EarnGem(amount,"Store",sku);
                        BazaarBilling.Consume(sku, (result) =>
                        {
                        
                        });
                    }, (r) =>
                    {
                        print("purchase failed");
                    });
                }
                else
                {
                    print("purchase failed");
                }
            });
        }
        // Update is called once per frame
        void Update()
        {
        
        }
    }
}
