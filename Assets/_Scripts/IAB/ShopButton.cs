﻿using UnityEngine;

namespace IAB
{
   public class ShopButton : MonoBehaviour
   {
      [SerializeField] private string sku;
      [SerializeField] private int amount;
   
   
      public void Purchase()
      {
         PurchaseManager.Instance.Purchase(sku, amount);
      }
   }
}
