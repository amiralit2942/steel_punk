﻿using _Scripts.Controller.Abstract;
using UnityEngine;

namespace _Scripts.Interface
{
    public interface IHittable
    {
        Soldier.Side side { get; }

        

        bool Dead
        {
            get;
            set;
        }
        void OnHit(int damage,Soldier.Soldiers attackingSoldier);
        GameObject GetGameObject();
    }
}
