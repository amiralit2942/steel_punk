﻿using System.Collections.Generic;
using _Scripts.DataHolder;
using _Scripts.Manager.Currencies;
using _Scripts.Model;
using _Scripts.Controller.Abstract;
using Manager;
using UnityEngine;
using Util;

namespace _Scripts.Manager
{
    public class AiManager : MonoBehaviourSingleton<AiManager>
    {
        private float _timer;
        private Transform _enemySpawnPoint;
        public float[] spawnTimer;
        private int _spawnTurn;
        public List<CardsInfo> enemyDeck;
        public List<int> enemySoldiersLevel;
        private int _index;
        private System.Random _rnd;
        public bool _matchEnded;
        private CardsInfo _selectedSoldier;
        
        void Awake()
        {
            _rnd = new System.Random();
            _matchEnded = false;
            _timer = spawnTimer[_rnd.Next(spawnTimer.Length)];
            _enemySpawnPoint = GameManager.Instance.enemySpawnPoint;
            timer = 2;
            //Getting Deck Cards Info
            foreach (var userCard in SafeBox.Instance.userInfo.deck_cards)
            {
                var selectedCardInfo = CardsManager.Instance.GETSoldierInfo(userCard.card_id);
                if (!enemyDeck.Contains(selectedCardInfo) && selectedCardInfo.soldier_type != Soldier.Soldiers.Base)
                {
                    enemyDeck.Add(selectedCardInfo);    
                }
            }
            
            // var baseSoldier =enemyDeck.Find(x => x.soldier_type == Soldier.Soldiers.Base);
            //
            // if (baseSoldier != null)
            // {
            //     enemyDeck.Remove(baseSoldier);    
            // }
            
            foreach (var soldier in enemyDeck )
            {
                if (soldier.soldier_type != Soldier.Soldiers.Base)
                {
                    foreach (var info in SafeBox.Instance.userInfo.cards)
                    {
                        if (soldier.ID == info.card_id)
                        {
                            enemySoldiersLevel.Add(info.level);
                        }
                    }    
                }
            }
            
            _spawnTurn = _rnd.Next(0, enemyDeck.Count);
            _selectedSoldier = enemyDeck[_spawnTurn];
           /* while (_selectedSoldier.required_mana > ManaManager.Instance.GetMaxMana())
            {
                _spawnTurn = _rnd.Next(0, enemyDeck.Count);
                _selectedSoldier = enemyDeck[_spawnTurn];
            }*/
           foreach (var soldier in enemyDeck)
           {
               
               if (_selectedSoldier.required_mana > ManaManager.Instance.GetMaxMana())//soldier.required_mana)
               {
                   _selectedSoldier = soldier;
               }
           }
            //print("Selected Soldier : " + _selectedSoldier.soldier_type );
            //print("Required Mana : " + _selectedSoldier.required_mana);
            //_matchEnded = true;
        }

        [SerializeField] private float spawnCooldown;
         private float timer ;
        void Update()
        {
            if (!_matchEnded)
            {
                timer -= Time.deltaTime;
                if (ManaManager.Instance.GetAiMana() >= _selectedSoldier.required_mana && timer <= 0)
                {
                    SpawnSoldier();
//                    print("Ame talaa");
                    ManaManager.Instance.UseAiMana( _selectedSoldier.required_mana);
                    _spawnTurn = _rnd.Next(0, enemyDeck.Count);
                    _selectedSoldier = enemyDeck[_spawnTurn];
                    timer = spawnCooldown;
                    while (_selectedSoldier.required_mana > ManaManager.Instance.GetMaxMana())
                    {
                        _spawnTurn = _rnd.Next(0, enemyDeck.Count);
                        _selectedSoldier = enemyDeck[_spawnTurn];
                    }
                    
                    /*print("Selected Soldier : " + _selectedSoldier.soldier_type );
                    print("Required Mana : " + _selectedSoldier.required_mana);*/
                }
                /*_timer -= Time.deltaTime;
            
                if (_timer <= 0)
                {
                    _timer = spawnTimer[_rnd.Next(spawnTimer.Length)];//[Random.Range(0, spawnTimer.Length)];
                    _spawnTurn = _rnd.Next(0,enemyDeck.Count);//_rnd.Next(1,enemyDeck.Count);//Random.Range(0,enemyDeck.Count) ;//
                    SpawnEnemy(_spawnTurn);
                    //_timer = spawnTimer[Random.Range(0, spawnTimer.Length)];
                }*/
            }
        }
        private void SpawnEnemy(int spawnTurn)
        {

            var soldierType = enemyDeck[spawnTurn];
            var soldierInfo = enemySoldiersLevel[spawnTurn]; //UserInfo.Instance.cards.Find(x => x.soldierType == soldierType);
            var newGo = Instantiate(PrefabHolder.Instance.GetPlayerSoldier(enemyDeck[spawnTurn].soldier_type), _enemySpawnPoint );
            //newGo.SetSoldierType(enemyDeck[spawnTurn].soldier_type);
            newGo.transform.rotation = Quaternion.Euler(0,180,0);
            //newGo.transform.localScale = new Vector3(.2f,.2f,.2f);
            newGo.SetSide(Soldier.Side.Player2);
            newGo.tag = "Enemy";
        }

        private void SpawnSoldier()
        {
            var newGo = Instantiate(PrefabHolder.Instance.GetPlayerSoldier
                (_selectedSoldier.soldier_type), _enemySpawnPoint );
            newGo.SetSoldierInfo(enemyDeck[_spawnTurn]);
            newGo.SetSide(Soldier.Side.Player2);
            newGo.transform.rotation = Quaternion.Euler(0,180,0);
            //newGo.transform.localScale = new Vector3(.2f,.2f,.2f);
            newGo.tag = "Enemy";
            GameManager.Instance.SoldierSpawned(newGo,true);
        }
        
        
    }
}