﻿using UnityEngine;
using UnityEngine.Audio;
using UnityEngine.UIElements;
using UnityEngine.Audio;

namespace Manager
{
    public class AudioManager : MonoBehaviour
    {
        public AudioMixer audioMixer;
        private float _masterVolume;
        private float _musicVolume;
        
        // Start is called before the first frame update
        void Start()
        {
            _masterVolume = PlayerPrefs.GetFloat("MasterVol",1);
            _musicVolume = PlayerPrefs.GetFloat("MusicVol", 1);
            audioMixer.SetFloat("MusicVol", Mathf.Log10(_musicVolume) * 20);
            audioMixer.SetFloat("MasteVol", Mathf.Log10(_masterVolume) * 20);
        }

        // Update is called once per frame
        void Update()
        {
           
        }

        public void SetMasterVolume(float musicVolume)
        {
            audioMixer.SetFloat("MusicVol", Mathf.Log10(musicVolume) * 20);
            PlayerPrefs.SetFloat("MusicVol",musicVolume);
        }

        public void SetMusicVolume(float mastervolume)
        {
            audioMixer.SetFloat("MasterVol", Mathf.Log10(mastervolume) * 20);
            PlayerPrefs.SetFloat("MasterVol",mastervolume);
        }
        
    }
}
