﻿using System;
using System.Collections.Generic;
using _Scripts.UI.NoticePopUp;
using _Scripts.Util;
using _Scripts.Util.UI;
using UnityEngine;
using Util;

namespace _Scripts.Manager
{
    public class BackStackManager : MonoBehaviourSingleton<BackStackManager>
    {
        private Stack<AnimatedPanel> _backstack;

        private void Awake()
        {
            _backstack = new Stack<AnimatedPanel>();
        }
        
        

        private void Update()
        {
            if (Input.GetKeyUp(KeyCode.Escape))
            {
                if (NoticePopUpPanel.Instance.IsPopUpActive)
                {
                    NoticePopUpPanel.Instance.DeActive();
                    return;
                }
                Pop();
            }
        }

        public void Push(AnimatedPanel openedPanel)
        {
            if (_backstack.Count > 0)
            {
                if (_backstack.Peek() == openedPanel)
                {
                    return;
                }
            }
            // openedPanel.gameObject.SetActive(true);
            openedPanel.ActivePanel();
            _backstack.Push(openedPanel);
            // print(_backstack);
        }

        public void Pop()
        {
            if (_backstack.Count > 0)
            {
                _backstack.Peek().DeActivePanel();
                print(_backstack.Peek());
                _backstack.Pop();
                if (_backstack.Count > 0)
                {
                    _backstack.Peek().ActivePanel();    
                }
            }
            else
            {
                //PopUpManager.Instance.AcceptExit();
                NoticePopUpPanel.Instance.Exit();
            }
            
        }
    }
}