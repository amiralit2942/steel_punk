using System;
using System.Threading.Tasks;
using _Scripts.Model;
using Backman.Client;
using Backman.Identity.Models;
using UnityEngine;

namespace _Scripts.Manager
{
    public class BackmanManager : MonoBehaviour
    {
        private bool _isSuccessful;
        private string _refreshToken;
        private User _user;
        private void Awake()
        {
            DontDestroyOnLoad(this);
        }

        private async void Start()
        {
            BackmanClient.Initial();
          _isSuccessful = await BackmanClient.IdentityManager.SignIn();

          if (_isSuccessful)
          {
              _refreshToken = BackmanClient.IdentityManager.GetRefreshToken();
              _isSuccessful = await BackmanClient.IdentityManager.SignIn(_refreshToken);
              if (_isSuccessful)
              {
                  _user = await BackmanClient.IdentityManager.GetUserInfo();
                  SafeBox.Instance.BackManUser = _user;
              }
          }
        }
    }
}
