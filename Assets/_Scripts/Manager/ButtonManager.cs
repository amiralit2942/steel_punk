﻿using System;
using _Scripts.Controller.Card.Deck;
using _Scripts.Manager.Currencies;
using _Scripts.UI;
using _Scripts.UI.NoticePopUp;
using _Scripts.Util.UI;
using UnityEngine;
using Util;

namespace _Scripts.Manager
{
    public class ButtonManager : MonoBehaviourSingleton<ButtonManager>
    {
        [SerializeField] private AnimatedPanel mainMenu;
        [SerializeField] private AnimatedPanel deckPanel;
        [SerializeField] private AnimatedPanel settingPanel;
        [SerializeField] private AnimatedPanel shopPanel;
        private void Start()
        {
            //BackStackManager.Instance.Push(mainMenu);
            CurrencyUIManager.Instance.UpdateUI();
        }

        // public void Test()
        // {
        //     NoticePopUpPanel.Instance.NewNotice().WithContent("سلام کونی").WithAcceptButton("بریم کون هم بزاریم", () =>
        //     {
        //         NoticePopUpPanel.Instance.NewNotice().WithContent("کون هم میزاریم").WithAcceptButton("کون هم",
        //             () =>
        //             {
        //                 NoticePopUpPanel.Instance.NewNotice().WithContent("کون هم").WithAcceptButton("کیر خر", () =>
        //                 {
        //                 });
        //             });
        //     });
        // }
        public void TestMethod()
        {
            print(ChestManager.Instance.GetOpeningChest().gameObject);
            
        }
        
        
        public void OpenSetting()
        {
            //BackStackManager.Instance.Push(settingPanel);
            MiniSettingPanel.Instance.OnClick();
        }
        public void OpenShop()
        {
            BackStackManager.Instance.Push(shopPanel);
            MiniSettingPanel.Instance.Close();
        }
        
        public void OpenDeckPanel()
        {
            BackStackManager.Instance.Push(deckPanel);
            MiniSettingPanel.Instance.Close();
            //DeckPanel.Instance.ReloadCards();
        }

        public void PlayGame()
        {
            //PanelManager.Instance.OnPlayClick();
            DeckPanel.Instance.StartGameBtn();
        }
        
        
        
        
        
        
    }
}
