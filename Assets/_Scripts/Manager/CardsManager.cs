﻿using System.Collections.Generic;
using System.Linq;
using _Scripts.Model;
using Manager;
using Newtonsoft.Json;
using Util;

namespace _Scripts.Manager
{
    public class CardsManager : MonoBehaviourSingleton<CardsManager>
    {
        private List<CardsInfo> _ownedCards;
        private List<CardsInfo> _cardsToBuy;

        void Start()
        {
            // _ownedCards = new List<CardsInfo>();
            // _cardsToBuy = new List<CardsInfo>();
            // SafeBox.Instance.ownedCards.Clear();
            SetOwnedCardInfo();
            SetDeckInfo();
        }

        public void EarnsSoldier(UserCard userCard)
        {
            if (!SafeBox.Instance.userInfo.cards.Contains(userCard))
            {
                SafeBox.Instance.userInfo.cards.Add(userCard);
                SetOwnedCardInfo();
            }
            
        }


        public void SetOwnedCardInfo()
        {
            foreach (var cardInfo in SafeBox.Instance.cardsInfo)
            {
                foreach (var userCard in SafeBox.Instance.userInfo.cards)
                {
                    if (cardInfo.ID == userCard.card_id)
                    {
                        var cardId = SafeBox.Instance.ownedCards.Find(x => x.ID == cardInfo.ID);
                        if (cardId == null)//!SafeBox.Instance.ownedCards.Contains(cardInfo))
                        {
                            SafeBox.Instance.ownedCards.Add(cardInfo);
                            //_ownedCards.Add(cardInfo);
                        }
                    }
                }
            }
        
            // foreach (var card in _ownedCards )
            // {
            //     if (!SafeBox.Instance.ownedCards.Contains(card))
            //     {
            //         SafeBox.Instance.ownedCards.Add(card);
            //     }
            // }
            
            var tempCardsToBuy = SafeBox.Instance.cardsInfo.Except(SafeBox.Instance.ownedCards);
            foreach (var card in tempCardsToBuy)
            {
                if (SafeBox.Instance.cardsToBuy.Find(x => x.soldier_type == card.soldier_type) == null)
                {
                    SafeBox.Instance.cardsToBuy.Add(card);
                }    
            }
        }
        
        public void SetDeckInfo()
        {
            if (SafeBox.Instance.userInfo.deck_cards != null)
            {
                foreach (var card in SafeBox.Instance.userInfo.deck_cards)
                {
                    foreach (var cardsInfo in SafeBox.Instance.cardsInfo)
                    {
                        if (card.card_id == cardsInfo.ID)
                        {
                            if (!SafeBox.Instance.deck.Contains(cardsInfo))
                            {
                                SafeBox.Instance.deck.Add(cardsInfo);
                            }
                        }
                    }
                }
            }
        }

        public CardsInfo GETSoldierInfo(uint cardId)
        {
            return SafeBox.Instance.cardsInfo.Find(x => x.ID == cardId);
        }

        public SpellModel GETSpellInfo(UserSpell spell)
        {
            
            if (SafeBox.Instance.spellsInfo == null)
            {
                SafeBox.Instance.spellsInfo = new List<SpellModel>();
            }
            var spellModel = SafeBox.Instance.spellsInfo?.Find(x => x.id == spell.spellId);
            print(JsonConvert.SerializeObject(spellModel));
            return spellModel;
        }
        
    }
}