﻿using System;
using System.Collections.Generic;
using _Scripts.Controller.Card;
using _Scripts.Controller.Card.Chest;
using _Scripts.DataHolder;
using _Scripts.Model;
using _Scripts.UI.NoticePopUp;
using _Scripts.Util;
using UnityEngine;
using Util;

namespace _Scripts.Manager.Currencies
{
    public class ChestManager : MonoBehaviourSingleton<ChestManager>
    {
        [SerializeField] private List<GameObject> _chestHolders;
        
        private List<UserChest> _userChests;
        private List<ChestCard> _chestCards;
        [SerializeField] private int maximumChest = 4;
        private uint _openingChestInfo;
        
        //BOOLS
        private bool _canGetChest;
        private bool _canUnlockChest;
        
        public bool CanUnlockChest
        {
            get;
            set;
        }
        
        
        
        private void Start()
        {

            _chestCards = new List<ChestCard>();


            SpawnChests();
            
            

            _openingChestInfo = SafeBox.Instance.userInfo.unlocking_user_chest_id;
            // _openingChestInfo = SafeBox.Instance.userInfo.unlocking_user_chest;
            if (SafeBox.Instance.userInfo.unlocking_user_chest_id != 0)
            {
                var openingChest = _chestCards.Find(x => x.UserChest.ID == _openingChestInfo);
                print(openingChest.gameObject.name);
                openingChest.SetTimer();
            }
        }

        public void EarnChest()
        {
            if (_canGetChest)
            {
                
            }
        }
        

        public void SpawnChests()
        {
            // for (int i = 0; i < SafeBox.Instance.userInfo.user_chests.Count; i++)
            // {
            //     var chest = SafeBox.Instance.userInfo.user_chests[i];
            //     if (chest.chest_state == ChestState.ChestEmpty)
            //     {
            //         SafeBox.Instance.userInfo.user_chests.Remove(chest);
            //     }   
            // }
            
            _userChests = SafeBox.Instance.userInfo.user_chests;
            
            
            
            var chestHolderIndex = 0;
            if (_userChests.Count > 0)
            {
                for (var index = 0; index < _userChests.Count; index++)
                {
                    if (_userChests[index].chest_state != ChestState.ChestEmpty)
                    {
                        var chest = _userChests[index];
                        var chestCard = Instantiate(PrefabHolder.Instance.GetChestCard(chest.chest_type)
                            , _chestHolders[chestHolderIndex].transform);
                        chestCard.SetInfo(chest);
                        // chestCard.transform.localPosition = Vector3.zero;
                        // chestCard.transform.SetParent(_chestHolders[chestHolderIndex].transform);
                        _chestCards.Add(chestCard);
                        chestHolderIndex++; 
                        //You Can Remove This Part Later
                        if (chestHolderIndex == 4)
                        {
                            break;
                        }
                    }
                }
            }
        }

        public void SetChests(List<UserChest> chests)
        {
            _userChests = chests;
        }

        
        public bool CheckCanUnlockChest()
        {
            if (SafeBox.Instance.userInfo.unlocking_user_chest_id == 0)
            {
                return true;
            }

            return false;
        }
        
        public void OpenChestWithGem()
        {
            var requiredGem = OpenChestWithGemPrice();
            if (GemManager.Instance.CheckGem(requiredGem))
            {
                ServerManager.Instance.OpenChestWithGem(SafeBox.Instance.userInfo.unlocking_user_chest_id, () =>
                {
                    // GemManager.Instance.SpendGem(requiredGem, Strings.ItemTypeGem
                    //     , Strings.ItemTypeChest, Strings.FromChestPanel);
                    GetOpeningChest().Open();
                }, (response) =>
                {
                    print("Unlock With Gem" + response);
                });
            }
            else
            {
                NoticePopUpPanel.Instance.NewNotice().WithContent(Strings.NotEnoughGem + "بریم یکم بخری؟")
                    .WithAcceptButton(ButtonManager.Instance.OpenShop).WithRejectButton();
            }
        }

        public ChestCard GetOpeningChest()
        {
            var openingChestInfo = SafeBox.Instance.userInfo.unlocking_user_chest_id;
            if (openingChestInfo != 0)
            {
                foreach (var chest in _chestCards)
                {
                    if (chest.UserChest.ID == openingChestInfo)
                    {
                        return chest;
                    }
                }
            }
            return null;
        }
        public int OpenChestWithGemPrice()
        {
            return (int)(GetOpeningChest().chestTimer.GetRemainingTime() * 1f / 3600) * 5 + 1;
        }

        public void ChestUnlocked()
        {
            SafeBox.Instance.userInfo.unlocking_user_chest_id = 0;
            CheckCanUnlockChest();
        }

        public void ChestOpened(uint chestID)
        {
            var openedUserChest = SafeBox.Instance.userInfo.user_chests.Find(x => x.ID == chestID);
            var openedChestCard = _userChests.Find(x => x.ID == chestID);
            _userChests.Remove(openedChestCard);
            SafeBox.Instance.userInfo.user_chests.Remove(openedUserChest);
        }
        
    }
}
