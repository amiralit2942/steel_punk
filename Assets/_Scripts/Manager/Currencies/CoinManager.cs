﻿using _Scripts.Analytics;
using Manager;
using _Scripts.Model;
using _Scripts.UI;
using GameAnalyticsSDK;
using UI;
using Util;

namespace _Scripts.Manager.Currencies
{
    public class CoinManager : MonoBehaviourSingleton<CoinManager>
    {
        
        
        
        private int _earnedCoin;
        private int _coin ;
        private int _coinBonus = 500;
        private int _coinPerGem = 100;
        
        private int _gemToUse;


        
        void Start()
        {
            DontDestroyOnLoad(this);
            _coin = SafeBox.Instance.userInfo.coin;
        }


        public int GameOverCoinPrize(int score ,bool win)
        {
            if (win)
            {
                //_earnedCoin = ;
            }
            else
            {
                _earnedCoin = _coinBonus * score;
            }
            return _earnedCoin;
        }
        
        // we call this method in upgrade method of upgradeble items to check the XP
        // public bool CheckCoin(int price)
        // {
        //     if (_coin >=  price)
        //     {
        //         BuyItem(price);
        //         print("Bought");
        //         return true;
        //     }
        //     else
        //     {
        //         var neededMarsh = price - _coin;
        //         ConvertCoinToGem(neededMarsh);
        //         if (GemManager.Instance.CheckGem(_gemToUse))
        //         {
        //             PanelManager.Instance.NotEnoughCoin(_gemToUse,true);    
        //         }
        //         else
        //         {
        //             PanelManager.Instance.NotEnoughCoin(_gemToUse,false);
        //         }
        //         print("NotEnoughMarsh");
        //         return false;
        //     }
        // }

        public bool CheckCoin(int amount)
        {
            return _coin >= amount;
        }
        public void BuyItem(int price,string itemType,string itemName,string from)
        {
            _coin -= price;
            SafeBox.Instance.userInfo.coin = _coin;
            CurrencyUIManager.Instance.UpdateUI();
            AnalyticsManager.Instance.SinkFlow("Coin",price,itemType,itemName,from);
            //PanelManager.Instance.SetCurrencies();
        }
        
        public void EarnCoin(int amount,string from,string itemType ="",string itemName ="")
        {
            _coin += amount;
            SafeBox.Instance.userInfo.coin = _coin;
            CurrencyUIManager.Instance.UpdateUI();
            AnalyticsManager.Instance.SourceFlow("Coin",amount,itemType,itemName,from);
        }
        public int GetCoin()
        {
            _coin = SafeBox.Instance.userInfo.coin;
            return _coin;
        }

        public int ConvertCoinToGem(int neededMarsh)
        {
            // 100 Marsh = 1 Gem
            _gemToUse = neededMarsh / _coinPerGem;
            return _gemToUse;
        }

        
    }
}