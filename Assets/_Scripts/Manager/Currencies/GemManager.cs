﻿using _Scripts.Analytics;
using _Scripts.UI;
using Manager;
using UnityEngine;
using UPersian.Components;
using Util;

namespace _Scripts.Manager.Currencies
{
    public class GemManager : MonoBehaviourSingleton<GemManager>
    {

        private const string CURRENCY_GEM = "Gem";
        public const string ITEM_TYPE_CCC = "ccc";
        
        private int _gem;
        // Start is called before the first frame update
        void Start()
        {
            _gem = SafeBox.Instance.userInfo.gem;
        }


        public void EarnGem(int earnedGem,string from,string itemType="",string itemName="")
        {
            _gem += earnedGem;
            SafeBox.Instance.userInfo.gem = _gem;
            CurrencyUIManager.Instance.UpdateUI();
            AnalyticsManager.Instance.SourceFlow(CURRENCY_GEM,earnedGem,itemType,itemName,from);
        }

        public void SpendGem(int price,string itemType,string itemName,string from)
        {
            _gem -= price;
            SafeBox.Instance.userInfo.gem = _gem;
            CurrencyUIManager.Instance.UpdateUI();
            AnalyticsManager.Instance.SinkFlow(CURRENCY_GEM,price,itemType,itemName,from);
        }

        public int GetGem()
        {
            _gem = SafeBox.Instance.userInfo.gem;
            return _gem;
        }

        public bool CheckGem(int gemToSpend)
        {
            if (gemToSpend <= _gem)
            {
                _gem -= gemToSpend;
                print("Gem Used" + _gem);
                //PanelManager.Instance.SetCurrencies();
                return true;
            }
            else
            {
                print("Not Enough Gem");
                //Show Not Enough Gem Panel
                return false;
            }
        }

        

        
    }
}
