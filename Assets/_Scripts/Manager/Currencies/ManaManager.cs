﻿using System;
using System.Collections.Generic;
using DG.Tweening;
using UnityEngine;
using UnityEngine.UI;
using Util;

namespace _Scripts.Manager.Currencies
{
    public class ManaManager : MonoBehaviourSingleton<ManaManager>
    {

        [SerializeField] public Text manaText;
        [SerializeField] private Button manaUpgradeButton;
        [SerializeField] private Image coolDownImage;
        [SerializeField] private Image manaSlider;
        [SerializeField] private Image manaUnLockedField;
        [SerializeField] private Image manaLockedField;
        [SerializeField] private float manaSliderMovementSpeed;
        private float _mana = 0;
        private float _aiMana = 0;
        public  List<float> manaIncomeRate = new List<float>() ;
        public  List<float> maxMana = new List<float>();

        private float _incomeRate;
        private float _maxMana;
        
        private int _manaLevel ;
        private float _timer;
        private bool _canUse;
        public bool CanUse => _canUse;
        private bool _isManaMaxLevel = false;
        public void Start()
        {
            ManaUpdate();
            UpdateManaAmount();
        }

        private void OnEnable()
        {
            _manaLevel = 0;
            manaIncomeRate = SafeBox.Instance.manaIncomeRate;
            maxMana = SafeBox.Instance.manMax;
            _incomeRate = manaIncomeRate[_manaLevel];
            _maxMana = maxMana[_manaLevel];
            _canUse = false;
            CalculateManaSliderFields();
            coolDownImage.fillAmount = 1;
            manaUnLockedField.fillAmount = .5f;
            manaLockedField.fillAmount = .5f;
            
            GameManager.Instance.ManaTrigger += OnManaTriggerFire;
        }

        private void OnDisable()
        {
            GameManager.Instance.ManaTrigger -= OnManaTriggerFire;
        }

        private void OnManaTriggerFire()
        {
            ManaUpdate();
            UpdateManaAmount();
            AiMana();
        }

        private Tweener _manaUpdater;
        
        private void ManaUpdate()
        {
            var target = Mathf.Clamp(_mana + _incomeRate, 0, _maxMana);
            _manaUpdater = DOTween.To(() => _mana, (value) => _mana = value, target, 1f).OnUpdate(() =>
            {
                //MAXMana[_manaLevel / 2];
                UpdateManaAmount();
                if (!_isManaMaxLevel)
                {
                    coolDownImage.fillAmount =  (float) _mana / (_maxMana * 0.8f);
                    if (_mana >= _maxMana * .8f) //MAXMana[_manaLevel])
                    {
                        if (_mana >= _maxMana)
                        {
                            _mana = _maxMana;
                        }

                        //MAXMana[_manaLevel];
                        manaUpgradeButton.interactable = true;
                        _canUse = true;
                    }
                    else
                    {
                        manaUpgradeButton.interactable = false;
                        _canUse = false;
                    }
                }
            });
            
            // _mana = (int)Mathf.Lerp(_mana, _mana + _incomeRate, 1);
            // print("Mana Updated: " +_mana );
            // UpdateManaAmount();
            // _mana += _incomeRate;//ManaIncomeRate[_manaLevel];
        }

        private void AiMana()
        {
            _aiMana += _incomeRate;//ManaIncomeRate[_manaLevel];
            if (_aiMana >= _maxMana)//MAXMana[_manaLevel])
            {
                _aiMana = _maxMana; //MAXMana[_manaLevel];
            }
        }

        public float GetAiMana()
        {
            return _aiMana;
        }

        public void UseAiMana(int manaPrice)
        {
            if (manaPrice <= _aiMana)
                _aiMana -= manaPrice;
        }

        public void Cheat()
        {
            _mana += 9000;
            _maxMana = 10000;
            _incomeRate = 50;
        }
        
        
        private void UpdateManaAmount()
        {
            // manaText.text = /*MAXMana[_manaLevel]*/_maxMana + " / " + _mana.ToString() ;
            
            //manaUnLockedField.DofillAmount = _mana * _upgradePercents[_manaLevel] / _maxMana;
            manaUnLockedField.DOFillAmount(_mana * (0.5f +eachManaLevelFillAmount[_manaLevel]) / _maxMana, manaSliderMovementSpeed);
            manaText.text = $"{_mana}/{_maxMana}";
            GameManager.Instance.CheckCardAvailability();
        }

        public void UpgradeManaLevel()
        {
            _manaLevel++;
            SetMana(_maxMana / 2);
            _maxMana = maxMana[_manaLevel];
            _incomeRate = manaIncomeRate[_manaLevel];
            UpgradeManaSlider();
            if (_manaLevel == manaIncomeRate.Count - 1)
            {
                _isManaMaxLevel = true;
                manaUpgradeButton.interactable = false;
                coolDownImage.fillAmount = 0;
            }
        }    

        public float GetMana()
        {
            return _mana;
        }

        public void SetMana(float newMana)
        {
            _manaUpdater?.Kill();
            if (newMana < 0)
            {
                newMana = 0;
            }
            _mana -= newMana;
            //manaUnLockedField.DOFillAmount(_mana * _upgradePercents[_manaLevel] / _maxMana, manaSliderMovementSpeed);
            UpdateManaAmount();
            ManaUpdate();
        }

        public int GetManaLevel()
        {
            return _manaLevel;
        }

        public float GetMaxMana()
        {
            return _maxMana; //MAXMana[_manaLevel];
        }

        public float GetInconeRate()
        {
            return _incomeRate;
        }

        public List<float> eachManaLevelFillAmount = new List<float>();
        
        public void CalculateManaSliderFields()
        {
            var manaSliderDiff = 0.5f / (manaIncomeRate.Count - 1);
            eachManaLevelFillAmount.Add(0);
            for (int i = 1; i <= manaIncomeRate.Count - 1; i++)
            {
                eachManaLevelFillAmount.Add(manaSliderDiff * i);
            }
        }
        
        private void UpgradeManaSlider()
        {
            manaLockedField.DOFillAmount(0.5f - eachManaLevelFillAmount[_manaLevel], 0.5f);
            print("Mana Level : " + _manaLevel);
        }

    }       
        
}