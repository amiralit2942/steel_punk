﻿using System.Collections.Generic;
using _Scripts.Controller.Abstract;
using _Scripts.Model;
using Manager;
using Util;

namespace _Scripts.Manager
{
    public class DeckManager : MonoBehaviourSingleton<DeckManager>
    {
        private List<CardsInfo> _cards;
        private List<Soldier.Soldiers> ownedSoldiers;    
        private List<Soldier.Soldiers> equipedSolders;
        
        
        // Start is called before the first frame update
        private void Awake()
        {
           // DontDestroyOnLoad(gameObject);
            _cards = new List<CardsInfo>();
            _cards.AddRange(SafeBox.Instance.ownedCards);
            ownedSoldiers = new List<Soldier.Soldiers>();
            equipedSolders = new List<Soldier.Soldiers>(5);
            foreach (var card in _cards)
            {
                
            }
            //SpawnOwnedSoldiers();            
        }
        

        private void SpawnOwnedSoldiers()
        {
           // ownedSoldiers.AddRange(UserInfo.Instance.deck);
            equipedSolders.Add(Soldier.Soldiers.Sopo);
            equipedSolders.Add(Soldier.Soldiers.Tank);
            equipedSolders.Add(Soldier.Soldiers.Cop);
        }

        public List<Soldier.Soldiers> GetOwnedSoldiers()
        {
            return ownedSoldiers;
        }

        public List<Soldier.Soldiers> GetEquipedSoldiers()
        {
            
            if (equipedSolders.Count < 3)
            {
                for (int i = 0; i < ownedSoldiers.Count; i++)
                {
                    equipedSolders.Add(ownedSoldiers[i]);
                    if (equipedSolders.Count >= 5)
                    {
                        break;
                    }
                }
            }
            // Post new Deck
            return equipedSolders;
        }

        public void SetEquipedSoldiers(List<Soldier.Soldiers> equipedSoldiers)
        {
            equipedSolders.AddRange(equipedSoldiers);
            // Post New Deck 
        }
        
        
    }
}
