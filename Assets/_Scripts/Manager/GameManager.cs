﻿using System;
using System.Collections;
using System.Collections.Generic;
using _Scripts.Controller;
using _Scripts.Controller.Player;
using _Scripts.DataHolder;
using _Scripts.Manager.Currencies;
using _Scripts.Model;
using _Scripts.Controller.Abstract;
using DG.Tweening;
using Manager;
using UI;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using Util;
using _Scripts.Controller.Card;
using _Scripts.Controller.Card.Chest;
using _Scripts.Controller.Spells;
using _Scripts.Soldiers;
using _Scripts.UI;
using _Scripts.UI.NoticePopUp;
using _Scripts.Util;
using TMPro;
using UnityEngine.U2D;
using UPersian.Components;

namespace _Scripts.Manager
{
    public class GameManager : MonoBehaviourSingleton<GameManager>
    {
        [SerializeField] private Base playerBase;
        [SerializeField] public Transform playerSpawnPoint;
        [SerializeField] public Transform enemySpawnPoint;
        [SerializeField] private Transform soldierScPanel;
        [SerializeField] private Transform spellScPanel;
        [SerializeField] private Button manaBoostButton;
        [SerializeField] private Image gameOverPanel;
        [SerializeField] private Sprite winPic;
        [SerializeField] private Sprite loosPic;
        [SerializeField] private TMP_Text gameOverText;
        [SerializeField] private TMP_Text prizeAmount;
        [SerializeField] public GameObject coinUpgradeParticle;
        [SerializeField] public Text coinUpgradedAmount;
        [SerializeField] private LoadingPanel newLoadingPanel;
        [SerializeField] private Color freezeColor;
        private List<UserSpell> _usedSpells = new List<UserSpell>(2);
        public Color FreezeColor => freezeColor;

        private int _earnedMarshmello;
        private int _score;
        private uint _winnerID;
        private int _earnedCoin;
        public event Action ManaTrigger;
        private List<NormalSoldier> _fieldSoldiers;
        public List<NormalSoldier> FieldSoldiers => _fieldSoldiers;
        public List<CardsInfo> soldierInfos;
        public CardsInfo baseInfos;
        public List<UserCard> playerSoldierDeck;
        public List<UserSpell> playerSpellDeck;
        private List<SoldierSpawnCard> _soldierSpawnCards;
        private List<CardsInfo> _enemyDeck;
        public List<CardsInfo> EnemyDeck => _enemyDeck;
        private List<NormalSoldier> _playerSpawnedSoldiers;
        private List<NormalSoldier> _enemySpawnedSoldier;
        private List<Spell> _spells;
        private void Awake()
        {
            baseInfos = SafeBox.Instance.cardsInfo.Find(x => x.soldier_type == Soldier.Soldiers.Base);
            _fieldSoldiers = new List<NormalSoldier>();
            playerSpellDeck = new List<UserSpell>();
            _soldierSpawnCards = new List<SoldierSpawnCard>(5);
            playerSoldierDeck = SafeBox.Instance.userInfo.deck_cards;
            playerSpellDeck = SafeBox.Instance.userInfo.deck_spells;
            soldierInfos = SafeBox.Instance.cardsInfo;
            SpawnCard();
            _playerSpawnedSoldiers = new List<NormalSoldier>();
            _enemySpawnedSoldier = new List<NormalSoldier>();
        }

        private void Start()
        {
            StartCoroutine(nameof(CallMana));
            InvokeRepeating(nameof(CallCoolDown),0,1f);
            gameOverPanel.transform.localPosition = new Vector3(0,0,0);
            gameOverPanel.gameObject.SetActive(false);
        }
            
        public void CallCoolDown()
        {
            foreach (var card in _soldierSpawnCards)
            {
                card.CoolDown();
            }
        }
        
        public IEnumerator CallMana()
        {
            while (true)
            {
                ManaTrigger?.Invoke();
                yield return new WaitForSeconds(1);
            }
        }
        
        private void SpawnCard()
        {
            foreach (var userCard in playerSoldierDeck)
            {
                var cardInfo = SafeBox.Instance.cardsInfo.Find(x => x.ID == userCard.card_id);
                if (cardInfo.soldier_type != Soldier.Soldiers.Base)
                {
                    var card = Instantiate(PrefabHolder.Instance.GetCard(), soldierScPanel);
                    card.SetUserCard(userCard);
                    _soldierSpawnCards.Add(card);    
                }
            }

            
            if (SafeBox.Instance.userInfo.deck_spells == null)
            {
                SafeBox.Instance.userInfo.deck_spells = new List<UserSpell>();
            }
            
            foreach (var spell in SafeBox.Instance.userInfo.deck_spells)
            {
                var gamePlayCard = Instantiate(PrefabHolder.Instance.GetSpellGamePlayCard(), spellScPanel);
                print("1 Spell ID: " + spell.spellId);
                gamePlayCard.SetInfo(spell);
            }
            
        }
        

        public void GameOver(bool win)
        {
            
            AiManager.Instance._matchEnded = true;
            GamePlaySceneUi.Instance.GameOver(win);
            //_score = SafeBox.Instance.level * 200;
            // gameOverPanel.gameObject.SetActive(true);
            // gameOverPanel.transform.localPosition = new Vector3(0,0,0);
            
            if (win)
            {
                // gameOverPanel.sprite = winPic;
                // gameOverText.text = Strings.YouWin;
                _winnerID = SafeBox.Instance.userInfo.id;
            }
            else
            {
                // gameOverPanel.sprite = loosPic;
                // gameOverText.text = Strings.YouLost;
                _winnerID = 0;
            }
            print(SafeBox.Instance.roomName);
            ServerManager.Instance.EndMatch(SafeBox.Instance.roomName,SafeBox.Instance.userInfo.id,0,
                false,false,_usedSpells,_winnerID,true, () =>
                {
                    if (win)
                    {
                        // CoinManager.Instance.EarnCoin(endMatchField.win_prize,"Wining");
                        // prizeAmount.text = endMatchField.win_prize.ToString();
                        print("Successful");
                    }
                    else
                    {
                        // CoinManager.Instance.EarnCoin(endMatchField.lose_price,"Loosing");
                        // prizeAmount.text = endMatchField.lose_price.ToString();
                        print("Successful");
                    }
                    print("Winner ID: " + _winnerID);
                    ServerManager.Instance.Login((userInfo) =>
                        {
                            SafeBox.Instance.userInfo = userInfo;
                        }, () =>
                    { }, (response) =>
                    {
                        print("Login:" + response);
                    });
                    
                }, (statusCode, response) =>
                {
                    print("EndMatch : " + statusCode);
                    print("EndMatch response : " + response);
                    print(response);
                });
        }

        public void PlayAgain()
        {
            GamePlaySceneUi.Instance.DisableGameOverPanel();
            ServerManager.Instance.MatchStart(SafeBox.Instance.roomName , SafeBox.Instance.userInfo.id, 0,
                SafeBox.Instance.userInfo.name, "", DateTime.Now, SafeBox.Instance.userInfo.deck_cards, null,
                () =>
                {
                    AiManager.Instance._matchEnded = false;
                    //SceneManager.LoadScene(2);
                    newLoadingPanel.FakeLoading(2);
                    Time.timeScale = 1;
                    gameOverPanel.transform.localPosition = new Vector3(0,-1080,0);
                    gameOverPanel.gameObject.SetActive(false);
                    SafeBox.Instance.gameState = GameState.InGame;
                }, (statusCode,response) =>
                {
                    NoticePopUpPanel.Instance.NewNotice().WithContent(Strings.MatchMakingFailed).WithRejectButton(OpenMenu);
                    
                    //PopUpManager.Instance.MatchNotFound();
                    print("Match Start : " + statusCode);
                    print(response);
                });
            
        }

        public void OpenMenu()
        {
            GamePlaySceneUi.Instance.DisableGameOverPanel();
            newLoadingPanel.FakeLoading(1);
            //SceneManager.LoadScene(1);
            Time.timeScale = 1;
//            SettingPanel.Instance.SetMenuMusic();
 //           PanelManager.Instance.OnStartClick();
            gameOverPanel.transform.localPosition = new Vector3(0,-1080,0);
            gameOverPanel.gameObject.SetActive(false);
            SafeBox.Instance.gameState = GameState.InMenu;
        }

        public void OnManaBoostClick()
        {
            var canBoost = ManaManager.Instance.CanUse;
            if (canBoost)
            {
                ManaManager.Instance.UpgradeManaLevel();
                ManaManager.Instance.manaText.gameObject.SetActive(false);
                coinUpgradeParticle.SetActive(true);
                coinUpgradedAmount.gameObject.SetActive(true);
                coinUpgradedAmount.text = ManaManager.Instance.GetInconeRate().ToString();
                coinUpgradedAmount.DOFade(1, 1f).OnComplete(() =>
                {
                    StartCoroutine(Wait(2));
                    coinUpgradedAmount.DOFade(0, 1f).OnComplete(() =>
                    {
                        coinUpgradedAmount.gameObject.SetActive(false);
                        ManaManager.Instance.manaText.gameObject.SetActive(true);
                    });
                });
                
                
            }
        }

        public void BoostTest()
        {
            ManaManager.Instance.UpgradeManaLevel();
            ManaManager.Instance.manaText.DOFade(0f, .5f);
            ManaManager.Instance.manaText.gameObject.SetActive(false);
            coinUpgradeParticle.SetActive(true);
            coinUpgradedAmount.gameObject.SetActive(true);
            coinUpgradedAmount.text = "+" + ManaManager.Instance.GetInconeRate().ToString();
            coinUpgradedAmount.DOFade(1, 1).OnComplete(() =>
            {
                StartCoroutine(Wait(2));
                coinUpgradedAmount.DOFade(0, 1.5f).OnComplete(() =>
                {
                    coinUpgradedAmount.gameObject.SetActive(false);
                    ManaManager.Instance.manaText.gameObject.SetActive(true);
                    ManaManager.Instance.manaText.DOFade(1, 0.5f);
                });
            });
        }

        private IEnumerator Wait(int seconds)
        {
            yield return new WaitForSeconds(seconds);
        }

        public void CheckCardAvailability()
        {
            //TODO:
            var mana = ManaManager.Instance.GetMana();
            foreach (var card in _soldierSpawnCards)//_deckCards)
            {
                var spawnCoolDown = card.GetSpawnCoolDown();
                if (card.GetRequiredMana() <= mana && spawnCoolDown)
                {
                    // card can be used
                    //Set Active Color
                    card.SetIsActive(true);
                } else if (card.GetRequiredMana() > mana || !spawnCoolDown)
                {
                    // card is deactivated
                    //Set Active Color
                    card.SetIsActive(false);
                }
            }
        }


        /*public void ActiveSpells(Spell.SpellType spellType,int amount)
        {
            switch (spellType)
            {
                case Spell.SpellType.HealSpell:
                    HealSpell(amount);
                    break;
                case Spell.SpellType.SpeedSpell:
                    SpeedSpell(amount);
                    break;
            }
        }*/



        public void HealSpell(int hp)
        {
            if (_playerSpawnedSoldiers.Count > 0)
            {
                foreach (var soldier in _playerSpawnedSoldiers)
                {
                    soldier.Heal(hp);
                }
            }
        }

        public void SpeedSpell(float boostAmount, float duration)
        {
            foreach (var soldier in _playerSpawnedSoldiers)
            {
                soldier.SpeedUp(boostAmount,duration);
            }
        }
        

        public void SpeedSpell(float boostAmount)
        {
            foreach (var soldier in _playerSpawnedSoldiers)
            {
                soldier.SpeedUp(boostAmount,4);
            }
        }

        public void FreezeSpell(float duration)
        {
            if (_enemySpawnedSoldier.Count > 0)
            {
                foreach (var soldier in _enemySpawnedSoldier)
                {
                    soldier.Freeze(duration);
                }
            }
        }

        public void SoldierSpawned(NormalSoldier soldier, bool isEnemy)
        {
            if (isEnemy)
            {
                _enemySpawnedSoldier.Add(soldier);
            }
            else
            {
                _playerSpawnedSoldiers.Add(soldier);    
            }
            
        }

        public void SoldierDied(NormalSoldier soldier, bool isEnemy)
        {
            if (isEnemy)
            {
                _enemySpawnedSoldier.Remove(soldier);
            }
            else
            {
                _playerSpawnedSoldiers.Remove(soldier);    
            }
            
        }

        public void SpellUsed(UserSpell userSpell)
        {
            _usedSpells.Add(userSpell);
        }

        public void Cheat()
        {
            ManaManager.Instance.Cheat();
            playerBase.Cheat();
        }
    }
}