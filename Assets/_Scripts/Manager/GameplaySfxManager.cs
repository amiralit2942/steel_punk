using System;
using System.Collections.Generic;
using _Scripts.Controller.Abstract;
using UnityEngine;
using Util;
using Random = UnityEngine.Random;

namespace _Scripts.Manager
{
    [Serializable]
    public class GameplaySfxHolder
    {
        public Soldier.Soldiers soldierType;
        public List<AudioClip> attackSfx;
        public List<AudioClip> dieSfx;
    }


    public enum GameplaySfxType
    {
        Attack,
        Death
    }

    public class GameplaySfxManager : MonoBehaviourSingleton<GameplaySfxManager>
    {
        [SerializeField] private AudioSource audioSource;
        [SerializeField] private List<GameplaySfxHolder> sfxHolders;
        [SerializeField] private List<AudioSource> audioSources;


        private List<AudioClip> _listToPlay;
        public void PlayClip(Soldier.Soldiers soldierType, GameplaySfxType type)
        {
            var sfxHolder = sfxHolders?.Find(x => x.soldierType == soldierType);
            if (sfxHolder != null)
            {
                if (type == GameplaySfxType.Attack)
                {
                    _listToPlay = sfxHolder.attackSfx;
                    //audioSource.PlayOneShot(sfx[Random.Range(0, sfx.Count)]);
                }
                else
                {
                    _listToPlay = sfxHolder.dieSfx;
                    //audioSource.PlayOneShot(sfx[Random.Range(0, sfx.Count)]);
                }
                FindAudioSource().PlayOneShot(_listToPlay[Random.Range(0,_listToPlay.Count)]);
            }
        }

        public void PlayClip(AudioClip audioClip)
        {
            FindAudioSource().PlayOneShot(audioClip);
        }

        
        private AudioSource FindAudioSource()
        {
            foreach (var audioSource in audioSources)
            {
                if (!audioSource.isPlaying)
                {
                    return audioSource;
                }
            }
            
            return audioSources[Random.Range(0,audioSources.Count)];
        }
    }
}