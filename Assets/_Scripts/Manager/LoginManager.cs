﻿using System;
using _Scripts.Manager;
using UI;
using UnityEngine;
using UnityEngine.UI;
using DG.Tweening;
namespace Manager
{
    public class LoginManager : MonoBehaviour
    {
        [SerializeField] private InputField nameInputField;
        [SerializeField] private Button singInBtn;
        
        // Start is called before the first frame update
        void Start()
        {
            singInBtn.interactable = false;
            
                /* ServerManager.Instance.GetUser((UserInfo)=> {}, () =>
            {
             //PanelManager.Instance.registerPanel.SetActive(true);   
             PanelManager.Instance.DisplayRegisterPanel();
            }, (int errorCode) =>
            {
                //PanelManager.Instance.SetRegisterErrorPanel(errorCode);
            });*/
        }

        // Update is called once per frame
        void Update()
        {
            if ( nameInputField.text.Length >= 2)
            {
                singInBtn.interactable = true;
            }
        }

        public void Register()
        {
            PanelManager.Instance.loadingPanel.SetActive(true);
            //PanelManager.Instance.loadingPanel.transform.DOMove(new Vector3(0, 0, 0), 0.5f);
            
            ServerManager.Instance.SetRegister(nameInputField.text, () =>
            {
                //PanelManager.Instance.loadingPanel.transform.DOMove(new Vector3(0, -1080,0), 0.5f);
                PanelManager.Instance.loadingPanel.SetActive(false);
            });
        }

        public void mammad()
        {
            
        }
    }
}
