﻿using System;
using System.Collections.Generic;
using _Scripts.DataHolder;
using _Scripts.Manager.Currencies;
using _Scripts.Model;
using TMPro;
using UnityEngine;
using UnityEngine.UI;
using Util;

namespace _Scripts.Manager
{
    public class MinerManager : MonoBehaviourSingleton<MinerManager>
    {
        
        [SerializeField] private TMP_Text remainingTimeText;
        [SerializeField] private GameObject setTimerPanel;
        [SerializeField] private GameObject collectPrizePanel;
        [SerializeField] private Transform prizeSpawnPoint;
        [SerializeField] private Button minerButton;
        private List<UserChestPrizeStruct> _prizes;
        
        private bool _timerIsActive;
        private bool _gotPrize;
        
        
        private int activationType;
        private DateTime activationTime;
        private int _activationPeriod;
        private int _selectedIndex = 0;

        private float _totalTime;
        private float _second;
        private int _minute;
        private int _hour;

        private void Start()
        {
            //TODO Check If Miner is active from server
            _prizes = new List<UserChestPrizeStruct>();
            CheckButtonFunc();
            
            
        }

        void Update()
        {
            Clock();
        }



        public void CheckButtonFunc()
        {
            var newEvent = new Button.ButtonClickedEvent();
            if (!_timerIsActive)
            {
                if (!_gotPrize)
                {
                    remainingTimeText.text = "غیر فعال";
                    newEvent.AddListener(SetTimerPanel);
                }
                else
                {
                    remainingTimeText.text ="جایزه آماده ست";
                    newEvent.AddListener(CollectPrizePanel);    
                }
                minerButton.interactable = true;
                minerButton.onClick = newEvent;
                return;
            }
            minerButton.interactable = false;
            //Show Remaining Time
            //TODO get remaining time from the server
            SetTimer(1);
        }

        public void SetTimerPanel()
        {
            setTimerPanel.SetActive(true);
        }
        
        public void SetTimer(float time)
        {
            _totalTime = time;
            
            _minute = (int)_totalTime % 60;
            _hour = (int) _totalTime / 60;
            _timerIsActive = true;
            setTimerPanel.SetActive(false);
        }

        public void CollectPrizePanel()
        {
            collectPrizePanel.SetActive(true);
        }

        
        
        private int _openedPrizes;
        private GameObject _tempPrize;
        
        
        public void CollectPrize()
        {
            if (_openedPrizes == 0)
            {
                //TODO Play Animation
                //TODO OnComplete
                //SpawnContent(_prizes[0]);
                
                _openedPrizes++;
                return;
            }    
            //TODO Play Animation
            //TODO OnComplete
            Destroy(_tempPrize);
            //SpawnContent(_prizes[_openedPrizes]);
            if (_openedPrizes >= _prizes.Count)
            {
                CheckButtonFunc();
                collectPrizePanel.SetActive(false);
            }
        }

        public void CollectAllPrizes()
        {
            foreach (var prize in _prizes)
            {
                switch (prize.prizeType)
                {
                    case PrizeType.CoinPrizeType :
                        CoinManager.Instance.EarnCoin(prize.prizeAmount,"Miner");
                        break;
                    case PrizeType.GemPrizeType:
                        GemManager.Instance.EarnGem(prize.prizeAmount,"Miner","Miner Gem Pack");
                        break;
                    case PrizeType.CardPrizeType:
                        ShardManager.Instance.EarnShard(prize.cardId,prize.prizeAmount);
                        //TODO Add Cards To Card Upgrade Manager
                        break;
                }
            }
        }

        public void SendMinerButton()
        {
            //check if miner is back
            if (!_timerIsActive)
            {
                _activationPeriod = _selectedIndex;
                activationTime = DateTime.Now;
                //sendMiner.interactable = false;
                _timerIsActive = true;
            }
            else
            {
                print("Your Miner is not back YET!!!");
            }
        }

        


        public void Clock()
        {
            if (_timerIsActive)
            {
                _second -= 1 * Time.deltaTime;
                remainingTimeText.text =
                    $"{_hour.ToString("0")} h {_minute.ToString("0")} min {_second.ToString("0")} sec";

                if (_second <= 0)
                {
                    if (_minute > 0)
                    {
                        _minute--;
                        _second = 60;
                    }
                    else
                    {
                        if (_hour > 0)
                        {
                            _hour--;
                            _minute = 60;
                        }
                        else
                        {
                            remainingTimeText.text = "READY";
                            _second = 0;
                            _gotPrize = true;
                            _timerIsActive = false;
                            CheckButtonFunc();
                        }
                    }
                }
            }
        }
        
        // private void SpawnContent(UserChestPrizeStruct chestContent)
        // {
        //     var content = Instantiate(PrefabHolder.Instance.GetChestContentCard()
        //         , prizeSpawnPoint);
        //     content.transform.localPosition = new Vector3(0,0,0);
        //
        //     if (chestContent.prizeType == PrizeType.CardPrizeType)
        //     {
        //         content.SetInfo(chestContent.prizeType,chestContent.prizeAmount,chestContent.cardId);
        //     }
        //     else
        //     {
        //         content.SetInfo(chestContent.prizeType,chestContent.prizeAmount);
        //     }
        //
        //     _tempPrize = content.gameObject;
        //
        // }
    }
}