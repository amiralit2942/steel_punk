﻿using System;
using System.Collections.Generic;
using _Scripts.Util;
using UnityEngine;
using Util;
using Random = UnityEngine.Random;

namespace _Scripts.Manager
{
    public enum OneShopSfxType
    {
        NormalButton,
        BuyCurrency,
        BuySpell,
        UseSpell,
        SpawnSoldier,
        AbilityButton,
        ChangeSoldierDeck,
        ChangeSpellDeck
    }
    
    
    [Serializable]
    public class OneShotSfxHolder
    {
        [SerializeField] private OneShopSfxType type;
        [SerializeField] private List<AudioClip> sfx;

        public OneShopSfxType Type => type;
        public List<AudioClip> Sfx => sfx;
    }
    public class OneShotSfxManager: MonoBehaviourSingleton<OneShotSfxManager>
    {
        [SerializeField] private List<OneShotSfxHolder> sfxHolder;
        [SerializeField] private AudioSource audioSource;

        public void PlayOneShot(AudioClip clip)
        {
            audioSource.PlayOneShot(clip);
        }
        
        public void PlayOneShot(OneShopSfxType type)
        {
            var list = sfxHolder.Find(x => x.Type == type).Sfx;
            var clip = list[Random.Range(0, list.Count)];
            audioSource?.PlayOneShot(clip);
        }
        
    }
}