﻿using System;
using System.Collections;
using _Scripts.Manager.Currencies;
using _Scripts.Model;
using _Scripts.UI;
using _Scripts.UI.NoticePopUp;
using _Scripts.Util;
using DG.Tweening;
using Manager;
using TMPro;
using UI;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using UPersian.Components;
using Util;
using Button = UnityEngine.UI.Button;
using Image = UnityEngine.UI.Image;
using Slider = UnityEngine.UI.Slider;


namespace _Scripts.Manager
{
    public class PanelManager : MonoBehaviourSingleton<PanelManager>
    {
     
        [SerializeField] private GameObject firstPanel;
        [SerializeField] private GameObject mainMenuPanel;
        [SerializeField] private GameObject acceptExitPanel;
        [SerializeField] private GameObject equipPanel;
        [SerializeField] private GameObject upgradePanel;
        [SerializeField] private GameObject settingPanel;
        [SerializeField] private GameObject storePanel;
        [SerializeField] private GameObject minerPanel;
        [SerializeField] private GameObject headerPanel;
        [SerializeField] private GameObject warningPanel;
        [SerializeField] public GameObject marshmelloStorePanel;
        [SerializeField] public GameObject loadingPanel;
        [SerializeField] private TMP_Text loadingPanelText;
        
        
        
        
        
        [SerializeField] private RtlText warningText;
        [SerializeField] private RtlText acceptWarningPanelText;
        [SerializeField] private Button acceptWarningPanel;
        [SerializeField] private GameObject errorPanel;
        [SerializeField] private Text errorPanelText;
        [SerializeField] public  Image errorPanelImage;
        [SerializeField] public GameObject registerPanel;
        [SerializeField] private GameObject itemsToUpgradePanelSlider;
        [SerializeField] private GameObject itemsToBuyPanelSlider;
        [SerializeField] private Button itemsToUpgradeSliderButton;
        [SerializeField] private Button itemsToBuySliderButton;
        [SerializeField] private RectTransform firstPanelRct;
        [SerializeField] private Button startButton;
        [SerializeField] private RectTransform mainMenuPanelRect;
        [SerializeField] private RectTransform firstPanelRect;
        [SerializeField] private RectTransform acceptExitPanelRect;
        [SerializeField] private RectTransform equipPanelRect;
        [SerializeField] private RectTransform upgradePanelRect;
        [SerializeField] private RectTransform settingPanelRect;
        [SerializeField] private RectTransform storePanelRect;
        [SerializeField] private RectTransform headerPanelRect;
        [SerializeField] private RectTransform warningPanelRect;
        [SerializeField] private RectTransform marshmelloStorePanelRect;
        [SerializeField] private RectTransform loadingPanelRect;
        [SerializeField] private RectTransform errorPanelRect;
        [SerializeField] private RectTransform registerPanelRect;
        [SerializeField] public Transform coinStoreProductView;
        [SerializeField] public Transform gemStoreProductContainer;
        [SerializeField] public Transform buyItem;
        [SerializeField] public Transform upgradeItem;
        [SerializeField] private LoadingPanel newLoadingPanel;
        /*[SerializeField] private Button optionButton;
        [SerializeField] private Button exitButton;*/
        private int _scene = 0;
        private int _level;
        public float transactionDuration ;
        public bool snapping;
        

        // Start is called before the first frame update
        void Start()
        {
            //SetCurrencies();
            SetXp();
            // firstPanel.SetActive(false);
            // mainMenuPanel.SetActive(true);
            // acceptExitPanel.SetActive(false);
            // equipPanel.SetActive(false);
            // upgradePanel.SetActive(false);
            // settingPanel.SetActive(false);
            // storePanel.SetActive(false);
            // minerPanel.SetActive(false);
            // headerPanel.SetActive(true);
            // warningPanel.SetActive(false);
            // marshmelloStorePanel.SetActive(false);
            // loadingPanel.SetActive(false);
//            registerPanel.SetActive(false);
        }

        // Update is called once per frame
        void Update()
        {
            // if (Input.GetKeyDown(KeyCode.Escape))
            // {
            //     OnBackClick();
            // }
        }
        
        public void OnStartClick()
        {
            startButton.transform.DOComplete();
            startButton.transform.DOPunchScale(new Vector3(-0.1f, -0.1f, -0.1f), transactionDuration, 0, 1);
            mainMenuPanel.SetActive(true);
            headerPanel.SetActive(true);
            mainMenuPanel.transform.DOComplete();
            mainMenuPanel.transform.DOMoveY(0, 1,snapping).OnComplete(() =>
            {
                firstPanel.SetActive(false);    
            });
            headerPanel.transform.DOComplete();
            headerPanel.transform.DOMoveY(0, 1);
            
            _scene = 1;
        }

        public void OnSettingClick()
        {
            settingPanel.SetActive(true);
            settingPanel.transform.DOComplete();
            settingPanel.transform.DOMoveY(0, transactionDuration * Time.deltaTime, snapping).OnComplete(() =>
            {
                firstPanel.SetActive(false);
                headerPanel.SetActive(false);
            });
            _scene = 2;
        }

        public void AcceptExit()
        {
            Application.Quit();
            print("Quited");
            _scene = 0;
        }
        
        public void OnExitClick()
        {
            /* var exit = new Button.ButtonClickedEvent();
             exit.AddListener(()=> AcceptExit());
             acceptWarningPanel.onClick = exit;
             warningPanel.SetActive(true);
             warningText.text = "میخوای بری بیرون؟";
             acceptWarningPanelText.text = "خروج";
             warningPanel.transform.DOComplete();
             warningPanel.transform.DOMoveY(0, transactionDuration * Time.deltaTime, snapping);*/
            PopUpManager.Instance.AcceptExit();
        }
        
        public void OnPlayClick()
        {
            loadingPanel.SetActive(true);
            loadingPanel.transform.DOComplete();
            loadingPanelText.text = Strings.MatchMaking;
            loadingPanel.transform.DOMoveY(0, .5f);
            //loadingPanel.transform.DOMoveY(0, 1).OnComplete(() =>
            //{
                ServerManager.Instance.GetCards((cardsInfo) =>
                {
                    SafeBox.Instance.cardsInfo.Clear();
                    SafeBox.Instance.cardsInfo.AddRange(cardsInfo);
                    ServerManager.Instance.Login((userInfo) =>
                    {
                        SafeBox.Instance.roomName = DateTime.Now+ SystemInfo.deviceUniqueIdentifier;
                        print(SafeBox.Instance.roomName);
                        // SafeBox.Instance.roomName = SystemInfo.deviceUniqueIdentifier + SafeBox.Instance.userInfo.xp +
                        //                             SafeBox.Instance.userInfo.coin;
                        //SafeBox.Instance.userInfo.cards.Clear();
                        //SafeBox.Instance.userInfo.deck_cards.Clear();
                        SafeBox.Instance.userInfo = userInfo;
                        
                        ServerManager.Instance.MatchStart(SafeBox.Instance.roomName , SafeBox.Instance.userInfo.id, 0,
                            SafeBox.Instance.userInfo.name, "", DateTime.Now, SafeBox.Instance.userInfo.deck_cards, null,
                            () =>
                            {
                                print("Match Start : ");
                                SettingPanel.Instance.SetGameplayMusic();
                                Time.timeScale = 1;
                                SafeBox.Instance.gameState = GameState.InGame;
                                //newLoadingPanel.FakeLoading(2);
                                LoadingPanel.Instance.ChangeScene(2);
                                //SceneManager.LoadScene(2);
                            }, (statusCode,response) =>
                            {   
                                //PopUpManager.Instance.MatchNotFound();
                                NoticePopUpPanel.Instance.NewNotice().WithContent(Strings.MatchMakingFailed).WithAcceptButton(OnPlayClick)
                                    .WithRejectButton(() =>
                                    {
                                        loadingPanel.transform.DOComplete();
                                        loadingPanel.transform.DOMoveY(-3000, .5f);
                                    });
                                print("Match Start : " + statusCode);
                                print(response);
                            });

                    }, (() =>{} ), (statusCode) =>
                    {
                        NoticePopUpPanel.Instance.LoginFailed();
                        //PopUpManager.Instance.LoginFailed();
                        print("Get User: " + statusCode);
                        //PopUpManager.Instance.MatchNotFound();
                    });
                }, (statusCode) =>
                {
                    NoticePopUpPanel.Instance.NewNotice().WithContent(Strings.MatchMakingFailed).WithAcceptButton(OnPlayClick)
                        .WithRejectButton(() =>
                        {
                            loadingPanel.transform.DOComplete();
                            loadingPanel.transform.DOMoveY(-3000, .5f);
                        });
                    print("Get Cards : " + statusCode );
                    // PopUpManager.Instance.MatchNotFound();
                });
                
                //await Task.Delay(5000);
                
            //});

        }
        

        public void OnEquipClick()
        {
            equipPanel.SetActive(true);
            equipPanel.transform.DOComplete();
            equipPanel.transform.DOMoveX(0, transactionDuration* Time.deltaTime, snapping).OnComplete(() =>
            {
                mainMenuPanel.SetActive(false);
                headerPanel.SetActive(false);    
            });
            _scene = 3;
        }
        
        public void OnUpgradeClick()
        {
            headerPanel.SetActive(true);
            upgradePanel.SetActive(true);
            ItemsToUpgradeSliderSelect();
            // upgradePanel.transform.DOComplete();
            // upgradePanel.transform.DOMoveX(0, transactionDuration* Time.deltaTime, snapping).OnComplete(() =>
            // {
            //     mainMenuPanel.SetActive(false);
            // });
            _scene = 4;
        }

        public void OnStoreClick()
        {
            
            storePanel.SetActive(true);
            warningPanel.SetActive(false);
            storePanel.transform.DOComplete();
            storePanel.transform.DOMoveX(0, transactionDuration* Time.deltaTime, snapping).OnComplete(() =>
            {
                mainMenuPanel.SetActive(false);
                headerPanel.SetActive(false);
                upgradePanel.SetActive(false);
                marshmelloStorePanel.SetActive(false);
            });
            _scene = 5;
        }

        public void OnMinerClick()
        {
            minerPanel.SetActive(true);
            headerPanel.SetActive(false);
            mainMenuPanel.SetActive(false);
            _scene = 6;
        }
        
        public void DenyWarningPanel()
        {
            warningPanel.transform.DOComplete();
            warningPanelRect.DOAnchorPosY(-1000, transactionDuration* Time.deltaTime, snapping).OnComplete(() =>
            {
                warningPanel.SetActive(false);
            });

        }

        public void OnMarshStoreClick()
        {
            marshmelloStorePanel.SetActive(true);
            marshmelloStorePanel.transform.DOComplete();
            marshmelloStorePanel.transform.DOMoveX(0, transactionDuration* Time.deltaTime, snapping).OnComplete(() =>
            {
                mainMenuPanel.SetActive(false);
                headerPanel.SetActive(false);
                upgradePanel.SetActive(false);
            });
            _scene = 7;
        }
        
        public void ItemsToUpgradeSliderSelect()
        {
            itemsToUpgradePanelSlider.SetActive(true);
            itemsToBuyPanelSlider.SetActive(false);
            itemsToUpgradeSliderButton.interactable = false;
            itemsToBuySliderButton.interactable = true;
        }

        public void ItemsToBuySliderSelect()
        {
            itemsToUpgradePanelSlider.SetActive(false);
            itemsToBuyPanelSlider.SetActive(true);
            itemsToUpgradeSliderButton.interactable = true;
            itemsToBuySliderButton.interactable = false;
        }

        
        
        public void OnBackClick()
        {
            switch (_scene)
            {
                case 0 :
                    //acceptExitPanel.SetActive(true);
                    //OnExitClick();
                    PopUpManager.Instance.AcceptExit();
                    break;
                case 1 :
                    firstPanel.SetActive(true);
                    mainMenuPanelRect.transform.DOComplete();
                    mainMenuPanelRect.DOAnchorPosY(-1080, transactionDuration* Time.deltaTime, snapping).OnComplete(()=>//DOMoveY(-1080, 1).OnComplete(() =>
                    {
                        mainMenuPanel.SetActive(false);
                    });
                    headerPanelRect.transform.DOComplete();
                    headerPanelRect.DOAnchorPosY(-1080,transactionDuration* Time.deltaTime,snapping).OnComplete(() =>
                    {
                        headerPanel.SetActive(false);
                    });
                    _scene = 0;
                    break;
                case 2:
                    
                    mainMenuPanel.SetActive(true);
                    headerPanel.SetActive(true);
                    settingPanelRect.transform.DOComplete();
                    settingPanelRect.DOAnchorPosY(1080, transactionDuration* Time.deltaTime, snapping).OnComplete(() =>
                    {
                        settingPanel.SetActive(false);
                    });
                    _scene = 0;
                    break;
                case 3:
                    
                    mainMenuPanel.SetActive(true);
                    headerPanel.SetActive(true);
                    equipPanelRect.transform.DOComplete();
                    equipPanelRect.DOAnchorPosX(1920, transactionDuration* Time.deltaTime, snapping).OnComplete(() =>
                    {
                        equipPanel.SetActive(false);
                    });
                    _scene = 0;
                    break;
                case 4:
                    
                    mainMenuPanel.SetActive(true);
                    headerPanel.SetActive(true);
                    upgradePanelRect.transform.DOComplete();
                    upgradePanelRect.DOAnchorPosX(-1920, transactionDuration* Time.deltaTime, snapping).OnComplete(() =>
                    {
                        upgradePanel.SetActive(false);
                    });
                    _scene = 0;
                    break;
                case 5:
                    
                    mainMenuPanel.SetActive(true);
                    headerPanel.SetActive(true);
                    storePanelRect.transform.DOComplete();
                    storePanelRect.DOAnchorPosX(1920, transactionDuration* Time.deltaTime, snapping).OnComplete(() =>
                    {
                        storePanel.SetActive(false);
                    });
                    _scene = 0;
                    break;
                case 6 :
                    minerPanel.SetActive(false);
                    mainMenuPanel.SetActive(true);
                    headerPanel.SetActive(true);
                    _scene = 0;    
                    break;
                case 7 :
                    
                    mainMenuPanel.SetActive(true);
                    headerPanel.SetActive(true);
                    marshmelloStorePanelRect.transform.DOComplete();
                    marshmelloStorePanelRect.DOAnchorPosX(1920, transactionDuration* Time.deltaTime, snapping).OnComplete(() =>
                    {
                        marshmelloStorePanel.SetActive(false);
                    });
                    _scene = 0;
                    break;
            }
        }

        // public void SetCurrencies()
        // {
        //     coinText.text = CoinManager.Instance.GetCoin().ToString();
        //     gemText.text = GemManager.Instance.GetGem().ToString();
        //     coinShopCoinTxt.text = CoinManager.Instance.GetCoin().ToString();
        // }

        

        public void SetXp()
        {
            /*levelSlider.maxValue = XpManager.Instance.GetNextLevelXp();
            levelSlider.value = XpManager.Instance.GetXp();
            levelText.text = XpManager.Instance.GetLevel().ToString();*/
        }


        public void AcceptBuyMarsh(int marshmello)
        {
            warningText.text = "میخوای خریدت رو کامل کنی؟";
            acceptWarningPanelText.text = "خرید" + marshmello + "مارشملو";
        }

        // public void AcceptBuyingItem(Transform cardTransform , int coin )
        // {
        //     var buyItem = new Button.ButtonClickedEvent();
        //     buyItem.AddListener(() => CoinManager.Instance.BuyItem(coin));
        //     acceptWarningPanel.onClick = buyItem;
        //     warningText.text = "میخوای خریدت رو تکمیل کنی؟";
        //     acceptWarningPanelText.text = "تایید";
        // }
        

        public void NotEnoughCoin(int gemToUse , bool haveEnoughGem)
        {
            warningText.text = "سکه کافی نداری :( ";
            if (haveEnoughGem)
            {
                var useGem = new Button.ButtonClickedEvent();
              //  useGem.AddListener(()=>CoinManager.Instance.AcceptToUseGem(gemToUse));
                acceptWarningPanel.onClick = useGem;
                acceptWarningPanelText.text = "استفاده از" + gemToUse +"الماس";
            }
            else
            {
                NotEnoughGem();
            }
            warningPanel.SetActive(true);
        }

        public void NotEnoughGem()
        {
            var goToStore = new Button.ButtonClickedEvent();
            goToStore.AddListener(()=>OnStoreClick());
            acceptWarningPanel.onClick = goToStore;
            acceptWarningPanelText.text = "بریم الماس بخرم";
            warningText.text = "الماس کافی نداری";
        }
        
        // public void AcceptToBuyCoin(Transform transform ,int gemToUse , int coin)
        // {
        //     
        //     var buyCoin = new  Button.ButtonClickedEvent();
        //     buyCoin.AddListener(() => CoinManager.Instance.BuyCoinWithGem(coin,gemToUse));
        //     warningPanel.SetActive(true);
        //     warningText.text = "میخوای " + coin + "سکه بخری؟";
        //     acceptWarningPanelText.text = "استفاده از" + gemToUse + "الماس";
        //     acceptWarningPanel.onClick = buyCoin;
        // }

        public void AcceptBuyingGem(int gem)
        {
            
        }

        

        private IEnumerator SetActivePanel(GameObject panel , Text panelText , Image panelImage)
        {
            yield return new WaitForSecondsRealtime(2);
            panel.SetActive(false);
            panelImage.DOFade(255f, 0.1f);
            panelText.DOFade(255f, 0.1f);
        }

        public void SetItemShopErrorPanel(int errorCode)
        {
            errorPanel.SetActive(true);
            errorPanelRect.transform.DOComplete();
            errorPanelRect.DOMoveY(0, transactionDuration, true);
            switch (errorCode)
            {
                case 401:
                    errorPanelText.text = "فروشگاه قابل دسترسی نیست";
                    break;
                default:
                    errorPanelText.text = "اینترنت قطع شد :(";
                    break;
            }
            errorPanelRect.transform.DOComplete();
            errorPanelRect.DOMoveY(-1920, transactionDuration, false).SetDelay(2);
        }
        
        
        /* public void BuyCoinFromStore(int coin , bool haveEnoughGem )
         {
             
             if (haveEnoughGem)
             {
                 var acceptButton = new Button.ButtonClickedEvent();
                 acceptButton.AddListener(()=>CoinManager.Instance.AcceptToUseGem());
                 acceptWarningPanel.onClick = acceptButton;
                 warningText.text = "مطمعنی میخوای " + coin + "  " + "مارشملو بخری؟ ";
                 acceptWarningPanelText.text = "خرید مارشملو";
             }
             else
             {
                 warningText.text = "الماس کافی نداری";
                 NotEnoughGem();
             }
             warningPanel.SetActive(true);
         }*/

        public void SetItemShopPurchaseErrorPanel(string response)
        {
            errorPanel.SetActive(true);
            if (response == "Not Enough Coin!")
            {
                errorPanelText.text = "چرخ دنده‌ی کافی ندارید ";
            }
            else if (response == "Sku Not Found!" || response =="Forbidden Token" || response == "Unauthorized")
            {
                errorPanelText.text = "یه چیزی درست نیست؛ بعدا دوباره متحان کن";
            }
            else if (response == default)
            {
                errorPanelText.text = "نتت قطع شد که :(";
            }
        }

        public void DisplayRegisterPanel()
        {
            registerPanel.SetActive(true);
            registerPanelRect.transform.DOComplete();
            registerPanel.transform.DOMoveY(0, 1);
        }
    }
    
}