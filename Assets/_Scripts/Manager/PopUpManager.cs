﻿using _Scripts.Controller.Card;
using _Scripts.Controller.Card.Deck;
using _Scripts.Controller.Spells;
using _Scripts.Manager.Currencies;
using _Scripts.Manager.Preload;
using _Scripts.Manager.Stores;
using _Scripts.Model;
using _Scripts.UI;
using _Scripts.UI.NoticePopUp;
using DG.Tweening;
using Manager;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using Util;

namespace _Scripts.Manager
{
    public class PopUpManager : MonoBehaviourSingleton<PopUpManager>
    {
        //SerializeFields
        [SerializeField] public RectTransform popUp;
        [SerializeField] public Text popUpText;
        [SerializeField] public Text acceptButtonText;
        [SerializeField] public Text denyButtonText;
        [SerializeField] public Button acceptButton;
        [SerializeField] public Button denyButton;

        [SerializeField] private Transform buttonsSeat;

        //Variables
        public Vector3 position;
        public float duration;
        public bool snapping;

        

        private void Start()
        {
            popUp.localPosition = position;
            popUp.gameObject.SetActive(false);
        }

        public void SpawnPopUp(int buttonNumbers)
        {

            popUp.gameObject.SetActive(true);
            if (buttonNumbers == 1)
            {
                acceptButton.gameObject.SetActive(true);
                denyButton.gameObject.SetActive(false);
            }
            else if (buttonNumbers == 2)
            {
                acceptButton.gameObject.SetActive(true);
                denyButton.gameObject.SetActive(true);
            }

            popUp.transform.DOComplete();
            popUp.DOMoveY(0, duration, snapping);
        }

        public void ClosePopUp()
        {
            popUp.localPosition = position;
            popUp.gameObject.SetActive(false);
        }

        public void LoginFailed()
        {
            NoticePopUpPanel.Instance.NewNotice().WithContent("یه اتفاقی افتاد").WithAcceptButton(() =>
            {
                PreloadManager.Instance.CallLogin();
            });

            // SpawnPopUp(1);
            // popUpText.text = "یه اتفاقی افتاد";
            // acceptButtonText.text = "تلاش مجدد ";
            //
            // var loginAgain = new Button.ButtonClickedEvent();
            // loginAgain.AddListener(() =>
            // {
            //     PreloadManager.Instance.CallLogin();
            //     ClosePopUp();
            // });
            //
            // acceptButton.onClick = loginAgain;
        }

        public void UpgradeSoldier(UserCard userCard)
        {
            var cardInfo = CardsManager.Instance.GETSoldierInfo(userCard.card_id);
            var level = userCard.level;
            if (CoinManager.Instance.GetCoin() >=cardInfo.coin_price[level] &&
                ShardManager.Instance.CheckShard(userCard.card_id,cardInfo.required_shard[level]))
            {
                SpawnPopUp(2);
                popUpText.text = "میخوای خریدت رو کامل کنی؟";
                acceptButtonText.text = "آره";
                denyButtonText.text = "بیخیال";

                var buy = new Button.ButtonClickedEvent();
                buy.AddListener(() =>
                {
                    ClosePopUp();
                    DeckPanel.Instance.Upgrade();
                });
                acceptButton.onClick = buy;

                var cancell = new Button.ButtonClickedEvent();
                cancell.AddListener(() => { ClosePopUp(); });
                denyButton.onClick = cancell;
            }
            else
            {
                SpawnPopUp(1);
                if (CoinManager.Instance.GetCoin() < cardInfo.coin_price[level])
                {
                    popUpText.text = "یه نگا به پولات بکن ";    
                }
                else
                {
                    popUpText.text = "کارت ارتقا به اندازه کافی نداری";
                }
                acceptButtonText.text = "باوش";
                var closePopUp = new Button.ButtonClickedEvent();
                closePopUp.AddListener(() =>
                {
                    ClosePopUp();
                });
                acceptButton.onClick = closePopUp;
            }
        }

        public void AcceptBuyingItem(int price, uint id, SoldierUpgradeCard soldierUpgradeCard)
        {
            var gemPrice = CoinManager.Instance.ConvertCoinToGem(price);
            if (CoinManager.Instance.GetCoin() >= price)
            {
                //PanelManager.Instance.AcceptBuyingItem(transform, price);
                ServerManager.Instance.ItemShopPurchase(id, (userInfo) =>
                {
                    SafeBox.Instance.userInfo = userInfo;
                    soldierUpgradeCard.level++;
                    if (soldierUpgradeCard.CheckIfItIsMax())
                    {
                        return;
                    }
                    soldierUpgradeCard.UpdateUi();
                    //CoinManager.Instance.BuyItem(price);
                    CurrencyUIManager.Instance.UpdateUI();
                    //XpManager.Instance.EarnXp(upgradeCard.xpReward[upgradeCard.level]);
                    CurrencyUIManager.Instance.UpdateUI();
                    if (!soldierUpgradeCard.isPurchased)
                    {
                        ItemStoreManager.Instance.CreatUpgradeCard(soldierUpgradeCard);
                        Destroy(soldierUpgradeCard.gameObject);                        
                        //transform.parent = PanelManager.Instance.upgradeItem;
                        //upgradeCard.isPurchased = true;
                    }
                    ServerManager.Instance.Login((userInfo) =>
                    {
                        SafeBox.Instance.userInfo = userInfo;
                        CardsManager.Instance.SetOwnedCardInfo();
                        CardsManager.Instance.SetDeckInfo();
                    },()=>{}, (response) =>
                    {
                        LoginFailed();
                    });
                    
                }, (response) =>
                {
                    SpawnPopUp(1);
                    if (response == "Not Enough Coin!")
                    {
                        popUpText.text = "چرخ دنده‌ی کافی ندارید ";
                    }
                    else if (response == "Sku Not Found!" || response =="Forbidden Token" || response == "Unauthorized")
                    {
                        popUpText.text = "یه چیزی درست نیست؛ بعدا دوباره متحان کن";
                    }
                    else if (response == default)
                    {
                        popUpText.text = "نتت قطع شد که :(";
                    }

                    //Set OK Button
                    var ok = new  Button.ButtonClickedEvent();
                    ok.AddListener(() =>
                    {
                        popUp.localPosition = position;
                        popUp.gameObject.SetActive(false);
                    });
                    acceptButton.onClick = ok;
                });
            }
        }
        
        public void PurchaseCurrency( ProductCard card)//ShopItems.PricesType price_type,int price , int _amount , string _sku , ProductCard card)
        {
            SpawnPopUp(2);
            popUpText.text = "میخوای خریدت رو کامل کنی؟";
            acceptButtonText.text = "آره";
            denyButtonText.text = "بیخیال";
            
            var buy = new Button.ButtonClickedEvent();
            buy.AddListener(() =>
            {
                ClosePopUp();
                //AcceptPurchaseCurrency(price_type,price,_amount,_sku);
                card.Buy();
            });
            acceptButton.onClick = buy;
            
            var cancell = new Button.ButtonClickedEvent();
            cancell.AddListener(() =>
            {
                ClosePopUp();
            });
            denyButton.onClick = cancell;
        }

        public void BuySpell(SpellShopCard spellShopCard)
        {
            SpawnPopUp(2);
            popUpText.text = "میخوای این اسپل رو بخری؟";
            acceptButtonText.text = "آره";
            denyButtonText.text = "نه";
            
            var buy = new Button.ButtonClickedEvent();
            buy.AddListener(spellShopCard.Buy);
            acceptButton.onClick = buy;
            
            var cancel = new Button.ButtonClickedEvent();
            cancel.AddListener(ClosePopUp);
            denyButton.onClick = cancel;
        }

        public void BuySoldier(SoldierShopCard soldierShopCard)
        {
            SpawnPopUp(2);
            popUpText.text = "میخوای این سرباز رو بخری؟";
            acceptButtonText.text = "آره";
            denyButtonText.text = "بیخیال";
            
            var buy = new Button.ButtonClickedEvent();
            buy.AddListener(soldierShopCard.Buy);
            acceptButton.onClick = buy;
            
            var cancel = new Button.ButtonClickedEvent();
            cancel.AddListener(ClosePopUp);
            denyButton.onClick = cancel;
        }
        
        public void UpgradeCard(SoldierShopCard soldierShopCard)
        {
            SpawnPopUp(2);
            popUpText.text = "میخوای این سرباز رو بخری؟";
            acceptButtonText.text = "آره";
            denyButtonText.text = "بیخیال";
            
            var buy = new Button.ButtonClickedEvent();
            buy.AddListener(soldierShopCard.Buy);
            acceptButton.onClick = buy;
            
            var cancel = new Button.ButtonClickedEvent();
            cancel.AddListener(ClosePopUp);
            denyButton.onClick = cancel;
        }

        public void NeedMoreMoney()
        {
              SpawnPopUp(2);
              popUpText.text = "پول کافی نداری. بریم یه سر فروشگاه؟";
              acceptButtonText.text = "بریم";
              denyButtonText.text = "بیخیال";
              
              var goToShop = new Button.ButtonClickedEvent();
              goToShop.AddListener(PanelManager.Instance.OnMarshStoreClick);
              acceptButton.onClick = goToShop;
              
              var cancel = new Button.ButtonClickedEvent();
              cancel.AddListener(ClosePopUp);
              denyButton.onClick = cancel;
        }

        /*public void AcceptPurchaseCurrency(ShopItems.PricesType price_type,int price , int _amount , string _sku)
        {
            if (price_type == ShopItems.PricesType.PriceGem)
            {
                print("On Mouse Down Check");
                if (price <= GemManager.Instance.GetGem())
                {
                    print("Has The Price");
                    ServerManager.Instance.ShopPurchase(_sku,"","", () =>
                    {
                        GemManager.Instance.SpendGem(price);
                        CoinManager.Instance.EarnCoin(_amount);
                        print("Coin Earned");
                    }, (statusCode) =>
                    {
                        print(statusCode);
                        print("Doesnt Have The Price");
                    });
                }
                else
                {
                    PanelManager.Instance.NotEnoughGem();
                }    
            }
            else if (price_type == ShopItems.PricesType.PriceMoney)
            {
                print("Gem Button");
                BazaarBilling.Purchase(sku, (purchaseResult) =>
                {
                    if (purchaseResult.Successful)
                    {
                        Purchase purchase = purchaseResult.Body;
                        
                        ServerManager.Instance.ShopPurchase(sku,purchase.PurchaseToken,"", () =>
                        {
                            GemManager.Instance.EarnGem(_amount);
                            BazaarBilling.Consume(sku , (consumeResult) =>
                            {
                            
                            });    
                        }, (response) =>
                        {
                            //Call PopUp
                        });
                        
                    }
                });
            }
        }*/

        public void AcceptExit()
        {
            SpawnPopUp(2);
            popUpText.text = "میخوای بری ؟؟؟ ";
            acceptButtonText.text = "آره دیگه";
            denyButtonText.text = "نه بابا";
            var acceptExit = new Button.ButtonClickedEvent();
            acceptExit.AddListener(() =>
            {
                ClosePopUp();
                Application.Quit();
            });
            acceptButton.onClick = acceptExit;
            
            var denyExit = new Button.ButtonClickedEvent();
            denyExit.AddListener(()=>
            {
                ClosePopUp();
            });
            denyButton.onClick = denyExit;
        }

        public void MatchNotFound()
        {
            SpawnPopUp(2);
            popUpText.text = "خطایی رخ داده";
            acceptButtonText.text = "تلاش دوباره";
            denyButtonText.text = "ای بابا بیخیال";
            var tryAgain = new Button.ButtonClickedEvent();
            tryAgain.AddListener(() =>
            {
                PanelManager.Instance.OnPlayClick();
                ClosePopUp();
            });
            acceptButton.onClick = tryAgain;
            
            var bacKToMenu = new Button.ButtonClickedEvent();
            bacKToMenu.AddListener(() =>
            {
                SceneManager.LoadScene(1);
                ClosePopUp();
            });
        }

        public void NeedMoreSoldiersToStart()
        {
            SpawnPopUp(1);
            popUpText.text = "برای شروع بازی باید حداقل دو سرباز با خودت ببری";
            acceptButtonText.text = "باشه";
            var closePopUp = new Button.ButtonClickedEvent();
            closePopUp.AddListener(ClosePopUp);
            acceptButton.onClick = closePopUp;
        }

        public void EmptyDeck()
        {
            SpawnPopUp(1);
            popUpText.text = "دک نمیتونه خالی باشه";
            acceptButtonText.text = "اوکی";
            var closePopUp = new Button.ButtonClickedEvent();
            closePopUp.AddListener(() =>
            {
                ClosePopUp();
            });
            acceptButton.onClick = closePopUp; 
        }
    }
}