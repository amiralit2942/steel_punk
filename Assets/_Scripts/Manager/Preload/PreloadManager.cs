﻿using System;
using System.Collections;
using System.Collections.Generic;
using _Scripts.Model;
using _Scripts.UI;
using _Scripts.UI.NoticePopUp;
using _Scripts.Util;
using DG.Tweening;
using Newtonsoft.Json;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using Util;
using View;

namespace _Scripts.Manager.Preload
{
    public class PreloadManager : MonoBehaviourSingleton<PreloadManager>
    {
        [SerializeField] private RegisterPanel registerPanel;
        [SerializeField] private Image luluGamesLogo;
        [SerializeField] private GameObject loadingPanel;
        [SerializeField] private Text loadingText;
        [SerializeField] private LoadingPanel newLoadingPanel;

        IEnumerator Start()
        {
            registerPanel.gameObject.SetActive(false);
            loadingPanel.gameObject.SetActive(false);
            luluGamesLogo.DOFade(255, 3);
            yield return new WaitForSeconds(2);
            luluGamesLogo.DOFade(0, 3);
            loadingPanel.SetActive(true);
            if (!PlayerPrefs.HasKey("LanguageIsSelected"))
            {
                PlayerPrefs.SetInt("LanguageIsSelected", 1);
                NoticePopUpPanel.Instance.NewNotice().WithContent("Select Language").ActiveButtonOneWithContent(
                    "English",
                    () =>
                    {
                        Localization.ChangeLanguage(LocalizationLanguage.English);
                        LoadingPanel.Instance.FakeLoading(0);
                    }).ActiveButtonTwoWithContent("فارسی",
                    () =>
                    {
                        Localization.ChangeLanguage(LocalizationLanguage.Persian);
                        LoadingPanel.Instance.FakeLoading(0);
                    });
            }
            else
            {
                Localization.SetLanguage();
                Login(Register, loadingText, newLoadingPanel);
            }
        }

        private void Register()
        {
            registerPanel.gameObject.SetActive(true);
        }

        public static void GetBaseGunInfo(Text loadingText, LoadingPanel loadingPanel)
        {
            ServerManager.Instance.GetBaseGunsInfo((response) =>
            {
                SafeBox.Instance.baseGunsInfo = response;
                loadingText.text = "در حال دریافت اسلحه ها";
                //loadingPanel.FakeLoading(1);
                LoadingPanel.Instance.FakeLoading(1);
            }, (statusCode) =>
            {
                //PopUpManager.Instance.LoginFailed();
                NoticePopUpPanel.Instance.LoginFailed();
                print("get base Guns info : " + statusCode);
            });
        }

        private static void GetChestsInfo(Text loadingText, LoadingPanel loadingPanel)
        {
            loadingText.text = "در حال دریافت صندوق ها";
            ServerManager.Instance.GetChestInfo((chests) =>
            {
                SafeBox.Instance.chestInfo = chests;
                GetBaseGunInfo(loadingText, loadingPanel);
            }, (response) =>
            {
                //PopUpManager.Instance.LoginFailed();
                NoticePopUpPanel.Instance.LoginFailed();
                print("Get Chests : " + response);
            });
        }

        private static void GetUpgradeCards(Text loadingText, LoadingPanel loadingPanel)
        {
            // ServerManager.Instance.GetUpgradeCards((upgradeCards) =>
            // {
            //     //SafeBox.Instance.UpgradeCards = upgradeCards;
            //     
            // }, (response) =>
            // {
            //     PopUpManager.Instance.LoginFailed();
            //     print(response);
            // });

            GetChestsInfo(loadingText, loadingPanel);
        }


        private static void GetUser(Action onRegister, Text loadingText, LoadingPanel loadingPanel)
        {
            loadingText.text = global::_Scripts.Util.Strings.GetUserData;
            ServerManager.Instance.Login((userInfo) =>
            {
                SafeBox.Instance.userInfo = userInfo;
                SafeBox.Instance.gameState = GameState.InMenu;
                loadingPanel.ChangeScene(1);
            }, onRegister, (statusCode) =>
            {
                //PopUpManager.Instance.LoginFailed();
                NoticePopUpPanel.Instance.LoginFailed();
                print("Get User: " + statusCode);
            });
        }

        private static void GetCardsInfo(Action onRegister, Text loadingText, LoadingPanel loadingPanel)
        {
            //Here We First Get Soldiers And Then Spells Infos
            loadingText.text = global::_Scripts.Util.Strings.GetCardData;
            ServerManager.Instance.GetCards((cardsInfo) =>
            {
                SafeBox.Instance.cardsInfo.Clear();
                SafeBox.Instance.cardsInfo.AddRange(cardsInfo);
                GetUser(onRegister, loadingText, loadingPanel);
                ServerManager.Instance.GetSpellsInfo((spellsInfo) =>
                {
                    SafeBox.Instance.spellsInfo = spellsInfo;
                    GetUser(onRegister, loadingText, loadingPanel);
                }, (statusCode) =>
                {
                    PopUpManager.Instance.LoginFailed();
                    print("Get Spells Info : " + statusCode);
                });
            }, (statusCode) =>
            {
                //PopUpManager.Instance.LoginFailed();
                NoticePopUpPanel.Instance.LoginFailed();
                print("Get Cards : " + statusCode);
            });
        }

        private static void GetShop(Action onRegister, Text loadingText, LoadingPanel loadingPanel)
        {
            loadingText.text = global::_Scripts.Util.Strings.GetShopData;
            ServerManager.Instance.GetShopItems((shops) =>
            {
                SafeBox.Instance.shopItems.Clear();
                SafeBox.Instance.shopItems.AddRange(shops);
                GetCardsInfo(onRegister, loadingText, loadingPanel);
            }, (statusCode) =>
            {
                //PopUpManager.Instance.LoginFailed();
                NoticePopUpPanel.Instance.LoginFailed();
                print("Shop Items " + statusCode);
                //Show popup});
            });
        }

        private static void GetConfig(Action onRegister, Text loadingText, LoadingPanel loadingPanel)
        {
            loadingText.text = global::_Scripts.Util.Strings.GetConfigData;
            ServerManager.Instance.GetConfig((configs) =>
            {
                print("Got Config");
                SafeBox.Instance.configs = configs;
                SafeBox.Instance.manaIncomeRate = JsonConvert.DeserializeObject<List<float>>
                    (SafeBox.Instance.configs.Find(x => x.key == Strings.ManaIncomeRate).value);
                SafeBox.Instance.manMax = JsonConvert.DeserializeObject<List<float>>
                    (SafeBox.Instance.configs.Find(x => x.key == Strings.ManaMax).value);
                GetShop(onRegister, loadingText, loadingPanel);
            }, () =>
            {
                print("Got Config Rid");
                //PopUpManager.Instance.LoginFailed();
                NoticePopUpPanel.Instance.LoginFailed();
            });
        }

        public static void Login(Action onRegister, Text loadingText, LoadingPanel loadingPanel)
        {
            loadingText.text = global::_Scripts.Util.Strings.GetConfigData;
            GetConfig(onRegister, loadingText, loadingPanel);
        }

        public void CallLogin()
        {
            Login(Register, loadingText, newLoadingPanel);
            registerPanel.gameObject.SetActive(false);
        }
    }
}