﻿using _Scripts.Util;
using DG.Tweening;
using UnityEngine;
using UnityEngine.UI;
using UPersian.Components;
using Util;

namespace _Scripts.Manager.Preload
{
    public class PreloadUi : MonoBehaviourSingleton<PreloadUi>
    {
        [SerializeField] private GameObject errorPanel;
        [SerializeField] private RtlText errorPanelText;
        [SerializeField] private RectTransform errorPanelRect;
        [SerializeField] private Button tryAgain;
        public void SetItemShopErrorPanel(int errorCode)
        {
            errorPanel.SetActive(true);
            errorPanelRect.transform.DOComplete();
            errorPanelRect.DOMoveY(0, 0, true);
            switch (errorCode)
            {
                case 401:
                    errorPanelText.text = Strings.ShopNotAvailable;
                    break;
                default:
                    errorPanelText.text = Strings.ConnectionError;
                    print(errorCode);
                    break;
            }
        }

        public void OnTryAgainClick()
        {
            PreloadManager.Instance.CallLogin();
            errorPanelRect.transform.DOComplete();
            errorPanelRect.DOMoveY(-1920, 1, false).SetDelay(2).OnComplete(() =>
            {
                errorPanel.SetActive(false);
            });
        }
        
        public void SetRegisterErrorPanel(int errorCode)
        {
            errorPanel.SetActive(true);
            errorPanel.transform.DOMove(new Vector3(0, 0, 0), 1);
            
            switch (errorCode)
            {
                case 200:
                    errorPanelText.text = Strings.RegisterSucceed;
                    break;
                case 401:
                    errorPanelText.text = Strings.RegisterFailed;
                    break;
                case 403:
                    errorPanelText.text = "این اسم قبلا انتخاب شده";
                    break;
                default:
                    print("Register : " + errorCode);
                    errorPanelText.text = Strings.ConnectionError;
                    break;
            }
            
            //errorPanelText.DOFade(0, 4);
            //errorPanelImage.DOFade(0, 4);
            //StartCoroutine(SetActivePanel(errorPanel, errorPanelText, errorPanelImage));

        }

        public void SetLoginErrorPanel(long statusCode)
        {
            errorPanel.SetActive(true);
            errorPanel.transform.DOMove(new Vector3(0, 0, 0), 1);

            switch (statusCode)
            {
                case 200:
                    errorPanelText.text = "لاگین شدی D: ";
                    print("Login Succeed");
                    break;
                default:
                    errorPanelText.text = "یه مشکلی پیش اومده";
                    print("Login :" + statusCode);
                    break;
            }
        }
    }
}
