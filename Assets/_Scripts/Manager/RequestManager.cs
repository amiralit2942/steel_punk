﻿using System;
using System.Collections;
using System.Security.Cryptography;
using System.Text;
using UnityEngine;
using UnityEngine.Networking;
using Util;

namespace _Scripts.Manager
{
    public class RequestManager : MonoBehaviourSingleton<RequestManager>
    {
        string authenticate(string username, string password)
        {
            string auth = username + ":" + password;
            // print("User: " + SystemInfo.deviceUniqueIdentifier);
             print("Pass: " + password);
            auth = System.Convert.ToBase64String(System.Text.Encoding.GetEncoding("ISO-8859-1").GetBytes(auth));
            auth = "Basic " + auth;
            return auth;
        }

        private static RequestManager _instance;
        private string HashSalt => "CasualCampHashSystem";

        static string ComputeSha256Hash(string rawData)
        {

            // Create a SHA256   
            using (SHA256 sha256Hash = SHA256.Create())
            {
                // ComputeHash - returns byte array  
                byte[] bytes = sha256Hash.ComputeHash(Encoding.UTF8.GetBytes(rawData));

                // Convert byte array to a string   
                StringBuilder builder = new StringBuilder();
                for (int i = 0; i < bytes.Length; i++)
                {
                    builder.Append(bytes[i].ToString("x2"));
                }
//            print("builder hash: " + builder);
                return builder.ToString();
            }
        } 

        public void Send(RequestMethod method, string uri, Action<long, string> response, string userName = "", string sendData = "")
        {
            string authorization = authenticate(userName, ComputeSha256Hash(userName + sendData + HashSalt));
            print($"sending request with auth {authorization}");
            switch (method)
            {
                case RequestMethod.GET:
                    StartCoroutine(GetRequest(uri, response, authorization));
                    break;
                case RequestMethod.POST:
                    StartCoroutine(PostRequest(uri, sendData, authorization, response));
                    break;
                case RequestMethod.PUT:
                    StartCoroutine(PutRequest(uri, sendData, authorization, response));
                    break;
            }
        }


        IEnumerator GetRequest(string uri, Action<long, string> response,
            string authorization)
        {
            UnityWebRequest www = UnityWebRequest.Get(uri);
            www.chunkedTransfer = false;

            if (!string.IsNullOrEmpty(authorization))
            {
                www.SetRequestHeader("AUTHORIZATION", authorization);
            }
            yield return www.SendWebRequest();
            Debug.Log($"Response for {uri}");
            Debug.Log(www.downloadHandler.text);
            response(www.responseCode, www.downloadHandler.text);

        }

        IEnumerator PostRequest(string uri, string postData, string authorization,
            Action<long, string> response)
        {
            Debug.Log(uri + " Sent =>"+postData);
            byte[] bytes = Encoding.UTF8.GetBytes(postData);
            using (UnityWebRequest www = UnityWebRequest.Post(uri, UnityWebRequest.kHttpVerbPOST))
            {
                if (!string.IsNullOrEmpty(postData))
                {
                    UploadHandlerRaw raw = new UploadHandlerRaw(bytes);
                    www.uploadHandler = raw;
                    www.SetRequestHeader("Content-Type", "application/json");
                }
                if (!string.IsNullOrEmpty(authorization))
                {
                    www.SetRequestHeader("AUTHORIZATION", authorization);
                }
                yield return www.SendWebRequest();
                Debug.Log(uri + "=>"+www.downloadHandler.text);
                response(www.responseCode, www.downloadHandler.text);
            }
        }

        IEnumerator PutRequest(string uri, string postData, string authorization,
            Action<long, string> response)
        {
            byte[] bytes = Encoding.UTF8.GetBytes(postData);
            using (UnityWebRequest www = UnityWebRequest.Put(uri, bytes))
            {
                www.SetRequestHeader("Content-Type", "application/json");
                if (!string.IsNullOrEmpty(authorization))
                {
                    www.SetRequestHeader("AUTHORIZATION", authorization);
                }
                yield return www.SendWebRequest();

                response(www.responseCode, www.downloadHandler.text);
            }
        }

    }

    public enum RequestMethod
    {
        POST,
        GET,
        PUT
    }
}