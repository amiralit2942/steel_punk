﻿using System;
using System.Collections.Generic;
using _Scripts.Controller.Abstract;
using _Scripts.Controller.Spells;
using _Scripts.Model;
using Backman.Identity.Models;
using Util;

namespace _Scripts.Manager
{
    public class SafeBox : MonoBehaviourSingleton<SafeBox>
    {
        public UserInfo userInfo;
        public User BackManUser;
        public List<Config> configs = new List<Config>();
        public List<float> manaIncomeRate = new List<float>();
        public List<float> manMax = new List<float>();
        public List<ShopItems> shopItems = new List<ShopItems>();
        public List<SpellModel> spellsInfo = new List<SpellModel>();
        public List<ChestInfo> chestInfo = new List<ChestInfo>();
        public List<BaseGunInfo> baseGunsInfo;
        public List<CardsInfo> cardsInfo = new List<CardsInfo>();
        public List<CardsInfo> ownedCards = new List<CardsInfo>();
        public List<CardsInfo> cardsToBuy = new List<CardsInfo>();
        public List<CardsInfo> deck = new List<CardsInfo>();
        public string roomName;
        //public Dictionary<Soldier.Soldiers, int> UpgradeCards;
        public GameState gameState;
        
        private void Awake()
        {
            DontDestroyOnLoad(gameObject);   
        }
    }
    
}