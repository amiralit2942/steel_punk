﻿using System;
using System.Collections.Generic;
using System.Data.SqlTypes;
using System.Text;
using _Scripts.Controller.Abstract;
using _Scripts.Model;
using _Scripts.Util;
using Microsoft.Win32;
using Newtonsoft.Json;
using UnityEngine;
using Util;

namespace _Scripts.Manager
{
    public class ServerManager : MonoBehaviourSingleton<ServerManager>
    {
        private string Uri
        {
            get
            {
                return "http://localhost:8070";
                //return "http://37.152.180.11:8070";
            }
        }


        void Awake()
        {
            DontDestroyOnLoad(this);
        }
        
        
        public void SetRegister(string userName,Action onSucceed)
        {
            var setStruct = new UserRegister();
            setStruct.name = userName;

            RequestManager.Instance.Send(RequestMethod.POST, Uri + "/user/register", (StatusCode, response) =>
            {
                print(Uri + "/user/register" + StatusCode +": " +response );
                switch (StatusCode)
                {
                    case 200:
                        onSucceed();
                        break;
                    case 401:
                        break;
                    case 404:
                        break;
                    default:
                        break;
                }
            }, SystemInfo.deviceUniqueIdentifier, JsonConvert.SerializeObject(setStruct));
        }

        public void Rename(string newUsername, Action onSucceed)
        {
            var setStruct = new Rename();
            setStruct.newUsername = newUsername;

            RequestManager.Instance.Send(RequestMethod.POST, Uri + "Rename", (StatusCode, response) =>
            {
                switch (StatusCode)
                {
                    case 200:
                        onSucceed();
                        break;
                }
            }, SystemInfo.deviceUniqueIdentifier, JsonConvert.SerializeObject(setStruct));
        }

        public void GetConfig(Action<List<Config>> onSuccess , Action onFailed)
        {

            RequestManager.Instance.Send(RequestMethod.GET, Uri + "/config", (statusCode, response) =>
            {
                print(response);
                switch (statusCode)
                {
                    case 200:
                        onSuccess(JsonConvert.DeserializeObject<List<Config>>(response));
                        break;
                    default:
                        onFailed();
                        print("Get Config :" + statusCode + " " + response);
                        break;
                }
            }, SystemInfo.deviceUniqueIdentifier);
        }

        public void GetShopItems(Action<List<ShopItems>> onSucceed , Action<int> onFail)
        {
            RequestManager.Instance.Send(RequestMethod.GET, Uri + "/shop/get", (StatusCode, response) =>
            {
                switch (StatusCode)
                {
                    case 200:
                        onSucceed(JsonConvert.DeserializeObject<List<ShopItems>>(response));
                        break;
                    case 401:
                        onFail(401);
                        break;
                    default:
                        onFail(default);
                        //Get Error Codes
                        break;
                }
            }, SystemInfo.deviceUniqueIdentifier);
        }

        public void GetCards(Action<List<CardsInfo>> onSucceed ,Action<long> onFailed)
        {
            RequestManager.Instance.Send(RequestMethod.GET,Uri + "/card/get", (StatusCode, response) =>
            {
                switch (StatusCode)
                {
                    case 200:
                        print("Cards Info" + response);
                        onSucceed(JsonConvert.DeserializeObject<List<CardsInfo>>(response));
                        break;
                    default:
                        onFailed(StatusCode);
                        break;
                }
            },SystemInfo.deviceUniqueIdentifier);
        }

        public void PostDeck(List<UserCard> newDeck ,Action<UserInfo> onSucceed, Action<string> onFailed)
        {
            var setStruct = new SetDeck();
            setStruct.new_deck = newDeck;//SafeBox.Instance.userInfo.deck;
            RequestManager.Instance.Send(RequestMethod.POST,Uri + "/user/changeDeckCards", (StatusCode, response) =>
            {
                print("Change Deck" + StatusCode);
                switch (StatusCode)
                {
                    case 200:
                        onSucceed(JsonConvert.DeserializeObject<UserInfo>(response));
                        break;
                    default:
                        onFailed(response);
                        break;
                }
            },SystemInfo.deviceUniqueIdentifier , JsonConvert.SerializeObject(setStruct));
        }

        public void PostSpellDeck(List<UserSpell> userSpells,Action onSucceed,Action<string> onFailed)
        {
            var newSpellDeck = new SetSpellDeck();
            newSpellDeck.userSpells = userSpells;
            
            RequestManager.Instance.Send(RequestMethod.POST,Uri + "/user/changeDeckSpells", (StatuseCode, response) =>
            {
                switch (StatuseCode)
                {
                    case 200:
                        onSucceed();
                        break;
                    default:
                        onFailed(response);
                        break;
                }
            },SystemInfo.deviceUniqueIdentifier,JsonConvert.SerializeObject(newSpellDeck));
        }

        public void ShopPurchase(string sku, string token, string userToken, Action onSucceed, Action<int> onFailed)
        {
            var setStruct = new Purchase();
            setStruct.sku = sku;
            setStruct.token = token;
            //setStruct.user_token = userToken;
            print(JsonConvert.SerializeObject(setStruct));
            RequestManager.Instance.Send(RequestMethod.POST, Uri + "/shop/purchase", (StatusCode, response) =>
            {
                switch (StatusCode)
                {
                    case 200:
                        onSucceed();
                        break;
                    case 404:
                        onFailed(401);
                        break;
                    default:
                        //Get Error Codes
                        break;
                }
            }, SystemInfo.deviceUniqueIdentifier, JsonConvert.SerializeObject(setStruct));
        }

        public void MatchStart(string roomName  ,uint playerOneId,uint playerTwoId,string playerOneName
            ,string playerTwoName, DateTime startTime,List<UserCard> playerOneDeck,List<UserCard> playerTwoDeck ,Action onSucceed,Action<long,string> onFailed)
        {
            var setStruct = new StartMatchStruct
            {
                room_name = roomName,
                player_one_deck = playerOneDeck,
                player_two_deck = playerTwoDeck,
                player_one_id = playerOneId,
                player_two_id = playerTwoId,
                player_one_name = playerOneName,
                player_two_name = playerTwoName,
                start_time = startTime
            };
            RequestManager.Instance.Send(RequestMethod.POST,Uri + "/match/start", (StatusCode, response) =>
            {
                switch (StatusCode)
                {
                    case 200:
                        onSucceed();
                        break;
                    default:
                        onFailed(StatusCode,response);
                        break;
                }
            },SystemInfo.deviceUniqueIdentifier, JsonConvert.SerializeObject(setStruct));
        }
        
        public void EndMatch(string matchName ,uint playerOneId,uint playerTwoId,bool playerTwoDc
            ,bool playerOneDc,List<UserSpell> usedSpells, uint winnerId,bool isBot , Action onSucceed ,Action<long , string> onFailed)
        {
            var setStruct = new EndMatchStruct
            {
                match_name = matchName,
                player_one_id = playerOneId,
                player_two_id = playerTwoId,
                player_one_disconnected = playerOneDc,
                player_two_disconnected = playerTwoDc,
                winner_id = winnerId,
                used_spells = usedSpells,
                is_bot = isBot
            };

            print(Uri);
            RequestManager.Instance.Send(RequestMethod.POST,Uri +"/match/finish", (statusCode, response) =>
            {
                switch (statusCode)
                {
                    case 200:
                        onSucceed();
                        break;
                    default:
                        onFailed(statusCode , response);
                        break;
                }
            },SystemInfo.deviceUniqueIdentifier, JsonConvert.SerializeObject(setStruct));
        }

        public void OpenChestWithGem(uint chestId,Action onSucceed,Action<string> onFailed)
        {
            var setStruct = new UnlockUserChestPost();
            setStruct.userChestID = chestId;
            RequestManager.Instance.Send(RequestMethod.POST,Uri + "/chest/unlockGem", (statusCode,response) =>
            {
                switch (statusCode)
                {
                    case 200:
                        onSucceed();
                        break;
                    default:
                        onFailed(response);
                        break;
                }
            },SystemInfo.deviceUniqueIdentifier,JsonConvert.SerializeObject(setStruct));
        }

        public void ItemShopPurchase( uint cardId , Action<UserInfo> onSucceed , Action<string> onFailed)
        {
            var setStruct = new ItemShopPurchase();
            setStruct.card_id = cardId;
            
            RequestManager.Instance.Send(RequestMethod.POST , Uri + "/card/buy" ,
                (StatusCode, response) =>
                {
                    switch (StatusCode)
                    {
                        case 200:
                            onSucceed(JsonConvert.DeserializeObject<UserInfo>(response));
                            break;
                        case 401:
                            //Unauthorized
                            //Password Unauthorized
                            onFailed(response);
                            break;
                        case 403:
                            //Forbidden Token! (Item Not Found)
                            //Not Enough Coin!
                            onFailed(response);
                            break;
                        case 404:
                            //User Not Found!
                            onFailed(response);
                            break;
                        default:
                            onFailed(default);
                            break;
                    }
                },SystemInfo.deviceUniqueIdentifier,JsonConvert.SerializeObject(setStruct));
        }
        
        public void BuySpell(uint spellId,Action<UserSpell> onSucceed,Action<string> onFailed)
        {
            var setStruct = new BuySpellStruct();
            setStruct.spellID = spellId;
            
            RequestManager.Instance.Send(RequestMethod.POST,Uri+"/spell/buy", (StatusCode,response) =>
            {
                switch (StatusCode)
                {
                    case 200:
                        onSucceed(JsonConvert.DeserializeObject<UserSpell>(response));
                        break;
                    default:
                        onFailed(response);
                        break;
                        
                }
            },SystemInfo.deviceUniqueIdentifier,JsonConvert.SerializeObject(setStruct));
        }

        public void GiftCode(string code, int amount , Action onSucceed)
        {
            var setStruct = new GiftCode();
            setStruct.code = code;
            setStruct.amount = amount;

            RequestManager.Instance.Send(RequestMethod.POST, Uri + "/giftCode", (StatusCode, response) =>
            {
                switch (StatusCode)
                {
                    case 200:
                        onSucceed();
                        break;
                    default:
                        //Get Error Codes
                        break;

                }
            }, SystemInfo.deviceUniqueIdentifier, JsonConvert.SerializeObject(setStruct));
        }

        // public void ChestOpened(string name, uint chestType, List<ChestContents> chestContents, int price, uint priceType,
        //     int openingTime, int chance, Action onSucceed , Action onFailed)
        // {
        //     var setStruct = new Chest();
        //     setStruct.name = name;
        //     setStruct.chest_type = chestType;
        //     setStruct.contents = chestContents;
        //     setStruct.price = price;
        //     setStruct.price_type = priceType;
        //     setStruct.opening_time = openingTime;
        //     setStruct.chance = chance;
        //
        //     RequestManager.Instance.Send(RequestMethod.POST, Uri + "/chest/unlock", (StatusCode, response) =>
        //     {
        //         switch (StatusCode)
        //         {
        //             case 200:
        //                 onSucceed();
        //                 break;
        //             default:
        //                 onFailed();
        //                 break;
        //         }
        //     }, SystemInfo.deviceUniqueIdentifier, JsonConvert.SerializeObject(setStruct));
        // }

        public void GetChestInfo(Action<List<ChestInfo>> onSucceed,Action<string> onFailed)
        {
            RequestManager.Instance.Send(RequestMethod.GET,Uri + "chest/get", (StatusCode, response) =>
            {
                switch (StatusCode)
                {
                    case 200:
                        onSucceed(JsonConvert.DeserializeObject<List<ChestInfo>>(response));
                        break;
                    default:
                        onFailed(response);
                        break;
                }
            },SystemInfo.deviceUniqueIdentifier);
        }

        public void GetChestRemainingTime(Action<UnlockChestResponse> onSucceed,Action<string> onFailed)
        {
            RequestManager.Instance.Send(RequestMethod.GET,Uri + "/chest/inquiry", (statusCode, response) =>
            {
                switch (statusCode)
                {
                    case 200:
                        onSucceed(JsonConvert.DeserializeObject<UnlockChestResponse>(response));
                        break;
                }
            },SystemInfo.deviceUniqueIdentifier);
        }
        

        public void UnlockChestPost
            (uint userChestId,Action<UnlockChestResponse> onSucceed,Action<string> onFailed)
        {
            var unlockingChest = new UnlockUserChestPost();
            unlockingChest.userChestID = userChestId;
            
            RequestManager.Instance.Send(RequestMethod.POST,Uri + "/chest/unlockTime",
                (statusCode, response) =>
                {
                    switch (statusCode)
                    {
                        case 200:
                            onSucceed(JsonConvert.DeserializeObject<UnlockChestResponse>(response));
                            break;
                        default:
                            onFailed(response);
                            break;
                    }
                },SystemInfo.deviceUniqueIdentifier,JsonConvert.SerializeObject(unlockingChest));
        }

        public void OpenChest(uint userChestId,Action<OpenChestStruct> onSucceed,Action<string> onFailed)
        {
            var openedChest = new UnlockUserChestPost();
            openedChest.userChestID = userChestId;
            
            RequestManager.Instance.Send(RequestMethod.POST,Uri + "/chest/open", (statusCode, response) =>
            {
                switch (statusCode)
                {
                    case 200:
                        onSucceed(JsonConvert.DeserializeObject<OpenChestStruct>(response));
                        break;
                    default:
                        onFailed(response);
                        break;
                }
            },SystemInfo.deviceUniqueIdentifier,JsonConvert.SerializeObject(openedChest));
        }

        public void OpenChestWithGemPrice(uint unlockingUserChestID,Action<GetOpenChestWithGemPrice> onSucceed,Action<string> onFailed)
        {
            var setStruct = new UnlockUserChestPost();
            setStruct.userChestID = unlockingUserChestID;
            
            RequestManager.Instance.Send(RequestMethod.POST,Uri + "/chest/timeToGem", (statusCode,response) =>
            {
                
                switch (statusCode)
                {
                    case 200:
                        onSucceed(JsonConvert.DeserializeObject<GetOpenChestWithGemPrice>(response));
                        
                        break;
                    default:
                        onFailed(response);
                        break;
                }
            },SystemInfo.deviceUniqueIdentifier,JsonConvert.SerializeObject(setStruct));
        }

        public void GetUpgradeCards(Action<Dictionary<Soldier.Soldiers,int>> onSucceed,Action<string> onFailed)
        {
            RequestManager.Instance.Send(RequestMethod.GET,Uri+"/upgradeCard/get", ( statusCode,response) =>
            {
                switch (statusCode)
                {
                    case 200:
                        onSucceed(JsonConvert.DeserializeObject<Dictionary<Soldier.Soldiers, int>>(response));
                        break;
                    default:
                        onFailed(response);
                        break;
                }
            },SystemInfo.deviceUniqueIdentifier);
        }

        public void GetUser(Action onSucceed, Action onRegister, Action<int> onfailed)
        {
            RequestManager.Instance.Send(RequestMethod.GET, Uri + "/user/robot", (StatusCode, response) =>
            {
                switch (StatusCode)
                {
                    case 200:
                        onSucceed();
                        break;
                    case 404:
                        onRegister();
                        //user no found
                        break;
                    case 403:
                        onfailed(403);
                        break;
                    case 401:
                        //Login pass Unauthorized 
                        onfailed(401);
                        break;
                    default:
                        onfailed(default);
                        // No internet connection
                        break;
                }
            }, SystemInfo.deviceUniqueIdentifier);
        }


        public void Login(Action<UserInfo> onSucceed,Action onRegister ,Action<long> onFailed)
        {
            RequestManager.Instance.Send(RequestMethod.GET,Uri + "/user/login", (StatusCode, response) =>
            {
                switch (StatusCode)
                {
                    case 200:
                        onSucceed(JsonConvert.DeserializeObject<UserInfo>(response));
                        break;
                    case 404:
                        onRegister();
                        break;
                    default:
                        onFailed(StatusCode);
                        break;
                }
            }, SystemInfo.deviceUniqueIdentifier);
        }

        public void GetSpellsInfo(Action<List<SpellModel>> onSucceed, Action<long> onFailed)
        {
            RequestManager.Instance.Send(RequestMethod.GET,Uri + "/spell/get", (StatusCode, response) =>
            {
                    print("Get Cards Response : " + response);
                switch (StatusCode)
                {
                    case 200:
                        onSucceed(JsonConvert.DeserializeObject<List<SpellModel>>(response));

                        break;
                    default:
                        onFailed(StatusCode);
                        break;
                }
            },SystemInfo.deviceUniqueIdentifier);
        }

       

        

        public void GetBaseGunsInfo(Action<List<BaseGunInfo>> onSucceed,Action<long> onFailed)
        {
            RequestManager.Instance.Send(RequestMethod.GET, Uri + "", (StatusCode, response) =>
            {
                switch (StatusCode)
                {
                    case 200:
                        JsonConvert.DeserializeObject<List<BaseGunInfo>>(response);
                        break;
                    default:
                        onFailed(StatusCode);
                        break;
                }
            }, SystemInfo.deviceUniqueIdentifier);
        }
    }
}