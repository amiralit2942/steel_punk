﻿using System;
using System.Collections.Generic;
using System.Linq;
using _Scripts.Controller.Abstract;
using _Scripts.Model;
using UnityEngine;
using Util;

namespace _Scripts.Manager
{
    public class ShardManager : MonoBehaviourSingleton<ShardManager>
    {
        public UserCard FindCard(uint cardID)
        {
            return SafeBox.Instance.userInfo.cards
                .Find(x => x.card_id == cardID);
        }
        
        public bool CheckShard(uint cardID, int requiredShard)
        {
            var card = FindCard(cardID);
            if (card.shards >= requiredShard)
            {
                return true;
            }

            return false;
        }

        public int GetShardAmount(uint cardID)
        {
            return FindCard(cardID).shards;
        }

        // public void UseShard(uint cardId,int usedShard)
        // {
        //     if (CheckShard(cardId,usedShard))
        //     {
        //         FindCard(cardId).shards -= usedShard;
        //     }
        // }

        public void EarnShard(uint cardID,int earnedCard)
        {
            FindCard(cardID).shards += earnedCard;
        }
        
    }
}