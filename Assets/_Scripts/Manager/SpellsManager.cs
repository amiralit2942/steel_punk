﻿using System;
using System.Collections.Generic;
using _Scripts.Controller.Card.Deck;
using _Scripts.Controller.Spells;
using _Scripts.Model;
using UnityEngine;
using Util;

namespace _Scripts.Manager
{
    public class SpellsManager : MonoBehaviourSingleton<SpellsManager>
    {
        private List<UserSpell> _spellDeck;

        private void Start()
        {
            _spellDeck = new List<UserSpell>(2);
            _spellDeck = SafeBox.Instance.userInfo.deck_spells;
        }

        public void EarnSpell(UserSpell userSpell)
        {
            print("Spell You wanna But id " + userSpell.spellId);
            var spell = SafeBox.Instance.userInfo.spells
                ?.Find(x => x.spellId == userSpell.spellId);
            if (spell != null)//SafeBox.Instance.userInfo.spells.Contains(userSpell))
            {
                SafeBox.Instance.userInfo.spells
                    .Find(x => x.spellId == userSpell.spellId).amount++;
                print("Spell Added");
            }
            else
            {
                SafeBox.Instance.userInfo.spells.Add(userSpell);
                //SafeBox.Instance.userInfo.spells.Find(x => x.spellId == userSpell.spellId).amount++;
                print("Spell Created");
            }
            DeckPanel.Instance.NewCard(userSpell);
        }

        public void UseSpell(uint spellId)
        {
            SafeBox.Instance.userInfo.spells.Find(x => x.spellId == spellId).amount--;
        }

        public SpellModel GetSpellInfo(uint spellId)
        {
            print("3 Spell ID: " + spellId);
            return SafeBox.Instance.spellsInfo?.Find(x => x.id == spellId);
        }

        public UserSpell GetUserSpell(uint id)
        {
            return SafeBox.Instance.userInfo.spells.Find(x => x.spellId == id);
        }

        public void AddToDeck(uint id)
        {
            _spellDeck?.Add(GetUserSpell(id));
        }
    }
}
