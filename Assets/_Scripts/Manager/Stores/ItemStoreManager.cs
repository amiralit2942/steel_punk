﻿using System.Collections.Generic;
using _Scripts.Controller.Abstract;
using _Scripts.DataHolder;
using _Scripts.Model;
using _Scripts.Controller.Abstract;
using Manager;
using _Scripts.Model;
using UI;
using UnityEngine;
using Util;

namespace _Scripts.Manager.Stores
{
    public class ItemStoreManager : MonoBehaviourSingleton<ItemStoreManager>
    {
        //OLD!!!
        [SerializeField] private Transform buySoldierPanel;
        [SerializeField] private Transform buySpellPanel;
        [SerializeField] private Transform upgradePanel;
        private List<Soldier.Soldiers> toBuyList;
        private List<Soldier.Soldiers> toUpgrade;
        
        
        void Start()
        {
            toBuyList = new List<Soldier.Soldiers>();
            toUpgrade = new List<Soldier.Soldiers>();
            
     
            

            if (SafeBox.Instance.cardsToBuy != null)
            {
                foreach (var cardToBuy in SafeBox.Instance.cardsToBuy)
                {
                    if (!toBuyList.Contains(cardToBuy.soldier_type))
                    {
                        // TO BUY
                        var card =Instantiate(PrefabHolder.Instance.GetItemShopCard()
                            , buySoldierPanel);
                        card.name = cardToBuy.name;
                        card.soldierType = cardToBuy.soldier_type;
                        card.coinPrice = cardToBuy.coin_price;
                        card.priceType = ShopItems.PricesType.PriceCoin;
                        card.rewardType = ShopItems.RewardType.RewardCoin;
                        card.id = cardToBuy.ID;
                        card.isPurchased = false;
                        toBuyList.Add(cardToBuy.soldier_type);    
                    }
                }    
            }
            if (SafeBox.Instance.userInfo.cards != null)
            {
                foreach (var card in SafeBox.Instance.userInfo.cards)
                {
                    var cardInfo = SafeBox.Instance.cardsInfo.Find(x => x.ID == card.card_id);
                
                    //TO UPGRADE
                    if (cardInfo.soldier_type != Soldier.Soldiers.Base)
                    {
                        var upgradeCards = Instantiate(PrefabHolder.Instance.GetUpgradeCart(),
                            upgradePanel);
                    
                        upgradeCards.level = card.level;
                        upgradeCards.soldierType = cardInfo.soldier_type;
                        upgradeCards.coinPrice = cardInfo.coin_price;
                        upgradeCards.xpReward = cardInfo.xp_reward;
                        upgradeCards.name = cardInfo.name;
                        upgradeCards.id = card.card_id;
                        upgradeCards.priceType = ShopItems.PricesType.PriceCoin;
                        upgradeCards.rewardType = ShopItems.RewardType.RewardCoin;
                        upgradeCards.isPurchased = true;    
                    }
                }    
            }
            if (SafeBox.Instance.spellsInfo != null)
            {
                foreach (var spell in SafeBox.Instance.spellsInfo)
                {
                    var card = Instantiate(PrefabHolder.Instance.GetSpellShopCard(), buySpellPanel);
                    card.SetInfo(spell);
                }    
            }
            
        }

        public void CreatUpgradeCard(SoldierUpgradeCard shopCard)
        {
            var upgradeCard = Instantiate(PrefabHolder.Instance.GetUpgradeCart(), PanelManager.Instance.upgradeItem);
            upgradeCard.image.sprite = PrefabHolder.Instance.GetCardImage(shopCard.soldierType);
            upgradeCard.level = shopCard.level;
            upgradeCard.coinPrice = shopCard.coinPrice;
            upgradeCard.name = shopCard.name;
            upgradeCard.id = shopCard.id;
            upgradeCard.priceType = shopCard.priceType;
            upgradeCard.rewardType = shopCard.rewardType;
            upgradeCard.isPurchased = true;
            
        }

        

        
    }
}