﻿using System.Collections.Generic;
using _Scripts.Model;
using Manager;
using Util;

namespace _Scripts.Manager.Stores
{
    public class ShopsProductManager : MonoBehaviourSingleton<ShopsProductManager>
    {
        private List<ShopItems> _items;
        private List<ShopItems> _itemStoreItems;
        private List<ShopItems> _coinStoreItems;
        private List<ShopItems> _gemStoreItems;
        // Start is called before the first frame update
        void Start()
        {
            //GetItemsFromServer();
            //_items = new List<ShopItems>();
            _itemStoreItems = new List<ShopItems>();
            _coinStoreItems = new List<ShopItems>();
            _gemStoreItems = new List<ShopItems>();
            
            // foreach(var item in _items)
            // {
            //     switch (item.reward_type)
            //     {
            //         case ShopItems.RewardType.RewardCoin:
            //             _itemStoreItems.Add(item);
            //             break;
            //         case ShopItems.RewardType.RewardGem:
            //             _coinStoreItems.Add(item);
            //             break;
            //         case ShopItems.RewardType.PriceMoney:
            //             _gemStoreItems.Add(item);
            //             print("Gem item Collected");
            //             break;
            //     }
            // }
        }
        
        public void GetItemsFromServer()
        {
            _items.AddRange(SafeBox.Instance.shopItems);
            print("Shop Products Collected");
        }

        public List<ShopItems> GetCoinStoreProducts()
        {
            return _coinStoreItems;
        }
        
        public List<ShopItems> GetItemStoreProducts()
        {
            return _itemStoreItems;
        }
        
        public List<ShopItems> GetGemStoreProducts()
        {
            return _gemStoreItems;
        }
        
    }
}