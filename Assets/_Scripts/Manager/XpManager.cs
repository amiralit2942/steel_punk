﻿using Manager;
using UI;
using Util;

namespace _Scripts.Manager
{
    public class XpManager :MonoBehaviourSingleton<XpManager>
    {
        private int _level;
        private int _xp;
        private int nextLevelXp;
        

         private void Awake()
         {
             DontDestroyOnLoad(gameObject);
         }

         
        void Start()
        {
           
            
            
            _level = SafeBox.Instance.userInfo.level;
            _xp = SafeBox.Instance.userInfo.xp;
            
            CalculateLevel();
            
        }


        private void CalculateLevel()
        {
            
            /*for (int i = 0; _xp > nextLevelXp[i]; i++)
            {
                _level++;
            }*/
            // for (int i = 1; _xp > nextXp  ; i++)
            // {
            //     nextXp = nextXp * 2;
            //     _level = i;
            // }
        }
        

        // public int GetLevel()
        // {
        //     return _level;
        // }

        public int GetXp()
        {
            return _xp;
        }
        //
        // public int GetNextLevelXp()
        // {
        //     return nextLevelXp[_level + 1];
        // }

       

        public void EarnXp(int xp)
        {
            _xp += xp;
        }
        
        
    }
}