﻿using System;
using _Scripts.Controller;
using _Scripts.Controller.Abstract;

namespace _Scripts.Model
{
    [Serializable]
    public class BaseInfo
    {
        public Base.Bases baseType;
        public Soldier.Side baseSide;
        public int health;
        public int gunDamage;
        public int gunRange;
        public float coolDown;
        
       
    }
}
