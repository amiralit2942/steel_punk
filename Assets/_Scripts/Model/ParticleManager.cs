using System;
using System.Collections.Generic;
using _Scripts.Controller.Abstract;
using _Scripts.Controller.Spells;
using _Scripts.Model.ScriptableObjectsScripts;
using UnityEngine;
using Util;
using Random = UnityEngine.Random;


namespace _Scripts.Model
{
    public class ParticleManager : MonoBehaviourSingleton<ParticleManager>
    {
        [SerializeField] private ParticleHolderAsset soldiersParticles;
        [SerializeField] private SpellParticleHolder spellParticleHolder;
        [SerializeField] private List<ParticleSystem> comicParticles = new List<ParticleSystem>();
        [SerializeField] private List<int> comicParticlePerHit;
        public List<int> ComicParticlePerHit => comicParticlePerHit;

        private void Awake()
        {
            DontDestroyOnLoad(this);
        }

        public GameObject GetSoldierParticle(Soldier.Soldiers soldierType, ParticleType particleType)
        {
            //print(soldierType +" And " + particleType);
            var particle = soldiersParticles.particleHolders.Find(
                x => x.soldierType == soldierType && x.particleType == particleType);
            //print("Particle name "+particle.particle.name);
            return particle.particle;
        }

        public ParticleSystem GetSpellParticle(Spell.SpellType spellType, float duration)
        {
            var particle = spellParticleHolder.SpellParticles.Find(x => x.SpellType == spellType).Particle;
            particle.time = duration;

            return particle;
        }

        public ParticleSystem GetComicParticle()
        {
            return comicParticles[Random.Range(0,comicParticles.Count)];
        }
}
}
