﻿using System;
using System.Collections.Generic;
using UnityEngine;

namespace _Scripts.Model.ScriptableObjectsScripts
{
    [CreateAssetMenu(fileName = "BaseGunSpriteHolder" , menuName = "ScriptableObjects/BaseGunSpriteHolder",order = 5)]
    public class BaseGunSpriteHolder : ScriptableObject
    {
        [SerializeField] private List<BaseGunSprite> baseGunSprites;
        public List<BaseGunSprite> BaseGunSprites => baseGunSprites;
    }

    [Serializable]
    public class BaseGunSprite
    {
        [SerializeField] private BaseGunType baseGunType;
        public BaseGunType BaseGunType1 => baseGunType;
        [SerializeField] private Sprite gunSprite;
        public Sprite GunSprite => gunSprite;
    }
}
