﻿using System;
using System.Collections.Generic;
using _Scripts.Controller.Player;
using _Scripts.Controller.Abstract;
using _Scripts.Soldiers;
using UnityEngine;

namespace _Scripts.Model.ScriptableObjects 
{

    
    [Serializable]
    public class CardHolder
    {
        public Soldier.Soldiers soldierType;
        public List<NormalSoldier> prefab;
    }
    
    
    [CreateAssetMenu(fileName = "CardHolderAsset",menuName = "ScriptableObjects/PrefabHolderAsset",order = 1)]
    public class CardHolderAsset : ScriptableObject
    {
        public List<CardHolder> cards;
    }
}