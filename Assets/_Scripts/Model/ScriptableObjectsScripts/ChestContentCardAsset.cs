﻿using System;
using System.Collections.Generic;
using _Scripts.Controller.Abstract;
using UnityEngine;
using UnityEngine.UI;

namespace _Scripts.Model.ScriptableObjectsScripts
{
    
        [Serializable]
        public class UpgradeCardContent
        {
            [SerializeField] private Soldier.Soldiers soldierType;
            public Soldier.Soldiers SoldierType => soldierType;
            
            [SerializeField] private Sprite sprite;
            public Sprite Sprite => sprite;
        }

        [CreateAssetMenu(fileName = "ChestContentAsset ", menuName = "ScriptableObjects/ChestContentAsset", order = 3)]
        public class ChestContentCardAsset : ScriptableObject
        {
            [SerializeField] private Sprite coinSprite;
            public Sprite CoinSprite => coinSprite;
            [SerializeField] private Sprite gemSprite;
            public Sprite GemSprite => gemSprite;
            [SerializeField] private List<UpgradeCardContent> upgradeCardsAsset;
            public List<UpgradeCardContent> UpgradeCardsAsset => upgradeCardsAsset;

        }
    
}
