﻿using System;
using System.Collections.Generic;
using _Scripts.Controller.Card.Chest;
using UnityEngine;

namespace _Scripts.Model.ScriptableObjectsScripts
{
    [Serializable]
    public class ChestPrefab
    { 
        [SerializeField] private ChestType chestType;
        [SerializeField] private ChestCard chestCard;

        public ChestType ChestType => chestType;
        public ChestCard ChestCard => chestCard;
    }
    
    [CreateAssetMenu(fileName = "ChestPrefabsHolder", menuName = "ScriptableObjects/ChestPrefabsHolder", order = 23)]
    public class ChestsPrefabHolder : ScriptableObject
    {
        [SerializeField] private List<ChestPrefab> chestPrefabs;
        public List<ChestPrefab> ChestPrefabs => chestPrefabs;
    }
}