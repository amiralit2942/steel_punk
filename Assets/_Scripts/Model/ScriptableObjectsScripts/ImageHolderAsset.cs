﻿using System;
using System.Collections.Generic;
using _Scripts.Controller.Abstract;
using UnityEngine;

namespace _Scripts.Model.ScriptableObjectsScripts
{
    
        [Serializable]
        public class ImageHolder
        {
            public Soldier.Soldiers soldierType;
            public Sprite soldierImage;
        }

        [CreateAssetMenu(fileName = "ImageHolderAsset", menuName = "ScriptableObjects/ImageHolderAsset", order = 2)]
        public class ImageHolderAsset : ScriptableObject
        {
            public List<ImageHolder> images;
        }
    
}
