﻿using System;
using System.Collections.Generic;
using _Scripts.Controller.Abstract;
using UnityEngine;

namespace _Scripts.Model.ScriptableObjectsScripts
{

    public enum ParticleType
    {
        Hit,
        Death
    }
    
    [Serializable]
    public class ParticleHolder
    {
        public Soldier.Soldiers soldierType;
        public ParticleType particleType;
        public GameObject particle;
    } 
    
    [CreateAssetMenu(fileName = "ParticleHolderAsset",menuName = "ScriptableObjects/ParticleHolderAsset",order = 2)]
    public class ParticleHolderAsset : ScriptableObject
    {
        public List<ParticleHolder> particleHolders;
    }

   
}
