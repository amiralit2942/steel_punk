﻿using System;
using System.Collections.Generic;
using _Scripts.Controller.Spells;
using UnityEngine;
using UnityEngine.UI;

namespace _Scripts.Model.ScriptableObjectsScripts
{
    [Serializable]
    public class SpellImageHolder
    {
        public Spell.SpellType spellType; 
        public Sprite spellImage;
    }
    
    
    
    [CreateAssetMenu(fileName = "SpellImageHolderAsset" , menuName = "ScriptableObjects/SpellImageHolderAsset" , order = 23)]
    public class SpellImageHolderAsset : ScriptableObject
    { 
        public List<SpellImageHolder> spellImageHolders;
    }
}