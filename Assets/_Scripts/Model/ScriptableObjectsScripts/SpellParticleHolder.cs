﻿using System;
using System.Collections.Generic;
using _Scripts.Controller.Spells;
using UnityEngine;

namespace _Scripts.Model.ScriptableObjectsScripts
{
    
    [Serializable]
    public class SpellParticle
    {
        [SerializeField] private Spell.SpellType spellType;
        [SerializeField] private ParticleSystem particle;

        public Spell.SpellType SpellType => spellType;
        public ParticleSystem Particle => particle;
    }
    
    
    
    [CreateAssetMenu(fileName = "SpellParticleHolder", menuName = "ScriptableObjects/SpellParticleHolder", order = 17)]
    public class SpellParticleHolder : ScriptableObject
    {
        [SerializeField] private List<SpellParticle> spellParticles;

        public List<SpellParticle> SpellParticles => spellParticles;
    }
}