﻿using System;
using System.Collections.Generic;
using _Scripts.Controller.Abstract;
using _Scripts.Controller.Spells;
using Newtonsoft.Json;
using UnityEngine.UIElements;

namespace _Scripts.Model
{
    [Serializable]
    public class UserInfo
    {
        
        private static UserInfo _instance;

        public static UserInfo Instance
        {
            get
            {
                if (_instance == null)
                {
                    new UserInfo();
                }

                return _instance;
            }
            set
            {
                _instance = value;
            }
        }
        private UserInfo()
        {
            _instance = this;
        }
        
        public uint id;
        public string name;
        public string device_id;
        public int gem;
        public int coin;
        public int xp;
        public int level;
        public int trophy;
        public List<UserCard> cards = new List<UserCard>() ;
        public List<UserCard> deck_cards = new List<UserCard>();
        public List<UserSpell> spells = new List<UserSpell>();
        public List<UserSpell> deck_spells = new List<UserSpell>();
        public int total_win;
        public int total_lose;
        public List<Friends> friends = new List<Friends>();
        public List<UserChest> user_chests = new List<UserChest>();
        public uint unlocking_user_chest_id;


        // public List<UserBaseGun> user_base_guns;
        // public UserBaseGun selectedGun;
        // public DateTime last_user_shop_time;

    }

    [Serializable]
    public class UserCard
    {
        public uint id;
        public uint card_id;
        public int level;
        public int shards;
        public uint user_id;
    }
    
    [Serializable]
    public class CardsInfo 
    {
        public uint ID;
        public string name;
        public int required_mana;
        public List<int> coin_price;
        public List<int> gem_price;
        public List<int> xp_reward;
        public List<int>  damage;
        public List<int>  health;
        public float  view_range;
        public float  speed;
        public float  spawn_cool_down;
        public float  attack_cool_down;
        private List<int> skin;
        public bool is_register_card;
        public Soldier.Soldiers soldier_type;
        public List<int> required_shard;
    }

    [Serializable]
    public class EndMatchStruct
    {
        public bool is_bot;
        public string match_name;
        public bool player_one_disconnected;
        public bool player_two_disconnected;
        public uint player_one_id;
        public uint player_two_id;
        public List<UserSpell> used_spells;
        public uint winner_id;
        
        
        
    }

    [Serializable]
    public class StartMatchStruct
    {
        public List<UserCard> player_one_deck;
        public uint player_one_id;
        public string player_one_name;
        public List<UserCard> player_two_deck;
        public uint player_two_id;
        public string player_two_name;
        public string room_name;
        public DateTime start_time;
    }

    [Serializable]
    public class EndMatchFields
    {
        public uint ID;
        public string name;
        public int lose_price;
        public int win_prize;
        public ChestInfo winnerChestInfo;
    }
    
    //Get
    [Serializable]
    public class Config
    {
        public string key;
        public string value;
    }
    
    

    /// <summary>
    /// ///////////////////////////////////////////////////////////////////////////////////
    /// </summary>
    /// Shop

    [Serializable]
    public class ShopItems
    {
        public uint id;
        public string name;
        public string sku;
        public int reward_amount;
        public PricesType price_type;
        public int price;
        public string picture_url;
        public RewardType reward_type;
        
        
        public enum RewardType
        {
            RewardCoin = 0,
            RewardGem = 1,
            RewardSpell = 2
        }

        public enum  PricesType
        {
            PriceCoin = 0 ,
            PriceGem = 1,
            PriceMoney = 2
        }
    }

    [Serializable]
    public class Bundles
    {
        public BundlePrices type;
        public uint property;
        public int amount;

        public enum BundlePrices
        {
            BundleCoin = 0 ,
            BundleGem = 1,
            BundleChest = 2
        }
    }

    [Serializable]
    public class Purchase
    {
        public string sku;
        public string token;
        //public string user_token;
    }

    [Serializable]
    public class ItemShopPurchase
    {
        public uint card_id;
    }
///////////////////////////////////////////////////////////////////////////////////////
    ///
    /// LeaderBoard
    ///
    /// 
    [Serializable]
    public class LeaderBoard
    {
        public int rank;
        public int level;
        public string name;
        public int score;
        public bool myRank;
    }

    [Serializable]
    public class ChestInfo
    {
        public string name;
        public ChestType chest_type;
        public List<ChestContents> contents;
        public int price;
        public int opening_time;
    }
    
    [Serializable]
    public class UnlockUserChestPost
    {
        //For Opening and Unlocking
        [JsonProperty("user_chest_id")]public uint userChestID;
    }

    [Serializable]
    public class GetOpenChestWithGemPrice
    {
        [JsonProperty("gem_cost")]public int gemCost;
    }

    [Serializable]
    public class UnlockChestResponse
    {
        public uint user_chest_id;
        public float remaining_time;
        public ChestState chest_state;
    }

    [Serializable]
    public class UserChestPrizeStruct
    {
        [JsonProperty("prize_type")]public PrizeType prizeType;
        [JsonProperty("prize_amount")] public int prizeAmount;
        [JsonProperty("card_id")] public uint cardId;
        [JsonProperty("user_card")] public UserCard userCard;
    }

    [Serializable]
    public class ChestRewardStruct
    {
        [JsonProperty("coin_amount")] public int coinAmount;
        [JsonProperty("gem_amount")] public int gemAmount;
        [JsonProperty("card_id")] public uint cardId;
        [JsonProperty("card_amount")] public int cardAmount;
    }

    [Serializable]
    public class OpenChestStruct
    {
        [JsonProperty("chest_reward")]public ChestRewardStruct chestReward;
        public UserInfo user;

    }

    public enum PrizeType
    {
        CoinPrizeType  = 0,
        GemPrizeType   = 1,
        CardPrizeType = 2 ,
        SpellPrizeType = 3
    }
    

    [Serializable]
    public class UserChest
    {
        public uint ID;
        public uint user_id;
        public uint chest_id;
        public ChestType chest_type;
        public ChestState chest_state;
        public DateTime start_time;
    }

    [Serializable]
    public enum ChestState
    {
        ChestEmpty = 0,
        ChestOpened = 1,
        ChestUnlocking = 2,
        ChestLocked = 3
    }

    [Serializable]
    public enum ChestType
    {
        CommonChest = 0 ,
        RareChest = 1,
        EpicChest = 2
    }

    [Serializable]
    public class ChestContents
    {
        public ChestContentType chestContentType;
        public int max;
        public int min;
    }
    public enum ChestContentType
    {
        Coin = 0 ,
        Gem = 1 ,
        Card = 2,
        Spell = 3
    }



    [Serializable]
    public class GiftCode
    {
        public string code;
        public int amount;
    }

    

    
    
    //Make a list of it in MatchStart class
    [Serializable]
    public class PlayerCard
    {
        public uint card_id;
        public int user_id;
        public int level;
    }
    

    [Serializable]
    public class SetUserCurrencies
    {
        public int coin;
        public int gem;
    }
    
    public enum GameState
    {
        InGame,
        InMenu,
    }

    [Serializable]
    public class UserRegister
    {
        public string name;
    }

    [Serializable]
    public class SetDeck
    {
        public List<UserCard> new_deck;
    }

    [Serializable]
    public class Rename
    {
        public string newUsername;
    }

    [Serializable]
    public class GetSoldiers
    {
        public List<Soldier.Soldiers> equipedSoldiers;
    }

    [Serializable]
    public class SpellModel
    {
        public uint id;
        public float duration;
        public string name;
        public int price;
        public int power;
        public Spell.SpellType spell_type;
        
        
    }

    [Serializable]
    public class BuySpellStruct
    {
        [JsonProperty("spell_id")]public uint spellID;
    }


    [Serializable]
    public class UserSpell
    {
        //[JsonProperty("ID")] public uint id;
        [JsonProperty("amount")] public int amount;
        [JsonProperty("spell_id")] public uint spellId;
        [JsonProperty("user_id")] public uint userId;

        public override bool Equals(object obj)
        {
            var spell = (UserSpell) obj;
            return spell != null && spell.spellId == spellId;
        }
    }

    [Serializable]
    public class UserBaseGun
    {
        public uint id;
        public int damage;
        public float coolDown;
    }

    [Serializable]
    public class BaseGunInfo
    {
        public BaseGunType base_gun_type;
        public float cooldown;
        public int damage;
        public uint gun_id;
        public float range;
        
        
    }

    [Serializable]
    public class Friends
    {
        public uint ID;
        public uint friend_id;
        public string friend_name;
        public int friend_level;
    }

    public enum  BaseGunType
    {
        //TODO Add Guns       
    }

    [Serializable]
    public class SetSpellDeck
    {
        [JsonProperty("new_deck")]public List<UserSpell> userSpells;
    }

    public class BackmanUser
    {
        public string UserId { get; set; }
        public string NickName { get; set; }
        public string ProfileUrl { get; set; }
    }
    


    

    
    
    
}