﻿using System;
using _Scripts.Controller.Abstract;

namespace _Scripts.Model
{
    [Serializable]
    public class SoldierInfo
    {
        public Soldier.Soldiers soldierType;
        public int soldierLevel;
        public int soldierUpgradePrice;
        public int requiredMana;
        public int health;
        public int damage;
        public float coolDown;
        public int coolDownLevel;
        public float viewRange;
        public float speed;
        public bool isSpecial;
        public int usage;
        
    }
}
