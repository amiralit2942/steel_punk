﻿using System.Collections.Generic;
using _Scripts.Controller.Abstract;
using _Scripts.Manager;
using _Scripts.Manager.Currencies;
using _Scripts.Model;
using _Scripts.DataHolder;
using _Scripts.Manager.Stores;
using _Scripts.UI;
using Manager;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace _Scripts
{
    public class SoldierUpgradeCard : MonoBehaviour
    {
    
    
        //SerializeFields
        [SerializeField] private Text nameText;
        [SerializeField] public Text priceText;
        [SerializeField] public Text levelText;
        [SerializeField] public Button upgradeButton;
        [SerializeField] private TMP_Text upgradeCardSliderText;
        [SerializeField] private Image upgradeCardSliderFillImage;
        [SerializeField] private Slider upgradeCardSlider;
        //Infos
        public string name;
        public List<int> coinPrice;
        public List<int> gemPrice;
        public List<int> xpReward;
        public int level;
        public ShopItems.RewardType rewardType;
        public ShopItems.PricesType priceType;
        public Soldier.Soldiers soldierType;
        public uint id;
        public bool isPurchased;
        public int maxLevel;
        public CardsInfo cardsInfo;
        public Image image;
        private int _upgradeCards;
        public int UpgradeCards => _upgradeCards;

        //TODO ADD TO SERVER
        private int _requiredUpgradeCard;
        public int RequiredUpgradeCard => _requiredUpgradeCard;


        void Start()
        {
            _upgradeCards = 9;
            _requiredUpgradeCard = 10;
            level = 1;
            soldierType = Soldier.Soldiers.Bomber;
            //SetInfo();
            SetUpgradeCardSlider();
            name = "SSS";
            if (CheckIfItIsMax())
            {
                return;
            }
            priceText.color= Color.red;
            // if (coinPrice[level] <= CoinManager.Instance.GetCoin())
            // {
            //     priceText.color= Color.green;
            // }
        }


    
    

        public void OnClick()
        {
            //PopUpManager.Instance.BuyItem(coinPrice[level],id,this);
        }

        #region Commented Upgrade Method

        

        
        // public void Upgrade()
        // {
        //
        //     var gemPrice = CoinManager.Instance.ConvertCoinToGem(coinPrice[level]);
        //     if (CoinManager.Instance.GetCoin() >= coinPrice[level])
        //     {
        //         //PanelManager.Instance.AcceptBuyingItem(transform , coinPrice[level]);
        //         ServerManager.Instance.ItemShopPurchase(id, () =>
        //         {
        //             level++;
        //             CoinManager.Instance.BuyItem(coinPrice[level]);
        //             if (level >= maxLevel)
        //             {
        //                 print("Maxed");
        //                 levelText.text = "Max";
        //                 upgradeButton.interactable = false;
        //                 priceText.text = "";
        //                 return;
        //             }
        //             levelText.text = level.ToString();
        //             priceText.text = coinPrice[level].ToString();
        //             PanelManager.Instance.SetCurrencies();
        //             XpManager.Instance.EarnXp(xpReward[level]);
        //             XpManager.Instance.SetXpUi();
        //         
        //             if (!isPurchased)
        //             {
        //                 ItemStoreManager.Instance.CreatUpgradeCard(this);
        //                 Destroy(gameObject);
        //             }
        //         }, (statusCode) =>
        //         {
        //             PopUpManager.Instance.SpawnPopUp(1);
        //             if (statusCode == "Not Enough Coin!")
        //             {
        //                 PopUpManager.Instance.popUpText.text = "چرخ دنده‌ی کافی ندارید ";
        //             }
        //             else if (statusCode == "Sku Not Found!" || statusCode =="Forbidden Token" || statusCode == "Unauthorized")
        //             {
        //                 PopUpManager.Instance.popUpText.text = "یه چیزی درست نیست؛ بعدا دوباره متحان کن";
        //             }
        //             else if (statusCode == default)
        //             {
        //                 PopUpManager.Instance.popUpText.text = "نتت قطع شد که :(";
        //             }
        //
        //             //Set OK Button
        //             var ok = new  Button.ButtonClickedEvent();
        //             ok.AddListener(() =>
        //             {
        //                 PopUpManager.Instance.popUp.localPosition = PopUpManager.Instance.position;
        //                 PopUpManager.Instance.popUp.gameObject.SetActive(false);
        //             });
        //             PopUpManager.Instance.acceptButton.onClick = ok;
        //
        //         });
        //     }
        //     else if (CoinManager.Instance.GetCoin() <= coinPrice[level] && CoinManager.Instance.ConvertCoinToGem(coinPrice[level]) >= GemManager.Instance.GetGem())
        //     {
        //         PanelManager.Instance.NotEnoughCoin(gemPrice,true);
        //     }
        // }
        #endregion 
        
        public void AcceptBuyingItem()//int price, uint id, UpgradeCard upgradeCard)
        {
            var gemPrice = CoinManager.Instance.ConvertCoinToGem(coinPrice[level]);
            if (CoinManager.Instance.GetCoin() >= coinPrice[level] 
                && ShardManager.Instance.CheckShard(id,_requiredUpgradeCard))
            {
                //PanelManager.Instance.AcceptBuyingItem(transform, coinPrice[level]);
                ServerManager.Instance.ItemShopPurchase(id, (userInfo) =>
                {
                    SafeBox.Instance.userInfo = userInfo;
                    level++;
                    if (CheckIfItIsMax())
                    {
                        return;
                    }
                    UpdateUi();
                    //CoinManager.Instance.BuyItem(coinPrice[level]);
                    //ShardManager.Instance.UseShard(id, _requiredUpgradeCard);
                    CurrencyUIManager.Instance.UpdateUI();
                    //XpManager.Instance.EarnXp(upgradeCard.xpReward[upgradeCard.level]);
                    CurrencyUIManager.Instance.UpdateUI();
                    if (!isPurchased)
                    {
                        ItemStoreManager.Instance.CreatUpgradeCard(this);
                        Destroy(gameObject);                        
                        //transform.parent = PanelManager.Instance.upgradeItem;
                        //upgradeCard.isPurchased = true;
                    }
                    ServerManager.Instance.Login((userInfo) =>
                    {
                        SafeBox.Instance.userInfo = userInfo;
                        CardsManager.Instance.SetOwnedCardInfo();
                        CardsManager.Instance.SetDeckInfo();
                    },()=>{}, (response) =>
                    {
                        PopUpManager.Instance.LoginFailed();
                    });
                    
                }, (response) =>
                {
                    PopUpManager.Instance.SpawnPopUp(1);
                    if (response == "Not Enough Coin!")
                    {
                        PopUpManager.Instance.popUpText.text = "چرخ دنده‌ی کافی ندارید ";
                    }
                    else if (response == "Sku Not Found!" || response =="Forbidden Token" || response == "Unauthorized")
                    {
                        PopUpManager.Instance.popUpText.text = "یه چیزی درست نیست؛ بعدا دوباره متحان کن";
                    }
                    else if (response == default)
                    {
                        PopUpManager.Instance.popUpText.text = "نتت قطع شد که :(";
                    }

                    //Set OK Button
                    var ok = new  Button.ButtonClickedEvent();
                    ok.AddListener(() =>
                    {
                        PopUpManager.Instance.popUp.localPosition = PopUpManager.Instance.position;
                        PopUpManager.Instance.popUp.gameObject.SetActive(false);
                    });
                    PopUpManager.Instance.acceptButton.onClick = ok;
                });
            }
        }
    
        public void UpdateUi()
        {
            levelText.text = level.ToString();
            priceText.text = coinPrice[level].ToString();
        }

        public bool CheckIfItIsMax()
        {
            if (level >= maxLevel && isPurchased)
            {
                levelText.text = "Max";
                priceText.text = "";
                upgradeButton.interactable = false;
                return true;
            }

            return false;
        }

        private void SetUpgradeCardSlider()
        {
            upgradeCardSliderText.text = _upgradeCards + " / " + _requiredUpgradeCard;
            upgradeCardSlider.maxValue = _requiredUpgradeCard;
            upgradeCardSlider.value = _upgradeCards;
            
            if (_upgradeCards < _requiredUpgradeCard / 3)
            {
                upgradeCardSliderFillImage.color = Color.red;
            }
            else if( _requiredUpgradeCard / 3 <= _upgradeCards && _upgradeCards <=2 * _requiredUpgradeCard / 3 )
            {
                upgradeCardSliderFillImage.color = Color.yellow;
            }
            else
            {
                upgradeCardSliderFillImage.color = Color.green;
            }
        }

        private void SetInfo()
        {
            nameText.text = name;
            levelText.text = level.ToString();
            priceText.text = coinPrice[level].ToString();
            maxLevel = coinPrice.Count;
            image.sprite = PrefabHolder.Instance.GetCardImage(soldierType);
            _upgradeCards = ShardManager.Instance.GetShardAmount(id);
        }
        
    }
}