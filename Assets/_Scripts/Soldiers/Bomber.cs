using _Scripts.DataHolder;
using _Scripts.Interface;
using _Scripts.Manager;
using _Scripts.Model.ScriptableObjectsScripts;
using DG.Tweening;
using UnityEditor;
using UnityEngine;

namespace _Scripts.Soldiers
{
    public class Bomber : NormalSoldier
    {
        [SerializeField] private float radius;
        [SerializeField] private AudioSource audioSource;
        [SerializeField] private AudioClip movingClip;
        private BoxCollider2D _collider2D;

        protected override void Start()
        {
            base.Start();
            audioSource.loop = true;
            audioSource.clip = movingClip;
            audioSource.Play();
            _collider2D = GetComponent<BoxCollider2D>();
        }

        protected override void See()
        {
            if (!Dead)
            {
                _boxCast = Physics2D.OverlapBoxAll
                    (transform.position, new Vector2(soldierInfo.view_range, 10f), 0f);
                foreach (var obs in _boxCast)
                {
                    if (obs.gameObject.TryGetComponent(out IHittable hit) && hit.side != _side && !hit.Dead)
                    {
                        //Stop Moving
                        //Attack CoolDown
                        _target = hit;
                        print("Bomber Attacked");
                        //GameplaySfxManager.Instance.PlayClip(Soldiers.Bomber,GameplaySfxType.Attack);
                        Attack();
                        Anim.SetBool("Attacking", true);
                        Dead = true;
                        break;
                    }
                }

                if (_target == null || _target.Dead)
                {
                    _target = null;
                    CanMove = true;
                }
                else
                {
                    CanMove = false;
                }

                //Call Move
                Move();
            }
        }

        protected override void Attack()
        {
            // Anim.SetBool("Attacking",true);
            audioSource.Stop();
            var circleCastHit = Physics2D.OverlapCircleAll(transform.position, radius);
            foreach (var obstacle in circleCastHit)
            {
                print("hit: " + obstacle.gameObject.name);
                if (obstacle.TryGetComponent<IHittable>(out var hit) && hit.side != _side)
                {
                    print("Hitted Enemy: " + obstacle.gameObject.name +" "+ hit.side);
                    hit.OnHit(soldierInfo.damage[soldierLevel], soldierInfo.soldier_type);
                    Dead = true;
                    _collider2D.isTrigger = true;
                    hit.GetGameObject().TryGetComponent<NormalSoldier>(out var mammad);
                    mammad.TargetDestroyed();
                    // var canvasGroup = GetComponent<CanvasGroup>();
                    // canvasGroup.alpha = 0;
                }
                else
                {
                    continue;
                }
            }
        }

        protected override void CollectBody()
        {
            Destroy(gameObject);
        }

        public override void Death()
        {
            // Anim.SetBool("Died",true);
            // Anim.SetBool("Attacking",false);
            if (_side == Side.Player1)
            {
                _side = Side.Player2;
            }
            else
            {
                _side = Side.Player1;
            }
            Dead = true;
            var canvasGroup = GetComponent<CanvasGroup>();
            canvasGroup.alpha = 0;
            Instantiate(PrefabHolder.Instance.GetParticle
                (Soldiers.Bomber, ParticleType.Death), transform);
        }
    }
}