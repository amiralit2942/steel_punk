using System.Collections.Generic;
using _Scripts.Manager;
using UnityEngine;

namespace _Scripts.Soldiers
{
    public class Ninja : NormalSoldier
    {
        [SerializeField] private int attackAnimNumbers;
        [SerializeField] private int deathAnimNumbers;
        [SerializeField] private List<AudioClip> attackAudios;
        private int _randomTurn;
        protected override void AttackCoolDownTimer()
        {
            _randomTurn = Random.Range(0, attackAnimNumbers);
            if (Timer > 0)
            {
                Timer -= Time.deltaTime;
                if (Timer <= 0)
                {
                    if (_target != null)
                    {
                        //Attack Animation
                        print("Attack anim called");
                        Anim.SetInteger("AttackTurn",_randomTurn);
                        GameplaySfxManager.Instance.PlayClip(attackAudios[_randomTurn]);
                        Anim.SetBool("Attacking",true);
                        IsAttacking = true;
                        Timer = soldierInfo.attack_cool_down;
                        print("Ninjaaa!!!");
                    }
                    else
                    {
                        CanMove = false;
                        //CallMove Or Not
                    }
                }
            }
        }

        public override void Death()
        {
            var deathTurn = Random.Range(0, deathAnimNumbers);
            GameplaySfxManager.Instance.PlayClip(soldierInfo.soldier_type,GameplaySfxType.Death);
            Anim.SetInteger("DeathTurn",deathTurn);
            base.Death();
        }
    }
}