using System.Collections.Generic;
using System.Runtime.Versioning;
using _Scripts.Controller.Abstract;
using _Scripts.Controller.Card.Deck;
using _Scripts.Controller.Player;
using _Scripts.Controller.Spells;
using _Scripts.DataHolder;
using _Scripts.Interface;
using _Scripts.Manager;
using _Scripts.Model;
using _Scripts.Model.ScriptableObjectsScripts;
using DG.Tweening;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;
using ParticleManager = _Scripts.Model.ParticleManager;

namespace _Scripts.Soldiers
{
    public class NormalSoldier : Soldier, IHittable
    {
        protected Collider2D[] _boxCast;
        protected IHittable _target;
        [SerializeField] private GameObject hitParticleSp;
        [SerializeField] private Transform spellParticleSp;
        [SerializeField] private Transform comicParticleSp;
        [SerializeField] private Animator deathIcon;
        private List<int> comicParticlePerHit = new List<int>();
        [SerializeField] private List<SpriteRenderer> body;
        [SerializeField] private UnityEvent _onFreezeStart;
        [SerializeField] private UnityEvent _onFreezeEnd;
        public bool Dead { get; set; }

        protected virtual void Awake()
        {
            Anim = GetComponent<Animator>();
            Rigidbody2D = GetComponent<Rigidbody2D>();
        }

        protected virtual void Start()
        {
            Timer = soldierInfo.attack_cool_down;
            /*
            print($"{_side} => level :{soldierLevel}" +
                  $"  damage: {soldierInfo.damage[soldierLevel]} health: {soldierInfo.health[soldierLevel]}");
            */
            comicParticlePerHit = ParticleManager.Instance.ComicParticlePerHit;
            CanMove = true;
            Anim.SetBool("IsMoving", CanMove);
        }

        protected virtual void Update()
        {
            See();
        }

        //Animator Event
        protected virtual void Attack()
        {
            if (_target != null)
            {
                print(SoldierType +" Attacked");
                _target.OnHit(soldierInfo.damage[soldierLevel], soldierInfo.soldier_type);
                GameplaySfxManager.Instance.PlayClip(soldierInfo.soldier_type, GameplaySfxType.Attack);
                if (_target.Dead)
                {
                    _target = null;
                }
            }
            else
            {
                CanMove = true;
                //Call Move Or Not
            }
        }

        protected virtual void AttackEnded()
        {
            Anim.SetBool("Attacking", false);
            Timer = soldierInfo.attack_cool_down;
            IsAttacking = false;
        }

        protected virtual void See()
        {
            if (Dead)
                return;
            
            _boxCast = Physics2D.OverlapBoxAll
                (transform.position, new Vector2(soldierInfo.view_range, 10f), 0f);
            foreach (var obs in _boxCast)
            {
                if (obs.gameObject.TryGetComponent(out IHittable hit) && hit.side != _side && !hit.Dead)
                {
                    //Stop Moving
                    //Attack CoolDown
                    _target = hit;
                    AttackCoolDownTimer();
                    break;
                }
            }

            if (_target != null && soldierInfo.soldier_type != Soldiers.Bomber)
            {
                //print("Target: ain't null");

                if (!_target.Dead && soldierInfo.soldier_type != Soldiers.Bomber)
                {
                    //print("Target: ain't dead" + _target.side);
                }
            }


            if (_target == null || _target.Dead)
            {
                _target = null;
                CanMove = true;
            }
            else
            {
                CanMove = false;
            }

            //Call Move
            Move();
        }

        protected float Timer;
        protected Soldier.Side _side;
        protected bool IsAttacking = false;

        protected virtual void AttackCoolDownTimer()
        {
            if (Timer > 0)
            {
                Timer -= Time.deltaTime;
                if (Timer <= 0)
                {
                    if (_target != null)
                    {
                        //Attack Animation
                        //print($"{_side} Attacked");
                        Anim.SetBool("Attacking", true);
                        IsAttacking = true;
                    }
                    else
                    {
                        //Test
                        CanMove = true;
                        //CallMove Or Not
                    }
                }
            }
            // else if (!IsAttacking && Timer <= 0)
            // {
            //     Timer = soldierInfo.spawn_cool_down;    
            // }
        }

        public void TargetDestroyed()
        {
            _target = null;
        }

        protected virtual void Move()
        {
            if (!Dead && !IsAttacking)
            {
                Anim.SetBool("IsMoving", CanMove);
                if (CanMove)
                {
                    print(SoldierType+" Moving");
                    Rigidbody2D.velocity = new Vector2(1, 0) * Speed * (int)_side;
                }
                else
                {
                    print(SoldierType+" Stopped Moving");
                    Rigidbody2D.velocity = Vector2.zero;
                }
            }
        }

        public virtual void Death()
        {
            print(SoldierType+" DEAD!");
            Speed = 0;
            //Rigidbody2D.velocity = new Vector2();
            Anim.SetBool("Died", true);
            Anim.SetBool("IsMoving", false);
            Anim.SetBool("Attacking", false);
            deathIcon.gameObject.SetActive(true);
            deathIcon.Play("DeathSkull");
            GameplaySfxManager.Instance.PlayClip(soldierInfo.soldier_type, GameplaySfxType.Death);
            Dead = true;
        }

        protected virtual void CollectBody()
        {
            //print("Void");
            Instantiate(ParticleManager.Instance.GetComicParticle(), comicParticleSp);
            //Anim.SetBool("Died",false);
            var t = 0f;
            DOTween.To(() => t, (v) => t = v, 0, 5).OnComplete(() =>
            {
                //print("DoTween");
                var image = gameObject.GetComponent<CanvasGroup>();
                image.DOFade(0, 2).OnComplete(() =>
                {
                    //print("Done");
                    Destroy(gameObject);
                });
            });
        }

        public virtual void SetSoldierInfo(CardsInfo cardsInfo)
        {
            soldierInfo = cardsInfo;
            //GameManager.Instance.soldierInfos.Find(soldier => soldier.soldier_type == SoldierType);
            Name = soldierInfo.name;
            soldierLevel = SafeBox.Instance.userInfo.cards.Find(x => x.card_id == soldierInfo.ID).level;
            Health = soldierInfo.health[soldierLevel - 1];
            // //_maxHealth = SoldierInfo.health[SoldierLevel - 1];
            // Damage = SoldierInfo.damage[SoldierLevel - 1];
            // SpawnCoolDown = SoldierInfo.spawn_cool_down;
            // AttackCoolDown = SoldierInfo.attack_cool_down;
            // RequiredMana = SoldierInfo.required_mana;
            // ViewRange = SoldierInfo.view_range;
            Speed = soldierInfo.speed;
            Timer = soldierInfo.attack_cool_down;
            Dead = false;
        }

        public virtual void SetHealth(int newHealth)
        {
            Health = newHealth;
        }

        public virtual void SetSide(Side soldierSide)
        {
            _side = soldierSide;
        }

        private void OnDrawGizmos()
        {
            Gizmos.DrawWireCube(transform.position, new Vector2(soldierInfo.view_range, 10));
        }

        Soldier.Side IHittable.side => _side;


        public void OnHit(int damage, Soldier.Soldiers attackingSoldier)
        {
            if (attackingSoldier != Soldiers.Wizard)
            {
                GetRandomComicParticle();
                GetHitParticle(attackingSoldier);
            }

            if (Health > damage)
            {
                SetHealth(Health - damage);
                //HealthTextAnim(damage);
            }
            else
            {
                Death();
                GameManager.Instance.SoldierDied(this, false);
            }
        }

        public GameObject GetGameObject()
        {
            return gameObject;
        }

        private int _remainingHit;

        private void GetRandomComicParticle()
        {
            if (_remainingHit == 0)
            {
                Instantiate(ParticleManager.Instance.GetComicParticle(), comicParticleSp);
                _remainingHit = comicParticlePerHit[Random.Range(0, comicParticlePerHit.Count)];
                return;
            }

            _remainingHit--;
        }

        protected void GetHitParticle(Soldiers attackingSoldier)
        {
            Instantiate(ParticleManager.Instance
                .GetSoldierParticle(attackingSoldier, ParticleType.Hit), hitParticleSp.transform);
        }

        public void Heal(int hp)
        {
            if (!Dead)
            {
                Health += hp;
                Instantiate(ParticleManager.Instance.GetSpellParticle(Spell.SpellType.HealSpell, 3), spellParticleSp);
            }
        }

        public void SpeedUp(float boostSpeed, float duration)
        {
            if (!Dead)
            {
                Instantiate(ParticleManager.Instance.GetSpellParticle(Spell.SpellType.SpeedSpell, duration),
                    spellParticleSp);
                Speed += boostSpeed;
                Anim.speed += boostSpeed / Speed;
                var t = 0f;
                DOTween.To(() => t, (v) => t = v, 10, duration).OnComplete(() =>
                {
                    Speed -= boostSpeed;
                    Anim.speed = 1;
                });
            }
        }


        public void Freeze(float duration)
        {
            if (!Dead)
            {
                Instantiate(ParticleManager.Instance.GetSpellParticle(Spell.SpellType.FreezeSpell, duration),
                    spellParticleSp);
                _onFreezeStart?.Invoke();
                var oldSpeed = Speed;
                //
                var animSpeed = 1f;
                //

                DOTween.To(() => animSpeed, v => animSpeed = v, 0, 1.5f).OnUpdate(() =>
                {
                    foreach (var image in body)
                    {
                        image.DOColor(GameManager.Instance.FreezeColor, 1.5f);
                    }

                    Anim.speed = animSpeed;
                    Speed = oldSpeed * animSpeed;
                }).OnComplete(() =>
                {
                    var t = 0f;
                    DOTween.To(() => t, (v) => t = v, 1, duration).OnComplete(() =>
                    {
                        _onFreezeEnd?.Invoke();
                        DOTween.To(() => animSpeed, v => animSpeed = v, 1, 1.5f).OnUpdate(() =>
                        {
                            foreach (var image in body)
                            {
                                image.DOColor(Color.white, 1.5f);
                            }

                            Anim.speed = animSpeed;
                            Speed = oldSpeed * animSpeed;
                        }).OnComplete(() =>
                        {
                            Speed = oldSpeed;
                            Anim.speed = 1;
                        });
                    });
                });
            }
        }

        public void ShieldSpell(float duration, int shieldAmount)
        {
            Health += shieldAmount;
            var t = 0;
            DOTween.To(() => t, (v) => t = v, 1, duration).OnComplete(() => { Health -= shieldAmount; });
        }
    }
}