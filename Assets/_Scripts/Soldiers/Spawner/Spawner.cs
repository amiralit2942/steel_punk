using _Scripts.Manager;
using UnityEngine;

namespace _Scripts.Soldiers.Spawner
{
    public class Spawner : NormalSoldier
    {
        [SerializeField] private SpawnerDrone spawnerDrone;
        [SerializeField] private Transform drownSpawnPoint;
        [SerializeField] private AudioClip spawnDroneClip;
        protected override void Attack()
        {
            //TODO: Play SFX
            GameplaySfxManager.Instance.PlayClip(spawnDroneClip);
            var drone = Instantiate(spawnerDrone, drownSpawnPoint);
            drone.SetInfo(_target,soldierInfo,_side,soldierLevel,this);

        }
    }
}
