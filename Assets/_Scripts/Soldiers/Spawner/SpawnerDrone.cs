using System;
using _Scripts.Controller.Abstract;
using _Scripts.DataHolder;
using _Scripts.Interface;
using _Scripts.Manager;
using _Scripts.Model;
using _Scripts.Model.ScriptableObjectsScripts;
using AI.Utils;
using DG.Tweening;
using UnityEngine;
using UnityEngine.UI;

namespace _Scripts.Soldiers.Spawner
{
    public class SpawnerDrone : MonoBehaviour
    {
        [SerializeField] private float attackDur;
        [SerializeField] private float radius;
        [SerializeField] private Animator animator;
        [SerializeField] private Transform comicParticleSp;
        [SerializeField] private SpriteRenderer image;
        [SerializeField] private AudioSource audioSource;
        [SerializeField] private AudioClip movingClip;
        private Spawner _spawner;
        private IHittable target;
        private CardsInfo _soldierInfo;
        private Soldier.Side _side;
        private int _soldierLevel;

        public void SetInfo(IHittable target, CardsInfo soldierInfo, Soldier.Side side,int soldierLevel,Spawner spawner)
        {
            this.target = target;
            _soldierInfo = soldierInfo;
            _side = side;
            _soldierLevel = soldierLevel;
            _spawner = spawner;
        }

        private void Start()
        {
            //animator.Play("DroneSpawnMove");
            animator.SetBool("Attack",true);
            audioSource.loop = true;
            audioSource.clip = movingClip;
            audioSource.Play();
        }

        public async void Attack()
        {
            UnityThread.initUnityThread();
            animator.enabled = false;
            await transform.DOMove(target.GetGameObject().transform.position, attackDur).AsyncWaitForCompletion();
            print(target.GetGameObject().transform.position);
            var color = image.color;
            audioSource.Stop();
            GameplaySfxManager.Instance.PlayClip(Soldier.Soldiers.Wizard,GameplaySfxType.Attack);
            color = new Color(color.r, color.g, color.b, 0);
            image.color = color;
            var go1 = Instantiate(ParticleManager.Instance.GetComicParticle(), comicParticleSp);
            print("TASK 1 COMPLETED " + go1.name);
            var go2 = Instantiate(ParticleManager.Instance.GetSoldierParticle
                (Soldier.Soldiers.Bomber, ParticleType.Death), transform);
            print("TASK 2 COMPLETED " + go2.name);
            var circleCast = Physics2D.OverlapCircleAll(transform.position, radius);
            foreach (var soldier in circleCast)
            {
                if (soldier.TryGetComponent<IHittable>(out var hit) && hit.side != _side)
                {
                    hit.OnHit(_soldierInfo.damage[_soldierLevel], Soldier.Soldiers.Wizard);
                }
            }
            
            Destroy(gameObject,2);


        }
    }
}