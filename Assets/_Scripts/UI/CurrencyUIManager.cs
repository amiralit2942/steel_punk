﻿using _Scripts.Manager;
using _Scripts.Manager.Currencies;
using DG.Tweening;
using TMPro;
using UnityEngine;
using UnityEngine.UI;
using Util;

namespace _Scripts.UI
{
    public class CurrencyUIManager : MonoBehaviourSingleton<CurrencyUIManager>
    {
        [SerializeField] private TMP_Text coinText;
        [SerializeField] private TMP_Text gemText;
        [SerializeField] private TMP_Text levelText;
        [SerializeField] private Image levelSlider;
        

        public void UpdateUI()
        {
            coinText.text = CoinManager.Instance.GetCoin().ToString();
            gemText.text = GemManager.Instance.GetGem().ToString();
            levelText.text = SafeBox.Instance.userInfo.level.ToString();
            SetLevelSlider();
            
            //TODO: ADD NEXT LEVEL XP
            //levelSlider.minValue = XpManager.Instance.GetNextLevelXp() / 2;
        }

        public void DisableUI()
        {
            coinText.gameObject.SetActive(false);
            gemText.gameObject.SetActive(false);
            levelSlider.gameObject.SetActive(false);
        }

        public void ActiveCoinUI()
        {
            DisableUI();
            coinText.gameObject.SetActive(true);
        }

        public void ActiveGemUI()
        {
            DisableUI();
            gemText.gameObject.SetActive(true);
        }
        
        public void SetLevelSlider()
        {
            var xp = SafeBox.Instance.userInfo.xp;
            var nextLevelXp = SafeBox.Instance.userInfo.level * 500 + 1;
            levelSlider.DOFillAmount((float)xp / nextLevelXp, 0.2f);
        }
    }
}