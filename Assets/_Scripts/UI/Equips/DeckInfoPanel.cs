﻿using System;
using _Scripts.Controller.Spells;
using _Scripts.DataHolder;
using _Scripts.Manager;
using _Scripts.Model;
using _Scripts.UI.NoticePopUp;
using _Scripts.Util;
using TMPro;
using UnityEngine;
using UnityEngine.SocialPlatforms;
using UnityEngine.UI;
using Util;

namespace _Scripts.UI.Equips
{
    public class DeckInfoPanel : MonoBehaviourSingleton<DeckInfoPanel>
    {
        [SerializeField] private Image soldierImage;
        [SerializeField] private Image shardSlider;
        [SerializeField] private TMP_Text shardText;
        [SerializeField] private GameObject infoPanel;
        [SerializeField] private TMP_Text firstInfoText;
        [SerializeField] private TMP_Text firstAmountText;
        [SerializeField] private TMP_Text secInfoText;
        [SerializeField] private TMP_Text secAmountText;
        [SerializeField] private TMP_Text thirdInfoText;
        [SerializeField] private TMP_Text thirdAmountText;
        [SerializeField] private TMP_Text fourthInfoText;
        [SerializeField] private TMP_Text fourthAmountText;
        [SerializeField] private Button upgradeButton;
        private void Start()
        {
            infoPanel.SetActive(false);
            thirdInfoText.gameObject.SetActive(false);
        }

        private void OnEnable()
        {
            Localization.OnChangeLanguage += DeActivePanel;
        }

        private void OnDisable()
        {
            Localization.OnChangeLanguage -= DeActivePanel;
        }

        public void ActivePanel(UserCard soldier,Action upgrade)
        {
            // var cardInfo = CardsManager.Instance.GETSoldierInfo(soldier.card_id);
            // var level = soldier.level;
            infoPanel.SetActive(true);
            ExtraFieldsActivation(true);
            var info = CardsManager.Instance.GETSoldierInfo(soldier.card_id);
            var level = soldier.level;
            firstInfoText.text = Localization.Get("level");
            firstAmountText.text = level.ToString();
            secInfoText.text =  Localization.Get("health");
            secAmountText.text = info.health[level].ToString();
            thirdInfoText.text = Localization.Get("damage");
            thirdAmountText.text = info.damage[level].ToString();
            fourthInfoText.text = Localization.Get("spawnTime");
            fourthAmountText.text = info.spawn_cool_down.ToString(); 
            soldierImage.sprite = PrefabHolder.Instance.GetCardImage(info.soldier_type);
            var shard = soldier.shards;
            print("UserCard: " + soldier.card_id + " " +soldier.level+ " " + soldier.shards);
            print("Soldier: " + info.soldier_type + "Shard: " + soldier.shards);
            var requiredShard = info.required_shard[level];
            shardSlider.fillAmount = shard/ requiredShard;
            shardText.text = shard + " / " + requiredShard;
            if (shard >= requiredShard)
            {
                upgradeButton.interactable = true;
                upgradeButton.onClick.RemoveAllListeners();
                upgradeButton.onClick.AddListener(() =>
                {
                    NoticePopUpPanel.Instance.NewNotice().WithContent(Strings.Upgrade).WithAcceptButton(()=>
                    {
                        NoticePopUpPanel.Instance.DeActive();
                        upgrade();
                    }).WithRejectButton();
                });
            }
            else
            {
                upgradeButton.interactable = false;
            }
            
        }

        public void ActivePanel(UserSpell spell)
        {
            infoPanel.SetActive(true);
            ExtraFieldsActivation(false);
            var info = SafeBox.Instance.spellsInfo?.Find
                (x => x.id == spell.spellId);
            firstInfoText.text = Localization.Get("affectionDur");
            firstAmountText.text = info.duration.ToString();
            secInfoText.text = Localization.Get("power");
            secAmountText.text = info.power.ToString();
            var cardInfo = CardsManager.Instance.GETSpellInfo(spell);
            soldierImage.sprite = PrefabHolder.Instance.GetSpellImageHolderAsset(cardInfo.spell_type);
            if (info.spell_type == Spell.SpellType.HealSpell)
            {
                
            }
        }

        public void ExtraFieldsActivation(bool setActive)
        {
            thirdInfoText.gameObject.SetActive(setActive);
            thirdAmountText.gameObject.SetActive(setActive);
            fourthInfoText.gameObject.SetActive(setActive);
            fourthAmountText.gameObject.SetActive(setActive);
            shardSlider.gameObject.SetActive(setActive);
            shardText.gameObject.SetActive(setActive);
            upgradeButton.gameObject.SetActive(setActive);
        }

        public void DeActivePanel()
        {
            infoPanel.SetActive(false);
        }

        public void UpgradeButton()
        {
            
        }

        
        
    }
}