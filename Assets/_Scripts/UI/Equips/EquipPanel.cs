﻿using System.Collections.Generic;
using System.Linq;
using _Scripts.Controller.Abstract;
using _Scripts.Controller.Card;
using _Scripts.Controller.Card.Deck;
using _Scripts.DataHolder;
using _Scripts.Manager;
using _Scripts.Model;
using Unity.Collections;
using UnityEngine;
using Util;

namespace _Scripts.UI.Equips
{
    
    // FUCKIN OLD!!!!
    public class EquipPanel : MonoBehaviourSingleton<EquipPanel>
    {
        [SerializeField] private RectTransform equipedSoldiersPanel;
        public RectTransform EquipedSoldiersPanel => equipedSoldiersPanel;
        
        [SerializeField] private RectTransform ownedSoldiersPanel;
        public RectTransform OwnedSoldiersPanel => ownedSoldiersPanel;

        private List<UserCard> _deck;

        void Start()
        {
            _deck = new List<UserCard>(5);
            foreach (var soldier in SafeBox.Instance.ownedCards)
            {
                if (soldier.soldier_type != Soldier.Soldiers.Base)
                {
                    //We Spawn Owned Cards List In UI Here
                    if (SafeBox.Instance.deck.Find(x => x.soldier_type == soldier.soldier_type) == null)
                    {
                        var card = Instantiate(PrefabHolder.Instance.GetSoldierDeckCard(), ownedSoldiersPanel);
                        //card.SetInfo(soldier);
                    }
                    else
                    {
                        var card = Instantiate(PrefabHolder.Instance.GetSoldierDeckCard(), equipedSoldiersPanel);
                        //card.SetInfo(soldier);
                    }
                    // card.SetCardInfo(UserInfo.Instance.cards.Find(x => x.soldierType == soldier.soldierType));
                }
            }
        }
        
        
        public void CheckDeckCapacity()
        {
            if (_deck.Count == _deck.Capacity)
            {
                MoveCard(equipedSoldiersPanel.GetChild(0));
                var removedCard = equipedSoldiersPanel.GetChild(0).GetComponent<SoldierDeckCard>();
                _deck?.Remove(removedCard.UserCard);
            }
        }

        public void AddToDeck(Transform deckCardTransform)
        {
            var deckCard = deckCardTransform.GetComponent<SoldierDeckCard>();
            CheckDeckCapacity();
            if (!SafeBox.Instance.userInfo.deck_cards.Contains(deckCard.UserCard))
            {
                SafeBox.Instance.userInfo.deck_cards.Add(deckCard.UserCard);
                SafeBox.Instance.deck.Add(deckCard.CardsInfo);
                _deck.Add(deckCard.UserCard);
            }
        }

        public void RemoveFromDeck(Transform deckCardTransform)
        {
            var deckCard = deckCardTransform.GetComponent<SoldierDeckCard>();   
            if (SafeBox.Instance.userInfo.deck_cards.Count == 1)
            {
                MoveCard(deckCardTransform);
                return;
            }

            SafeBox.Instance.userInfo.deck_cards?.Remove(deckCard.UserCard);
            SafeBox.Instance.deck?.Remove(deckCard.CardsInfo);
        }
        
        
        public List<CardsInfo> GetEquipedSoldiers()
        {
            var cards = SafeBox.Instance.ownedCards;
            var deck = SafeBox.Instance.deck;
            
            if ( SafeBox.Instance.deck.Count < 3)
            {
                for (int i = 0; i < cards.Count; i++)
                {
                    deck.Add(cards[i]);
                    if (i >= 5)
                    {
                        break;
                    }
                }
            }
            // Post Player Deck
            return deck;
        }
        
        
        public void MoveCard(Transform cardTransform)
        {
            var card = cardTransform.gameObject.GetComponent<SoldierDeckCard>();
            var userCard = card.UserCard;
            var cardInfo = card.CardsInfo;
            
            if (cardTransform.IsChildOf(ownedSoldiersPanel))
            {
                cardTransform.parent = equipedSoldiersPanel.transform;
                var addedCard = SafeBox.Instance.userInfo.deck_cards.Find(x => x.card_id == userCard.card_id);
                SafeBox.Instance.userInfo.deck_cards.Add(addedCard);
                SafeBox.Instance.deck.Add(cardInfo);
            }
            else if (cardTransform.IsChildOf(equipedSoldiersPanel))
            {
                cardTransform.parent = ownedSoldiersPanel.transform;
                var removedCard = SafeBox.Instance.userInfo.deck_cards.Find(x => x.card_id == userCard.card_id);//Remove(userCard);
                SafeBox.Instance.userInfo.deck_cards.Remove(removedCard);
                SafeBox.Instance.deck.Remove(cardInfo);
            }
            
            ServerManager.Instance.PostDeck(SafeBox.Instance.userInfo.deck_cards,(response) =>
            {
                print("Deck Change : " + response);
                print("Post Deck Succeed");
            }, (response) =>
            {
                print("Post Deck Failed");
            });
        }


        private void ChangeDeck(CardsInfo cardsInfo)
        {
            var userCard = SafeBox.Instance.userInfo.cards.Find(x => x.card_id == cardsInfo.ID);
            if (SafeBox.Instance.deck.Contains(cardsInfo))
            {
                SafeBox.Instance.deck.Remove(cardsInfo);
                SafeBox.Instance.userInfo.deck_cards.Remove(userCard);
            }
            else
            {
                SafeBox.Instance.deck.Add(cardsInfo);
                SafeBox.Instance.userInfo.deck_cards.Add(userCard);
            }
        }
        
    }
}