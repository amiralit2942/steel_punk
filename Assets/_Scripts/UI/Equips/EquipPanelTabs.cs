﻿using UnityEngine;
using UnityEngine.UI;

namespace _Scripts.UI
{
    public class EquipPanelTabs : MonoBehaviour
    {
        [SerializeField] private GameObject unitPanel;
        [SerializeField] private GameObject gunsPanel;
        [SerializeField] private GameObject spellsPanel;
        [SerializeField] private Button unitBtn;
        [SerializeField] private Button gunsBtn;
        [SerializeField] private Button spellsBtn;

        public void DisablePanels()
        {
            unitPanel.SetActive(false);
            gunsPanel.SetActive(false);
            spellsPanel.SetActive(false);
            unitBtn.interactable = true;
            gunsBtn.interactable = true;
            spellsBtn.interactable = true;
        }

        public void UnitBtn()
        {
            DisablePanels();
            unitPanel.SetActive(true);
            unitBtn.interactable = false;
        }
        
        public void GunsBtn()
        {
            DisablePanels();
            gunsPanel.SetActive(true);
            gunsBtn.interactable = false;
        }
        
        public void SpellsBtn()
        {
            DisablePanels();
            spellsPanel.SetActive(true);
            spellsBtn.interactable = false;
        }
        
    }
}
