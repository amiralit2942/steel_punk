﻿using System;
using System.Collections.Generic;
using _Scripts.Controller.Spells;
using _Scripts.DataHolder;
using _Scripts.Manager;
using _Scripts.Model;
using UnityEngine;
using Util;

namespace _Scripts.UI.Equips
{
    public class SpellEquip : MonoBehaviourSingleton<SpellEquip>
    {    //OLD!!!
        public RectTransform ownedPanel;
        public RectTransform equipedPanel;
        private List<UserSpell> _spells;
        private void Start()
        {
            _spells = new List<UserSpell>(2);
            // foreach (var spell in SafeBox.Instance.userInfo.userSpell)
            // {
            //     var spellCard = Instantiate(PrefabHolder.Instance.GetSpellDeckCard(), ownedPanel);
            //     var spellInfo = SpellsManager.Instance.GetSpellInfo(spell.id);
            //     spellCard.SetInfo(spellInfo.spellType,spellInfo.duration,spellInfo.level);
            // }
        }

        public void AddToDeck(Transform spellDeckCard)
        {
            var userSpell = spellDeckCard.GetComponent<SpellDeckCard>().UserSpell;
            CheckDeckCapacity();
            if (!SafeBox.Instance.userInfo.spells.Contains(userSpell))
            {
                SafeBox.Instance.userInfo.spells.Add(userSpell);
                print("Spell Added");
            }
        }

        public void RemoveFromDeck(Transform spellDeckCard)
        {
            var userSpell = spellDeckCard.GetComponent<SpellDeckCard>().UserSpell;
            SafeBox.Instance.userInfo.spells?.Remove(userSpell);
            print("Spell Removed");
        }
        
        public void CheckDeckCapacity()
        {
            if (_spells.Count == _spells.Capacity)
            {
                MoveCard(equipedPanel.GetChild(0).transform);   
            }
        }

        public void MoveCard(Transform spellCard)
        {
            var spellDeckCard = spellCard.GetComponent<SpellDeckCard>();
            var spellInfo = spellDeckCard.SpellInfo;
            var userSpell = spellDeckCard.UserSpell;
            if (spellCard.IsChildOf(equipedPanel))
            {
                spellCard.parent = ownedPanel;
                SafeBox.Instance.userInfo.spells?.Remove(userSpell);
            }
            else
            {
                spellCard.parent = equipedPanel;
                SafeBox.Instance.userInfo.spells.Add(userSpell);
            }
        }
    }
}