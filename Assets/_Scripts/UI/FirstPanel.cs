﻿using System;
using Manager;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace UI
{
    public class FirstPanel : MonoBehaviour
    {
        [SerializeField] private GameObject firstPanel;
        [SerializeField] private GameObject acceptExitPanel;
        [SerializeField] public GameObject mainMenuPanel;
        
        //[SerializeField] private GameObject settingPanel;
    
        // Start is called before the first frame update
        void Start()
        {
            acceptExitPanel.SetActive(false);
        }

        // Update is called once per frame
        void Update()
        {
            if (Input.GetKey(KeyCode.Escape))
            {
                acceptExitPanel.SetActive(true);
            }
        }

        public void OnStartClick()
        {
            firstPanel.SetActive(false);
            mainMenuPanel.SetActive(true);
        }

        public void OnSettingClick()
        {
            firstPanel.SetActive(false);
           // settingPanel.SetActive(true);
        }

        public void AcceptExit()
        {
            Application.Quit();
        }

        public void DenyExit()
        {
            acceptExitPanel.SetActive(false);
        }

        public void OnExitClick()
        {
            acceptExitPanel.SetActive(true);
        }
        
    
    }
}
