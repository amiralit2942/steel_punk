﻿using _Scripts.Manager;
using _Scripts.Model;
using _Scripts.UI.NoticePopUp;
using _Scripts.Util;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using Util;

namespace _Scripts.UI
{
    public class GamePlaySceneUi : MonoBehaviourSingleton<GamePlaySceneUi>
    {
        [SerializeField] private GameObject pausePanel;
        [SerializeField] private GameObject settingPanel;
        [SerializeField] private GameObject acceptExitPanel;
        [SerializeField] private Button pauseBtn;
        [SerializeField] private GameObject buttonsPanel;
        [SerializeField] private GameObject gameOverPanel;
        [SerializeField] private GameObject winPic;
        [SerializeField] private GameObject losePic;
        
        private int scene = 0;

        // Start is called before the first frame update
        void Start()
        {
            pausePanel.SetActive(false);
            //settingPanel.SetActive(false);
            buttonsPanel.SetActive(false);
            DisableBlackPanel();
            DisableGameOverPanel();
        }

        // Update is called once per frame
        void Update()
        {
            if (Input.GetKeyDown(KeyCode.Escape))
            {
                OnBackClick();
            }
        }

        public void OnPauseClick()
        {
            pausePanel.SetActive(true);
            buttonsPanel.SetActive(true);
            pauseBtn.interactable = false;
            scene = 1;
        }

        public void OnResumeClick()
        {
            pauseBtn.interactable = true;
            pausePanel.SetActive(false);
            scene = 0;
        }

        public void OnSettingClick()
        {
            settingPanel.SetActive(true);
            buttonsPanel.SetActive(false);
            scene = 2;
        }

        public void OnExitClick()
        {
            //acceptExitPanel.SetActive(true);
            NoticePopUpPanel.Instance.NewNotice().WithContent(Strings.ExitGame).WithAcceptButton(() =>
            {
                SafeBox.Instance.gameState = GameState.InMenu;
                AcceptExit();
            }).WithRejectButton();
            // LoadingPanel.Instance.FakeLoading(3);
            //scene = 3;
        }

        public void AcceptExit()
        {
            SceneManager.LoadScene(1);
            scene = 0;
        }

        public void DenyExit()
        {
            acceptExitPanel.SetActive(false);
            scene = 1;
        }

        public void OnBackClick()
        {
            switch (scene)
            {
                case 0:
                    OnPauseClick();
                    break;
                case 1:
                    OnExitClick();
                    break;
                case 2:
                    OnPauseClick();
                    break;
                case 3:
                    OnPauseClick();
                    acceptExitPanel.SetActive(false);
                    break;
            }
            settingPanel.SetActive(false);
        }

        public void GameOver(bool win)
        {
            gameOverPanel.SetActive(true);
            if (win)
            {
                winPic.SetActive(true);
            }
            else
            {
                losePic.SetActive(true);
            }
        }

        public void DisableBlackPanel()
        {
            gameOverPanel.SetActive(false);
        }
        

        public void DisableGameOverPanel()
        {
            winPic.SetActive(false);
            losePic.SetActive(false);
        }
    }
}