﻿using System;
using System.Collections;
using DG.Tweening;
using TMPro;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using Util;

namespace _Scripts.UI
{
    public class LoadingPanel : MonoBehaviourSingleton<LoadingPanel>
    {
        [SerializeField] private GameObject loadingPanel;
        [SerializeField] private Image loadingImage;
        [SerializeField] private TMP_Text loadingText;

        private void Awake()
        {
            DontDestroyOnLoad(this);
        }


        private void Start()
        {
            //loadingPanel.transform.position = new Vector3(0,-1080,0);
            loadingPanel.SetActive(false);
            loadingImage.fillAmount = 0;
        }


        public void ChangeScene(int sceneIndex)
        {
            loadingPanel.SetActive(true);
            loadingPanel.transform.DOComplete();
            loadingPanel.transform.DOMoveY(0, 0, true).OnComplete(() =>
            {
                StartCoroutine(LoadAsync(sceneIndex));
            });
        }

        IEnumerator LoadAsync(int sceneIndex)
        {
            AsyncOperation operation = SceneManager.LoadSceneAsync(sceneIndex);

            while (!operation.isDone)
            {
                float progress = operation.progress;
                loadingImage.fillAmount = progress;
                loadingText.text =  "%" + progress * 100;
                //loadingImage.DOComplete();
                //loadingImage.DOFillAmount(progress, 0.5f);

                yield return null;
            }
        }

        public void FakeLoading(int index)
        {
            loadingImage.fillAmount = 0;
            loadingPanel.SetActive(true);
            loadingImage.DOComplete();
            loadingImage.DOFillAmount(1, 3).OnComplete(() =>
            {
                SceneManager.LoadScene(index);
            });
        }
    }
}
