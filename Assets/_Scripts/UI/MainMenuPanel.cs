﻿using UnityEngine;

namespace UI
{
    public class MainMenuPanel : MonoBehaviour
    {
        [SerializeField] private GameObject mainMenuPanel;
        [SerializeField] private GameObject firstPanel;
        [SerializeField] private GameObject equipPanel;
        [SerializeField] private GameObject upgradePanel;
        

        // Start is called before the first frame update
        void Start()
        {
            mainMenuPanel.SetActive(false);
        }

        // Update is called once per frame
        void Update()
        {
            if (Input.GetKey(KeyCode.Escape))
            {
                mainMenuPanel.SetActive(false);
                firstPanel.SetActive(true);
            }
        }

        public void OnPlayClick()
        {
            
        }

        public void OnEquipClick()
        {
            mainMenuPanel.SetActive(false);
            equipPanel.SetActive(true);
        }
        
        public void OnUpgradeClick()
        {
            mainMenuPanel.SetActive(false);
            upgradePanel.SetActive(true);
        }
        
        public void OnBackClick()
        {
            mainMenuPanel.SetActive(false);
            firstPanel.SetActive(true);
        }
    }
}
