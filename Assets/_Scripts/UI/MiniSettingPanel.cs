using System;
using DG.Tweening;
using ir.metrix.unity;
using Newtonsoft.Json;
using UnityEditor;
using UnityEngine;
using UnityEngine.UI;
using Util;

namespace _Scripts.UI
{
    public class MiniSettingPanel : MonoBehaviourSingleton<MiniSettingPanel>
    {
        [SerializeField] private RectTransform settingPanel;
        [SerializeField] private RectTransform settingButton;
        [SerializeField] private AudioSource music;
        //[SerializeField] private AudioSource volume;
        // [SerializeField] private Image volumeButtonImage;
        // [SerializeField] private Image musicButtonImage;
        [SerializeField] private Image volumeOffImage;
        [SerializeField] private Image musicOffImage;
        [SerializeField] private Vector2 openSize;
        [SerializeField] private float duration;
        private bool _isOpen = false;

        private void Start()
        {
            CheckPlayerPrefs("Music");
            _musicIsOn = bool.Parse(PlayerPrefs.GetString("Music"));
            CheckPlayerPrefs("Volume");
            _volumeIsOn = bool.Parse(PlayerPrefs.GetString("Volume"));
            volumeOffImage.gameObject.SetActive(!_volumeIsOn);
            musicOffImage.gameObject.SetActive(!_musicIsOn);

            music.mute = !_musicIsOn;
            _isOpen = false;
        }

        public void OnClick()
        {
            if (_isOpen)
            {
                //Close
                settingPanel.DOComplete();
                settingPanel.DOSizeDelta(new Vector2(openSize.x, 0), duration).OnUpdate(() =>
                {
                    settingButton.DORotate(new Vector3(0, 0, 0), duration);
                });
                _isOpen = false;
                return;
            }
            //Open
            settingPanel.DOComplete();
            settingPanel.DOSizeDelta(openSize, duration).OnUpdate(() =>
            {
                settingButton.DORotate(new Vector3(0, 0, -90), duration);
            });
            _isOpen = true;
        }

        public void Close()
        {
            if (_isOpen)
            {
                OnClick();
            }
        }

        private bool _volumeIsOn;
        public void VolumeButtonClick()
        {
            //TODO: Do the stuff with the mixer
            if (_volumeIsOn)
            {
                //Mute
                volumeOffImage.gameObject.SetActive(true);
                //volumeButtonImage.sprite = volumeOffImage;
                _volumeIsOn = false;
                //volume.mute = true;
                PlayerPrefs.SetString("Volume",_volumeIsOn.ToString());
                return;
            }
            //UnMute
            _volumeIsOn = true;
            //volume.mute = false;
            PlayerPrefs.SetString("Volume",_volumeIsOn.ToString());
            //volumeButtonImage.sprite = volumeOnImage;
            volumeOffImage.gameObject.SetActive(false);
        }

        private bool _musicIsOn;
        public void MusicButtonClick()
        {
            //TODO: Do the stuff with the mixer
            if (_musicIsOn)
            {
                //Mute
                _musicIsOn = false;
                music.mute = true;
                PlayerPrefs.SetString("Music",_musicIsOn.ToString());
                //musicButtonImage.sprite = musicOffImage;
                musicOffImage.gameObject.SetActive(true);
                return;
            }
            //UnMute
            _musicIsOn = true;
            music.mute = false;
            PlayerPrefs.SetString("Music",_musicIsOn.ToString());
            //musicButtonImage.sprite = musicOnImage;
            musicOffImage.gameObject.SetActive(false);
        }

        public void SetButtons()
        {
            if (_volumeIsOn)
            {
                //volumeButtonImage.sprite = volumeOnImage;
                volumeOffImage.gameObject.SetActive(false);
            }
            else
            {
                //volumeButtonImage.sprite = volumeOffImage;
                volumeOffImage.gameObject.SetActive(true);
            }

            if (_musicIsOn)
            {
                //musicButtonImage.sprite = musicOnImage;
                volumeOffImage.gameObject.SetActive(false);
                print("Unmute : " + _musicIsOn);
            }
            else
            {
                //musicButtonImage.sprite = musicOffImage;
                volumeOffImage.gameObject.SetActive(true);
            }

            music.mute = !_musicIsOn;
            //TODO: Add Volume
        }

        public bool CastString(string String)
        {
            if (String == "true")
            {
                return true;
            }
            if(String == "false")
            {
                return false;    
            }

            return false;
        }

        public void CheckPlayerPrefs(string key)
        {
            if (PlayerPrefs.HasKey(key))
            {
               bool.Parse(PlayerPrefs.GetString(key));
            }
            else
            {
                PlayerPrefs.SetString(key,"true");
            }
        }
    }
}