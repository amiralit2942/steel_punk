﻿using System;
using System.Security.AccessControl;
using _Scripts.Manager;
using _Scripts.Manager.Currencies;
using _Scripts.Manager.Preload;
using _Scripts.UI.Utils;
using _Scripts.Util;
using _Scripts.Util.UI;
using RTLTMPro;
using UnityEngine;
using UnityEngine.UI;
using Util;

namespace _Scripts.UI.NoticePopUp
{
    public class NoticePopUpPanel : MonoBehaviourSingleton<NoticePopUpPanel>, IPopUpPanel
    {
        private void Awake()
        {
            DontDestroyOnLoad(gameObject);
        }

        [SerializeField] private GameObject back;
        [SerializeField] private AnimatedPanel panel;
        [SerializeField] private Image gemImage;
        [SerializeField] private Image coinImage;
        [SerializeField] private RTLTextMeshPro content;
        // [SerializeField] private RTLTextMeshPro acceptButtonText;
        // [SerializeField] private RTLTextMeshPro rejectButtonText;
        [SerializeField] private RTLTextMeshPro currencyText;
        [SerializeField] private GameObject acceptButtonGo;
        [SerializeField] private GameObject rejectButtonGo;
        [SerializeField] private Button acceptButton;
        [SerializeField] private Button rejectButton;
        [SerializeField] private Button exitButton;
        [SerializeField] private Button retryButton;
        [SerializeField] private Button buttonWithContent1;
        [SerializeField] private RTLTextMeshPro button1Content;
        [SerializeField] private Button buttonWithContent2;
        [SerializeField] private RTLTextMeshPro button2Content;
        private bool _isPopUpActive = false;
        public bool IsPopUpActive => _isPopUpActive;

        public void Active()
        {
            back.SetActive(true);
            panel.ActivePanel();
        }

        public NoticePopUpPanel NewNotice()
        {
            _isPopUpActive = true;
            content.text = "";
            acceptButton.onClick.RemoveAllListeners();
            rejectButton.onClick.RemoveAllListeners();
            acceptButtonGo.SetActive(false);
            rejectButtonGo.SetActive(false);
            exitButton.gameObject.SetActive(false);
            retryButton.gameObject.SetActive(false);
            currencyText.gameObject.SetActive(false);
            coinImage.gameObject.SetActive(false);
            gemImage.gameObject.SetActive(false);
            buttonWithContent1.gameObject.SetActive(false);
            buttonWithContent2.gameObject.SetActive(false);
            Active();
            return this;
        }

        public NoticePopUpPanel WithContent(string text)
        {
            content.text = text;
            return this;
        }

        // public NoticePopUpPanel WithAcceptButton(string text, Action onClick)
        // {
        //     print("Click"); 
        //     acceptButtonGo.SetActive(true);
        //     acceptButtonText.text = text;
        //     acceptButton.onClick.AddListener(DeActive);
        //     acceptButton.onClick.AddListener(()=>onClick());
        //     return this;
        // }
        public NoticePopUpPanel WithAcceptButton(Action onClick)
        {
            print("Click"); 
            acceptButtonGo.SetActive(true);
            acceptButton.onClick.AddListener(DeActive);
            acceptButton.onClick.AddListener(()=>onClick());
            return this;
        }

        public NoticePopUpPanel WithRejectButton(Action onClick)
        {
            rejectButtonGo.SetActive(true);
            rejectButton.onClick.AddListener(DeActive);
            rejectButton.onClick.AddListener(()=>onClick());
            return this;
        }
        
        public NoticePopUpPanel WithRejectButton()
        {
            rejectButtonGo.SetActive(true);
            rejectButton.onClick.AddListener(DeActive);
            return this;
        }
        
        public void DeActive()
        {
            back.SetActive(false);
            panel.DeActivePanel();
            _isPopUpActive = false;
        }

        public void Exit()
        {
            NewNotice();
            exitButton.gameObject.SetActive(true);
            rejectButton.gameObject.SetActive(true);
            content.text = Strings.ExitApp;
            exitButton.onClick.AddListener(Application.Quit);
            rejectButton.onClick.AddListener(DeActive);
        }

        public void LoginFailed()
        {
            NewNotice();
            retryButton.gameObject.SetActive(true);
            content.text = Strings.LoginFailed;
            retryButton.onClick.AddListener(() =>
            {
                PreloadManager.Instance.CallLogin();
                DeActive();
            });
        }

        public void UnlockChestWithGem(string contentText , int gemAmount,Action openUnlockingChest)
        {
            print("Unlock With Gem Notice Called");
            NewNotice();
            gemImage.gameObject.SetActive(true);
            currencyText.gameObject.SetActive(true);
            
            currencyText.text = gemAmount.ToString();
            content.text = contentText;
            acceptButton.gameObject.SetActive(true);
            rejectButton.gameObject.SetActive(true);
            
            acceptButton.onClick.AddListener(() =>
            {
                openUnlockingChest();
                DeActive();
            });
            rejectButton.onClick.AddListener(() =>
            {
                gemImage.gameObject.SetActive(false);
                currencyText.gameObject.SetActive(false);
                DeActive();
            });
        }

        public NoticePopUpPanel ActiveButtonOneWithContent(string firstContent,Action action)
        {
            buttonWithContent1.gameObject.SetActive(true);
            button1Content.text = firstContent;
            buttonWithContent1.onClick.AddListener(() =>
            {
                action();
                DeActive();
            });
            return this;
        }
        public NoticePopUpPanel ActiveButtonTwoWithContent(string firstContent,Action action)
        {
            buttonWithContent2.gameObject.SetActive(true);
            button2Content.text = firstContent;
            buttonWithContent2.onClick.AddListener(() =>
            {
                action();
                DeActive();
            });
            return this;
        }
    }
}