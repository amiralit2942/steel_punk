﻿using UnityEngine;
using UnityEngine.UI;
using Util;

namespace UI
{
    public class SettingPanel : MonoBehaviourSingleton<SettingPanel>
    {
        [SerializeField] private AudioSource masterSource;
        [SerializeField] private AudioSource musicSource;
        [SerializeField] private Slider masterVolSlider;
        [SerializeField] private Slider musicVolSlider;
        [SerializeField] private AudioClip menuMusic;
        [SerializeField] private AudioClip gameplayMusic;
        [SerializeField] private GameObject slidersPanel; 
        [SerializeField] private GameObject buttonsPanel; 
        private float _masterVolume;
        private float _musicVolume;
        
        // Start is called before the first frame update
        void Start()
        {
            DontDestroyOnLoad(gameObject);    
            // masterVolSlider.value = PlayerPrefs.GetFloat("MasterVolume",1);
            // musicVolSlider.value = PlayerPrefs.GetFloat("MusicVolume", 1);
            
        }

        // Update is called once per frame
        void Update()
        {
           // masterSource.volume = _masterVolume;
            //musicSource.volume = _musicVolume;
        }

        public void SetMasterVolume(float volume)
        {
            _masterVolume = volume;
            PlayerPrefs.SetFloat("MasterVolume",_masterVolume);
            
        }
        public void SetMusicVolume(float volume)
        {
            _musicVolume = volume;
            PlayerPrefs.SetFloat("MusicVolume",_musicVolume);
        }

        public void SetMenuMusic()
        {
            //masterSource.clip = menuMusic;
        }

        public void SetGameplayMusic()
        {
            //masterSource.clip = gameplayMusic;
        }

        public void SlidersPanel()
        {
            slidersPanel.SetActive(true);
            buttonsPanel.SetActive(false);
        }
        public void ButtonsPanel()
        {
            slidersPanel.SetActive(false);
            buttonsPanel.SetActive(true);
        }
        
    }
}