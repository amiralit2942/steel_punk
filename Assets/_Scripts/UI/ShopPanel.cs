﻿using System;
using System.Collections.Generic;
using _Scripts.Controller.Spells;
using _Scripts.DataHolder;
using _Scripts.Manager;
using _Scripts.Model;
using UnityEngine;
using UnityEngine.UI;
using Util;

namespace _Scripts.UI
{
    public class ShopPanel : MonoBehaviourSingleton<ShopPanel>
    {
        
        [SerializeField] private GameObject buySpellPanel;
        [SerializeField] private GameObject buyCoinPanel;
        [SerializeField] private GameObject buyGemPanel;
        
        private void Start()
        {
            GetStoresContent();
            GetSpells();
        }

        public void GetStoresContent()
        {
            foreach(var content in SafeBox.Instance.shopItems)
            {
                if (content.reward_type == ShopItems.RewardType.RewardCoin)
                {
                    var card = Instantiate(PrefabHolder.Instance.GetCoinStoreCard()
                        , buyCoinPanel.transform);
                    card.SetInfo(content);
                }
                else if (content.reward_type == ShopItems.RewardType.RewardGem)
                {
                    var card = Instantiate(PrefabHolder.Instance.GetGemStoreCard()
                        , buyGemPanel.transform);
                    card.SetInfo(content);
                }

                // switch (content.reward_type)
                // {
                //     case ShopItems.RewardType.RewardCoin:
                //         var coinCard = Instantiate(PrefabHolder.Instance.GetCoinStoreCard()
                //             , buyCoinPanel.transform);
                //         coinCard.SetInfo(content);
                //         break;
                //     case ShopItems.RewardType.RewardGem:
                //         var gemCard = Instantiate(PrefabHolder.Instance.GetGemStoreCard()
                //             , buyGemPanel.transform);
                //         gemCard.SetInfo(content);
                //         break;
                //     case ShopItems.RewardType.RewardSpell:
                //         var spellCard =Instantiate(PrefabHolder.Instance.GetSpellShopCard()
                //             , buySpellPanel.transform);
                //         spellCard.SetInfo(content);
                //         break;
                // }
            }
        }

        
        
        
        public void GetSpells()
        {
            foreach (var spell in SafeBox.Instance.spellsInfo)
            {
                var card =Instantiate(PrefabHolder.Instance.GetSpellShopCard()
                    , buySpellPanel.transform);
                card.SetInfo(spell);
                
            }
        }

        public void DestroyCards(List<GameObject> cards)
        {
            if (cards != null)
            {
                foreach (var card in cards)
                {
                    Destroy(card);
                }    
            }
        }

    }
}