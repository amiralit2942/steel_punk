﻿using System;
using _Scripts.Controller.Abstract;
using _Scripts.Controller.Spells;
using _Scripts.DataHolder;
using _Scripts.Manager;
using _Scripts.Util;
using TMPro;
using UnityEngine;
using UnityEngine.UI;
using Util;

namespace _Scripts.UI
{
    public class SoldierInfoPopUp : MonoBehaviourSingleton<SoldierInfoPopUp>
    {
        [SerializeField] private GameObject popUpPanel;
        [SerializeField] private TMP_Text description;
        [SerializeField] private Image soldierImage;
        [SerializeField] private TMP_Text name;
        [SerializeField] private TMP_Text damage;
        [SerializeField] private TMP_Text health;
        [SerializeField] private TMP_Text spawnTime;


        private void Start()
        {
            popUpPanel.SetActive(false);
        }

        public void ActivePopUp(Soldier.Soldiers soldierType)
        {
                var soldier = SafeBox.Instance.cardsInfo.Find(x => x.soldier_type == soldierType);
            popUpPanel.SetActive(true);
            soldierImage.sprite = PrefabHolder.Instance.GetCardImage(soldierType);
            //description.text = Strings.GetSoldierInfos(soldierType);
            name.text = soldier.name;
            damage.text = soldier.damage.ToString();
            health.text = soldier.health.ToString();
            spawnTime.text = soldier.spawn_cool_down.ToString();
        }

        public void ActivePopUp(Spell.SpellType spellType)
        {
            popUpPanel.SetActive(true);
            soldierImage.sprite = PrefabHolder.Instance.GetSpellImageHolderAsset(spellType);
            //description.text = Strings.GetSoldierInfos(spellType);
            //TODO Add Spell Info To The Safe Box Then Use It
        }

        public void DisablePopUp()
        {
            popUpPanel.SetActive(false);
        }
        
    }
}
