﻿using _Scripts.Util.UI;
using UnityEngine;

namespace _Scripts.UI.Utils
{
    public interface IPopUpPanel
    {
        public void Active();
        public void DeActive();
    }
}