﻿using System;
using _Scripts.Controller.Card;
using _Scripts.Controller.Card.Chest;
using _Scripts.Model;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace _Scripts.Util
{
    public class ChestTimer : MonoBehaviour
    {
        private float _time;
        private int _hour;
        private int _minute;
        private float _second;
        private bool _timerIsActive;
        private ChestCard _chestCard;
        [SerializeField] private TMP_Text timerText;
        
        void Start()
        {
            //timerText.gameObject.SetActive(false);
            _chestCard = GetComponentInParent<ChestCard>();
        }
    
   
        void Update()
        {
            if (_timerIsActive)
            {
                _second -= 1 * Time.deltaTime;
                _time -= 1 * Time.deltaTime;
                var time = "";
                if (_hour > 0)
                {
                    timerText.text = $"{_hour:00}:{_minute:00}";
                }
                else
                {
                    timerText.text = $"{_minute:00}:{_second:00}";
                }
                
                if (_second <= 0)
                {
                    if (_minute > 0)
                    {
                        _minute--;
                        _second = 60;
                    }
                    else
                    {
                        if (_hour > 0)
                        {
                            _hour--;
                            _minute = 59;
                        }
                        else
                        {
                            timerText.text = "READY";
                            _second = 0;
                            _timerIsActive = false;
                            _chestCard.ChestReady();
                        }
                    }
                }
            }
        }

        public void SetTimer(float timer)
        {
            timerText.gameObject.SetActive(true);
            _time = timer;
            _timerIsActive = true;
            _second = (int)timer % 60;
            _minute = (int) timer / 60;
            _hour = (int) _minute / 60;
        }

        public float GetRemainingTime()
        {
            return _time;
        }

        public void AlreadyReady()
        {
            timerText.gameObject.SetActive(true);
            timerText.text = "Ready";
        }
        public bool TimerEnded()
        {
            if (_hour == 0 && _minute == 0 && _second == 0)
            {
                return true;
            }
            return false;
        }
    }
}