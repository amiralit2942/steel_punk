﻿using System;
using System.Collections.Generic;
using UnityEngine;

namespace _Scripts.Util
{
    public static class LocalizationLanguage
    {
        public const string Persian = "Persian";
        public const string English = "English";
    }
    
    public static class Localization
    {
        private static TextAsset _textAsset;
        private static List<Dictionary<string, object>> _csv;
        private static string _language = LocalizationLanguage.Persian;
        public static string Language => _language;

        public static event Action OnChangeLanguage;
        
        public static void ChangeLanguage(string lang)
        {
            _language = lang;
            PlayerPrefs.SetString("language", lang);
            OnChangeLanguage?.Invoke();
        }

        public static void SetLanguage()
        {
            _language = PlayerPrefs.GetString("language");
        }

        public static string Get(string key)
        {
            if (_textAsset == null)
            {
                _textAsset = Resources.Load<TextAsset>("Localization");
                _csv = CSVReader.Read(_textAsset.text);
            }
            var data = _csv.Find(t => t["KEY"].ToString() == key);
            if (data != null)
            {
                return data[_language].ToString();
            }

            Debug.LogWarning("Localization:: key not found!");
            return key;
        }
    }
}