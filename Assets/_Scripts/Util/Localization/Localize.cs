﻿using System;
using _Scripts.DataHolder;
using RTLTMPro;
using UnityEngine;

namespace _Scripts.Util
{
    [RequireComponent(typeof(RTLTextMeshPro))]
    public class Localize : MonoBehaviour
    {
        [SerializeField] private string key;
        private RTLTextMeshPro _text;

        private void OnEnable()
        {
            SetStr();
            Localization.OnChangeLanguage += SetStr;
        }

        private void OnDisable()
        {
            Localization.OnChangeLanguage -= SetStr;
        }

        private void SetFont()
        {
            if (Localization.Language == LocalizationLanguage.Persian)
            {
                
                _text.font = PrefabHolder.Instance.PersianFont;
            }
            else
            {
                _text.font = PrefabHolder.Instance.EnglishFont;
            }
        }

        private void SetStr()
        {
            _text = GetComponent<RTLTextMeshPro>();
            if (_text != null && key!="")
                _text.text = Localization.Get(key);
            SetFont();
        }
    }
}