using System;
using UnityEngine;
using Random = UnityEngine.Random;

namespace _Scripts.Util
{
    public class RandomAnimPlayer : MonoBehaviour
    {
        [SerializeField] private int animationsNumber;
        [SerializeField] private Animator animator;
        private int _lastRandom;
        private int _newRandom;
        private bool _randomIsRepetitious;
        private void Start()
        {
            _randomIsRepetitious = false;
            _lastRandom = 0;
            StartRandomAnim();
        }

        private void StartRandomAnim()
        {
            _newRandom = Random.Range(1, animationsNumber + 1);
            if (_newRandom == _lastRandom)
            {
                if (_randomIsRepetitious)
                {
                    while (_newRandom != _lastRandom)
                    {
                        _newRandom = Random.Range(1, animationsNumber + 1);
                    }
                    _lastRandom = _newRandom;
                    _randomIsRepetitious = false;
                }
                else
                {
                    _randomIsRepetitious = true;
                }
            }
            else
            {
                _randomIsRepetitious = false;
                _lastRandom = _newRandom;
            }
            animator.SetInteger("Random",_newRandom);
        }
        
        public void AnimationEnded()
        {
            animator.SetBool("Ended",true);
            StartRandomAnim();
        }
    }
}