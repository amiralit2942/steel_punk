﻿#if UNITY_EDITOR
using UnityEditor;
using UnityEditor.SceneManagement;
using UnityEngine;

namespace _Scripts.Util
{

    

    public class SceneUtility : MonoBehaviour
    {
        [MenuItem("Utility/Play", false, 1)]
        public static void Play()
        {
            var path = EditorBuildSettings.scenes[0].path;
            EditorSceneManager.SaveCurrentModifiedScenesIfUserWantsTo();
            EditorSceneManager.OpenScene(path);
            EditorApplication.isPlaying = true;
        }
        
        [MenuItem("Utility/Preload", false, 13)]
        public static void PreloadScene()
        {
            var path = EditorBuildSettings.scenes[0].path;
            EditorSceneManager.SaveCurrentModifiedScenesIfUserWantsTo();
            EditorSceneManager.OpenScene(path);
        }
        
        [MenuItem("Utility/Menu", false, 14)]
        public static void MenuScene()
        {
            var path = EditorBuildSettings.scenes[1].path;
            EditorSceneManager.SaveCurrentModifiedScenesIfUserWantsTo();
            EditorSceneManager.OpenScene(path);
        }
        
        [MenuItem("Utility/Gameplay", false, 15)]
        public static void Gameplay()
        {
            var path = EditorBuildSettings.scenes[2].path;
            EditorSceneManager.SaveCurrentModifiedScenesIfUserWantsTo();
            EditorSceneManager.OpenScene(path);
        }
        
        [MenuItem("Utility/Config", false, 1)]
        public static void Config()
        {
            var path = EditorBuildSettings.scenes[0].path;
            var scene = AssetDatabase.LoadAssetAtPath<SceneAsset>(path);
            EditorSceneManager.playModeStartScene = scene;
        }
    }
}
#endif
