﻿using _Scripts.Controller.Abstract;
using _Scripts.Controller.Spells;

namespace _Scripts.Util
{
    public class Strings
    {
        public static string sss => Localization.Get("sss");
        public static string ShopNotAvailable => Localization.Get("shopNotAvailable");
        public static string ConnectionError => Localization.Get("connectionError");
        public static string RegisterSucceed => Localization.Get("registerSucceed");
        public static string RegisterFailed => Localization.Get("registerFailed");
        public static string GetUserData => Localization.Get("getUserData");
        public static string GetCardData => Localization.Get("getCardData");
        public static string GetShopData => Localization.Get("getShopData");
        public static string GetConfigData => Localization.Get("getConfigData");
        public static string YouWin => Localization.Get("youWin");
        public static string YouLost => Localization.Get("youLost");
        public static string GameOverPlayAgain => Localization.Get("gameOverPlayAgain");
        public static string GameOverGoBack => Localization.Get("gameOverGoBack");
        public static string BuySpellNotice => Localization.Get("buySpellNotice");
        public static string BuyCurrency => Localization.Get("buyCurrency");
        public static string NotEnoughGem => Localization.Get("notEnoughGem");
        public static string NotEnoughCoin => Localization.Get("notEnoughCoin");
        public static string PurchaseFailed => Localization.Get("purchaseFailed");
        public static string SpellAlreadyInDeck => Localization.Get("spellAlreadyInDeck");
        public static string Upgrade => Localization.Get("upgrade");
        public static string UpgradeFailed => Localization.Get("upgradeFailed");
        public static string LoginFailed => Localization.Get("loginFailed");
        public static string ExitApp => Localization.Get("exitApp");
        public static string ExitGame => Localization.Get("exitGame");
        public static string OpenChestWithGem => Localization.Get("openChestWithGem");
        public static string ManaIncomeRate ="manaIncomeRate";

        public static string ManaMax = "manaMax";
        public static string MatchMakingFailed => Localization.Get("matchMakingFailed");
        public static string ChangingDeckFailed => Localization.Get("changingDeckFailed");
        public static string NeedMoreSoldiers => Localization.Get("needMoreSoldiers");
        public static string MatchMaking => Localization.Get("matchMaking");

        //ANALYTICS:
        public static string FromShop => "Shop";
        public static string FromDeckPanel => "DeckPanel";
        public static string FromChestPanel => "ChestPanel";
        public static string CurrencyCoin => "Coin";
        public static string CurrencyGem => "Gem";
        public static string ItemTypeGem => "Gem";
        public static string ItemTypeCoin => "Gem";
        public static string ItemTypeSoldier => "SoldierUpgrade";
        public static string ItemTypeSpell => "Spell";
        public static string ItemTypeChest => "Chest";


        // public static static string GetSoldierInfos(Soldier.Soldiers soldierType)
        // {
        //     var info = "";
        //     switch (soldierType)
        //     {
        //         case Soldier.Soldiers.Sopo:
        //             info = " ";
        //             break;
        //         case Soldier.Soldiers.Tank:
        //             info = " ";
        //             break;
        //         case Soldier.Soldiers.Bomber:
        //             info = " ";
        //             break;
        //         case Soldier.Soldiers.Cop:
        //             info = " ";
        //             break;
        //         case Soldier.Soldiers.Sniper:
        //             info =" ";
        //             break;
        //         case Soldier.Soldiers.Wizard:
        //             info = " ";
        //             break;
        //     }
        //
        //     return info;
        // }
        //
        // public static static string GetSoldierInfos(Spell.SpellType spellType)
        // {
        //     var info => " ";
        //     switch (spellType)
        //     {
        //         case Spell.SpellType.HealSpell:
        //             info => " ";
        //             break;
        //         case Spell.SpellType.FreezeSpell:
        //             info => " ";
        //             break;
        //         case Spell.SpellType.SpeedSpell:
        //             info => " ";
        //             break;
        //     }
        //
        //     return info;
        // }
    }
}