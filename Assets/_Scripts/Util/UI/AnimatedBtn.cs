﻿using DG.Tweening;
using UnityEngine;
using UnityEngine.EventSystems;

namespace _Scripts.Util.UI
{
    public class AnimatedBtn : MonoBehaviour , IPointerDownHandler, IPointerUpHandler
    {
        private RectTransform _rectTransform;
        public RectTransform RectTransform
        {
            get
            {
                if (_rectTransform == null) _rectTransform = GetComponent<RectTransform>();
                return _rectTransform;
            }
        }
        
        

        [SerializeField,Range(0.5f, 1)]
        private float onHoldShrinkMultiplier = 0.8f;

        [SerializeField] private Vector2 defaultTransform;
        [SerializeField] private float doTweenDuration;

        public void OnPointerDown(PointerEventData eventData)
        {
            RectTransform.DOComplete();
            RectTransform.DOScale(Vector3.one * onHoldShrinkMultiplier, doTweenDuration);
        }

        public void OnPointerUp(PointerEventData eventData)
        {
            RectTransform.DOComplete();
            RectTransform.DOScale(Vector3.one, doTweenDuration);
        }
    }
}
