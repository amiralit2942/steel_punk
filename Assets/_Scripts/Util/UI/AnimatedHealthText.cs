﻿using System;
using DG.Tweening;
using TMPro;
using UnityEngine;
using UnityEngine.UI;
using Util;

namespace _Scripts.Util.UI
{
    public class AnimatedHealthText : MonoBehaviour
    {
        private TextMeshPro _healthText;
        private Animator _animator;
        private void Start()
        {
            _healthText = GetComponent<TextMeshPro>();
            
        }

        public void TextColorChanger(int maxHealth,int damage,Text healthText,float r,float g,float b , float a)
        {
            var damagePerColor = damage * 2 / maxHealth;
            if (r + damagePerColor > 1)
            {
                var difference = damagePerColor - (1 - r);
                
                if (g - difference < 0)
                {
                    g = 0;
                    g = 1; 
                    //healthText.color = new Color(_r,_g,0,255);
                    healthText.DOColor(new Color(r, g, 0, 1), 0.2f);
                }
                else
                {
                    g -= difference;
                    r = 1;
                    //healthText.color = new Color(_r,_g,0,255);
                    healthText.DOColor(new Color(r, g, 0, 1), 0.2f);

                }
            }
            else
            {
                r += damagePerColor;
                healthText.DOColor(new Color(r, g, 0, 1), .5f);
                //healthText.color = new Color(_r,_g,0,255);
            }
        }

        public void Fade()
        {
            var text = gameObject.GetComponent<TextMeshPro>();
            text.DOFade(0, 0.5f);
        }

        public void HealedText()
        {
            _healthText.gameObject.SetActive(true);
            //_animator.Play();
        }

        public void DamagedText()
        {
            
        }
        
        
        
        
        
        
    }
}
