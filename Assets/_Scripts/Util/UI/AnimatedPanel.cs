﻿using _Scripts.DataHolder;
using DG.Tweening;
using UnityEngine;

namespace _Scripts.Util.UI
{
    public class AnimatedPanel : MonoBehaviour
    {
        private RectTransform _rectTransform;
        public RectTransform RectTransform
        {
            get
            {
                if (_rectTransform == null)
                {
                    _rectTransform = GetComponent<RectTransform>();
                }

                return _rectTransform;
            }
        }

        private CanvasGroup _canvasGroup;

        private CanvasGroup CanvasGroup
        {
            get
            {
                if (_canvasGroup == null)
                {
                    _canvasGroup = GetComponent<CanvasGroup>();
                    if (_canvasGroup == null)
                    {
                        _canvasGroup = gameObject.AddComponent<CanvasGroup>();
                    }
                }

                return _canvasGroup;
            }
        }

        [SerializeField] private Vector3 disablePos;
        [SerializeField] private Vector3 activePos = new Vector3(0,0,0);
        [SerializeField] private float duration;
        // [SerializeField] private Vector3 peakPos;
        [SerializeField] private AnimationCurve curve;
        [SerializeField] private bool fade = true;
        
        public void ActivePanel()
        {
            gameObject.SetActive(true);
            RectTransform.anchoredPosition = disablePos;
            RectTransform.DOKill();
            RectTransform.DOAnchorPos(activePos, duration).SetEase(curve);
            if (fade)
            {
                CanvasGroup.DOKill();
                CanvasGroup.alpha = 0f;
                CanvasGroup.DOFade(1f, duration);
            }
        }

        public void DeActivePanel()
        {
            RectTransform.DOKill();
            RectTransform.DOAnchorPos(disablePos, duration).SetEase(curve).OnComplete(() =>
            {
                //gameObject.SetActive(false);
            });
            if (fade)
            {
                CanvasGroup.DOKill();
                CanvasGroup.alpha = 1f;
                CanvasGroup.DOFade(0f, duration);
            }
        }
    }
}
