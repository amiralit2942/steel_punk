﻿using System;
using UnityEngine;

namespace _Scripts.Util
{
    public class UndestroyableGO : MonoBehaviour
    
    {
        private void Awake()
        {
            DontDestroyOnLoad(gameObject);
        }
    }
}