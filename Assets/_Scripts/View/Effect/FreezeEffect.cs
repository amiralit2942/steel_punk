﻿using System.Collections;
using System.Collections.Generic;
using DG.Tweening;
using UnityEngine;

namespace View
{
    public class FreezeEffect : MonoBehaviour,IEffect
    {
        [SerializeField] private SpriteRenderer[] _sprites;
        [SerializeField] private Color _effectColor;

        public void OnStart()
        {
            foreach (var sprite in _sprites)
            {
                sprite.DOColor(_effectColor, 1.5f);
            }
        }

        public void OnEnd()
        {
            foreach (var sprite in _sprites)
            {
                sprite.DOColor(Color.white, 1.5f);
            }
        }
    }
}